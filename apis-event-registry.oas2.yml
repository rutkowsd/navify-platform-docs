swagger: '2.0'
info:
  description: Repository of Events used within Platform hosted applications
  version: '1.0'
  title: Platform Event Registry Microservice
  termsOfService: Terms of Service
  contact:
    name: Platform Core Team
    email: reworg_platform-core-team@msxdl.roche.com
host: localhost
basePath: /
tags:
  - name: event-registry-controller
    description: Event Registry Controller
paths:
  '/api/v1/app/{appId}/events':
    get:
      tags:
        - event-registry-controller
      summary: Get events for an application
      description: >-
        Retrieve list of events which are produced and consumed by the given
        application.

        * Called with: **Platform Admin** role
      operationId: getAllEventsByAppIdUsingGET
      produces:
        - application/json;charset=UTF-8
      parameters:
        - name: appId
          in: path
          description: appId
          required: true
          type: string
          format: uuid
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/EventSummaryDTO'
        '400':
          description: Bad Request
          schema:
            $ref: '#/definitions/ErrorDTO'
        '403':
          description: Forbidden
          schema:
            $ref: '#/definitions/ErrorDTO'
        '404':
          description: Not Found
          schema:
            $ref: '#/definitions/ErrorDTO'
      security:
        - Authorization:
            - global
        - X-Platform-Key:
            - global
      deprecated: false
  /api/v1/events:
    get:
      tags:
        - event-registry-controller
      summary: List events
      description: |-
        Retrieve list of all events.
        * Called with: **Platform Admin** role
      operationId: getEventsUsingGET
      produces:
        - application/json;charset=UTF-8
      parameters:
        - name: limit
          in: query
          description: >-
            Number of records to be retrieved through this search. Default limit
            is 100. Maximum limit is 500
          required: false
          type: integer
          format: int32
          allowEmptyValue: false
        - name: offset
          in: query
          description: Search will start from this offset. By default it is 0
          required: false
          type: integer
          format: int32
          allowEmptyValue: false
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/EventResponseListDTO'
        '400':
          description: Bad Request
          schema:
            $ref: '#/definitions/ErrorDTO'
        '403':
          description: Forbidden
          schema:
            $ref: '#/definitions/ErrorDTO'
        '404':
          description: Not Found
          schema:
            $ref: '#/definitions/ErrorDTO'
      security:
        - Authorization:
            - global
        - X-Platform-Key:
            - global
      deprecated: false
    post:
      tags:
        - event-registry-controller
      summary: Create events
      description: |-
        Create new events.
        * Called with: **Platform Admin** role
      operationId: createEventsUsingPOST
      consumes:
        - application/json
      produces:
        - application/json;charset=UTF-8
      parameters:
        - in: body
          name: createEventRequestDTO
          description: createEventRequestDTO
          required: true
          schema:
            $ref: '#/definitions/CreateEventRequestDTO'
      responses:
        '201':
          description: Created
          schema:
            $ref: '#/definitions/ResponseEntity'
        '400':
          description: Bad Request
          schema:
            $ref: '#/definitions/ErrorDTO'
        '403':
          description: Forbidden
          schema:
            $ref: '#/definitions/ErrorDTO'
        '404':
          description: Not Found
          schema:
            $ref: '#/definitions/ErrorDTO'
        '409':
          description: Conflict
          schema:
            $ref: '#/definitions/ErrorDTO'
      security:
        - Authorization:
            - global
        - X-Platform-Key:
            - global
      deprecated: false
  '/api/v1/events/{eventAlias}':
    get:
      tags:
        - event-registry-controller
      summary: Retrieve an event by event alias
      description: |-
        Retrieve an event by event alias.
        * Called with: **Platform Admin** role
      operationId: getEventUsingGET
      produces:
        - application/json;charset=UTF-8
      parameters:
        - name: eventAlias
          in: path
          description: eventAlias
          required: true
          type: string
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/EventResponseDTO'
        '400':
          description: Bad Request
          schema:
            $ref: '#/definitions/ErrorDTO'
        '403':
          description: Forbidden
          schema:
            $ref: '#/definitions/ErrorDTO'
        '404':
          description: Not Found
          schema:
            $ref: '#/definitions/ErrorDTO'
      security:
        - Authorization:
            - global
        - X-Platform-Key:
            - global
      deprecated: false
    put:
      tags:
        - event-registry-controller
      summary: Update event
      description: |-
        Update event data.
        * Called with: **Platform Admin** role
      operationId: updateEventUsingPUT
      consumes:
        - application/json
      produces:
        - application/json;charset=UTF-8
      parameters:
        - name: eventAlias
          in: path
          description: eventAlias
          required: true
          type: string
        - in: body
          name: updateEventDTO
          description: updateEventDTO
          required: true
          schema:
            $ref: '#/definitions/UpdateEventRequestDTO'
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/ResponseEntity'
        '400':
          description: Bad Request
          schema:
            $ref: '#/definitions/ErrorDTO'
        '403':
          description: Forbidden
          schema:
            $ref: '#/definitions/ErrorDTO'
        '404':
          description: Not Found
          schema:
            $ref: '#/definitions/ErrorDTO'
        '409':
          description: Conflict
          schema:
            $ref: '#/definitions/ErrorDTO'
      security:
        - Authorization:
            - global
        - X-Platform-Key:
            - global
      deprecated: false
    delete:
      tags:
        - event-registry-controller
      summary: Delete event
      description: |-
        Delete an event by event alias.
        * Called with: **Platform Admin** role
      operationId: deleteEventUsingDELETE
      produces:
        - application/json;charset=UTF-8
      parameters:
        - name: eventAlias
          in: path
          description: eventAlias
          required: true
          type: string
      responses:
        '204':
          description: No Content
          schema:
            $ref: '#/definitions/ResponseEntity'
        '400':
          description: Bad Request
          schema:
            $ref: '#/definitions/ErrorDTO'
        '403':
          description: Forbidden
          schema:
            $ref: '#/definitions/ErrorDTO'
        '404':
          description: Not Found
          schema:
            $ref: '#/definitions/ErrorDTO'
      security:
        - Authorization:
            - global
        - X-Platform-Key:
            - global
      deprecated: false
  '/api/v1/events/{eventAlias}/consumer':
    post:
      tags:
        - event-registry-controller
      summary: Create event consumer
      description: |-
        Add consumer of an event.
        * Called with: **Platform Admin** role
      operationId: createEventConsumerUsingPOST
      consumes:
        - application/json
        - application/json;charset=UTF-8
      produces:
        - application/json;charset=UTF-8
      parameters:
        - in: body
          name: consumerRequestDTO
          description: consumerRequestDTO
          required: true
          schema:
            $ref: '#/definitions/ConsumerRequestDTO'
        - name: eventAlias
          in: path
          description: eventAlias
          required: true
          type: string
      responses:
        '201':
          description: Created
          schema:
            $ref: '#/definitions/ConsumerResponseDTO'
        '400':
          description: Bad Request
          schema:
            $ref: '#/definitions/ErrorDTO'
        '403':
          description: Forbidden
          schema:
            $ref: '#/definitions/ErrorDTO'
        '404':
          description: Not Found
          schema:
            $ref: '#/definitions/ErrorDTO'
        '409':
          description: Conflict
          schema:
            $ref: '#/definitions/ErrorDTO'
      security:
        - Authorization:
            - global
        - X-Platform-Key:
            - global
      deprecated: false
  '/api/v1/events/{eventAlias}/consumers':
    get:
      tags:
        - event-registry-controller
      summary: List consumers
      description: |-
        Retrieve list of consumers which are subscribed to a given event.
        * Called with: **Platform Admin** role
      operationId: getConsumersForEventUsingGET
      produces:
        - application/json;charset=UTF-8
      parameters:
        - name: eventAlias
          in: path
          description: eventAlias
          required: true
          type: string
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/ConsumerListDTO'
        '400':
          description: Bad Request
          schema:
            $ref: '#/definitions/ErrorDTO'
        '403':
          description: Forbidden
          schema:
            $ref: '#/definitions/ErrorDTO'
        '404':
          description: Not Found
          schema:
            $ref: '#/definitions/ErrorDTO'
      security:
        - Authorization:
            - global
        - X-Platform-Key:
            - global
      deprecated: false
  '/api/v1/events/{eventAlias}/consumers/{appId}':
    delete:
      tags:
        - event-registry-controller
      summary: Delete consumer
      description: |-
        The consumer will no longer exist for the event.
        * Called with: **Platform Admin** role
      operationId: deleteConsumerUsingDELETE
      produces:
        - application/json;charset=UTF-8
      parameters:
        - name: appId
          in: path
          description: appId
          required: true
          type: string
          format: uuid
        - name: eventAlias
          in: path
          description: eventAlias
          required: true
          type: string
      responses:
        '204':
          description: No Content
          schema:
            $ref: '#/definitions/ResponseEntity'
        '400':
          description: Bad Request
          schema:
            $ref: '#/definitions/ErrorDTO'
        '403':
          description: Forbidden
          schema:
            $ref: '#/definitions/ErrorDTO'
        '404':
          description: Not Found
          schema:
            $ref: '#/definitions/ErrorDTO'
      security:
        - Authorization:
            - global
        - X-Platform-Key:
            - global
      deprecated: false
  '/api/v1/events/{eventAlias}/sampledata':
    get:
      tags:
        - event-registry-controller
      summary: Retrieve sample data
      description: |-
        Retrieve sample data of an event.
        * Called with: **Platform Admin** role
      operationId: getSampleDataUsingGET
      produces:
        - application/json;charset=UTF-8
      parameters:
        - name: eventAlias
          in: path
          description: eventAlias
          required: true
          type: string
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/SampleDataResponseDTO'
        '400':
          description: Bad Request
          schema:
            $ref: '#/definitions/ErrorDTO'
        '403':
          description: Forbidden
          schema:
            $ref: '#/definitions/ErrorDTO'
        '404':
          description: Not Found
          schema:
            $ref: '#/definitions/ErrorDTO'
      security:
        - Authorization:
            - global
        - X-Platform-Key:
            - global
      deprecated: false
  '/api/v1/events/{eventAlias}/schema':
    get:
      tags:
        - event-registry-controller
      summary: Retrieve schema
      description: |-
        Retrieve schema of an event.
        * Called with: **Platform Admin** role
      operationId: getSchemaUsingGET
      produces:
        - application/json;charset=UTF-8
      parameters:
        - name: eventAlias
          in: path
          description: eventAlias
          required: true
          type: string
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/SchemaResponseDTO'
        '400':
          description: Bad Request
          schema:
            $ref: '#/definitions/ErrorDTO'
        '403':
          description: Forbidden
          schema:
            $ref: '#/definitions/ErrorDTO'
        '404':
          description: Not Found
          schema:
            $ref: '#/definitions/ErrorDTO'
      security:
        - Authorization:
            - global
        - X-Platform-Key:
            - global
      deprecated: false
securityDefinitions:
  Authorization:
    type: apiKey
    name: Authorization
    in: header
  X-Platform-Key:
    type: apiKey
    name: X-Platform-Key
    in: header
definitions:
  ApplicationDTO:
    type: object
    required:
      - appID
    properties:
      appID:
        type: string
        format: uuid
    title: ApplicationDTO
  ConsumerListDTO:
    type: object
    required:
      - consumers
    properties:
      consumers:
        type: array
        items:
          $ref: '#/definitions/ApplicationDTO'
    title: ConsumerListDTO
  ConsumerRequestDTO:
    type: object
    required:
      - appId
    properties:
      appId:
        type: string
        format: uuid
    title: ConsumerRequestDTO
  ConsumerResponseDTO:
    type: object
    properties:
      appId:
        type: string
        format: uuid
    title: ConsumerResponseDTO
  CreateEventRequestDTO:
    type: object
    required:
      - appID
      - produce
    properties:
      appID:
        type: string
        format: uuid
      consume:
        type: array
        items:
          type: string
      links:
        type: array
        items:
          $ref: '#/definitions/Link'
      produce:
        $ref: '#/definitions/EventRequestListDTO'
    title: CreateEventRequestDTO
  CreateEventResponseDTO:
    type: object
    properties:
      eventAlias:
        type: string
      eventId:
        type: string
        format: uuid
      links:
        type: array
        xml:
          name: link
          namespace: 'http://www.w3.org/2005/Atom'
          attribute: false
          wrapped: false
        items:
          $ref: '#/definitions/Link'
    title: CreateEventResponseDTO
  CreateEventResponseListDTO:
    type: object
    properties:
      events:
        type: array
        items:
          $ref: '#/definitions/CreateEventResponseDTO'
      links:
        type: array
        xml:
          name: link
          namespace: 'http://www.w3.org/2005/Atom'
          attribute: false
          wrapped: false
        items:
          $ref: '#/definitions/Link'
    title: CreateEventResponseListDTO
  ErrorDTO:
    type: object
    properties:
      code:
        type: string
      description:
        type: string
      errorDetails:
        type: string
    title: ErrorDTO
  EventRequestDTO:
    type: object
    properties:
      eventAlias:
        type: string
        minLength: 1
        maxLength: 256
        pattern: '^[\p{L}\d\s\(\)_.-]*$'
      eventDescription:
        type: string
        minLength: 1
        maxLength: 256
      eventSampledata:
        type: string
      eventSchema:
        type: string
      eventStatus:
        type: string
        pattern: ACTIVE|INACTIVE
      links:
        type: array
        items:
          $ref: '#/definitions/Link'
    title: EventRequestDTO
  EventRequestListDTO:
    type: object
    properties:
      events:
        type: array
        items:
          $ref: '#/definitions/EventRequestDTO'
    title: EventRequestListDTO
  EventResponseDTO:
    type: object
    properties:
      consumers:
        type: array
        items:
          $ref: '#/definitions/ApplicationDTO'
      eventAlias:
        type: string
      eventDescription:
        type: string
      eventId:
        type: string
        format: uuid
      eventSampledata:
        type: string
      eventSchema:
        type: string
      eventStatus:
        type: string
      links:
        type: array
        xml:
          name: link
          namespace: 'http://www.w3.org/2005/Atom'
          attribute: false
          wrapped: false
        items:
          $ref: '#/definitions/Link'
      producers:
        type: array
        items:
          $ref: '#/definitions/ApplicationDTO'
    title: EventResponseDTO
  EventResponseListDTO:
    type: object
    properties:
      events:
        type: array
        items:
          $ref: '#/definitions/EventResponseDTO'
      links:
        type: array
        xml:
          name: link
          namespace: 'http://www.w3.org/2005/Atom'
          attribute: false
          wrapped: false
        items:
          $ref: '#/definitions/Link'
    title: EventResponseListDTO
  EventSummaryDTO:
    type: object
    properties:
      consume:
        type: array
        items:
          $ref: '#/definitions/EventResponseDTO'
      links:
        type: array
        xml:
          name: link
          namespace: 'http://www.w3.org/2005/Atom'
          attribute: false
          wrapped: false
        items:
          $ref: '#/definitions/Link'
      produce:
        type: array
        items:
          $ref: '#/definitions/EventResponseDTO'
    title: EventSummaryDTO
  Link:
    type: object
    properties:
      deprecation:
        type: string
        xml:
          name: deprecation
          attribute: true
          wrapped: false
      href:
        type: string
        xml:
          name: href
          attribute: true
          wrapped: false
      hreflang:
        type: string
        xml:
          name: hreflang
          attribute: true
          wrapped: false
      media:
        type: string
        xml:
          name: media
          attribute: true
          wrapped: false
      rel:
        type: string
        xml:
          name: rel
          attribute: true
          wrapped: false
      templated:
        type: boolean
      title:
        type: string
        xml:
          name: title
          attribute: true
          wrapped: false
      type:
        type: string
        xml:
          name: type
          attribute: true
          wrapped: false
    title: Link
  ResponseEntity:
    type: object
    properties:
      body:
        type: object
      statusCode:
        type: string
        enum:
          - 100 CONTINUE
          - 101 SWITCHING_PROTOCOLS
          - 102 PROCESSING
          - 103 CHECKPOINT
          - 200 OK
          - 201 CREATED
          - 202 ACCEPTED
          - 203 NON_AUTHORITATIVE_INFORMATION
          - 204 NO_CONTENT
          - 205 RESET_CONTENT
          - 206 PARTIAL_CONTENT
          - 207 MULTI_STATUS
          - 208 ALREADY_REPORTED
          - 226 IM_USED
          - 300 MULTIPLE_CHOICES
          - 301 MOVED_PERMANENTLY
          - 302 FOUND
          - 302 MOVED_TEMPORARILY
          - 303 SEE_OTHER
          - 304 NOT_MODIFIED
          - 305 USE_PROXY
          - 307 TEMPORARY_REDIRECT
          - 308 PERMANENT_REDIRECT
          - 400 BAD_REQUEST
          - 401 UNAUTHORIZED
          - 402 PAYMENT_REQUIRED
          - 403 FORBIDDEN
          - 404 NOT_FOUND
          - 405 METHOD_NOT_ALLOWED
          - 406 NOT_ACCEPTABLE
          - 407 PROXY_AUTHENTICATION_REQUIRED
          - 408 REQUEST_TIMEOUT
          - 409 CONFLICT
          - 410 GONE
          - 411 LENGTH_REQUIRED
          - 412 PRECONDITION_FAILED
          - 413 PAYLOAD_TOO_LARGE
          - 413 REQUEST_ENTITY_TOO_LARGE
          - 414 URI_TOO_LONG
          - 414 REQUEST_URI_TOO_LONG
          - 415 UNSUPPORTED_MEDIA_TYPE
          - 416 REQUESTED_RANGE_NOT_SATISFIABLE
          - 417 EXPECTATION_FAILED
          - 418 I_AM_A_TEAPOT
          - 419 INSUFFICIENT_SPACE_ON_RESOURCE
          - 420 METHOD_FAILURE
          - 421 DESTINATION_LOCKED
          - 422 UNPROCESSABLE_ENTITY
          - 423 LOCKED
          - 424 FAILED_DEPENDENCY
          - 426 UPGRADE_REQUIRED
          - 428 PRECONDITION_REQUIRED
          - 429 TOO_MANY_REQUESTS
          - 431 REQUEST_HEADER_FIELDS_TOO_LARGE
          - 451 UNAVAILABLE_FOR_LEGAL_REASONS
          - 500 INTERNAL_SERVER_ERROR
          - 501 NOT_IMPLEMENTED
          - 502 BAD_GATEWAY
          - 503 SERVICE_UNAVAILABLE
          - 504 GATEWAY_TIMEOUT
          - 505 HTTP_VERSION_NOT_SUPPORTED
          - 506 VARIANT_ALSO_NEGOTIATES
          - 507 INSUFFICIENT_STORAGE
          - 508 LOOP_DETECTED
          - 509 BANDWIDTH_LIMIT_EXCEEDED
          - 510 NOT_EXTENDED
          - 511 NETWORK_AUTHENTICATION_REQUIRED
      statusCodeValue:
        type: integer
        format: int32
    title: ResponseEntity
  SampleDataResponseDTO:
    type: object
    properties:
      eventSampledata:
        type: string
      links:
        type: array
        xml:
          name: link
          namespace: 'http://www.w3.org/2005/Atom'
          attribute: false
          wrapped: false
        items:
          $ref: '#/definitions/Link'
    title: SampleDataResponseDTO
  SchemaResponseDTO:
    type: object
    properties:
      eventSchema:
        type: string
      links:
        type: array
        xml:
          name: link
          namespace: 'http://www.w3.org/2005/Atom'
          attribute: false
          wrapped: false
        items:
          $ref: '#/definitions/Link'
    title: SchemaResponseDTO
  UpdateEventRequestDTO:
    type: object
    properties:
      eventDescription:
        type: string
        minLength: 1
        maxLength: 256
      eventSampledata:
        type: string
      eventSchema:
        type: string
      eventStatus:
        type: string
        pattern: ACTIVE|INACTIVE
      links:
        type: array
        items:
          $ref: '#/definitions/Link'
    title: UpdateEventRequestDTO
schemes:
  - https
