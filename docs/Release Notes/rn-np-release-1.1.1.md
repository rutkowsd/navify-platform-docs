# Release Notes

The release notes for platform 1.1.1

## Performance Enhancements

### Tenant MS

* Tenant Group caching introduced to improve OKTA rate limits. Cache time is configurable, default is 1 day.
* Internal API calls routed through traefik to improve performance
* Create Admin API enhanced to look for user groups in platform tables instead of OKTA. This improves performance and OKTA rate limits.

### Defects Fixed

#### Auth UI

* Clicking back button in username, password login window will redirect the user to the main login window
* Error handling for expired OTP code. Once the code expires, the user will have to start the login process again by clicking the back button in browser
* UI alignment issues in password reset window
* Support for a long email address ( current limitation was 50 characters)

#### Security

* RDS security certificate update as the current certificates expire on March 5 2020
* Audit log rotation fixed in AMI’s to avoid filling up of disk space

### Miscellaneous

* Login event audit message updated to log tenant alias and app alias in the entity description field. For missing tenant id, N/A will be logged in the facility field.
* Message attribute in JSON body updated with double quotes to return valid json response for error messages
* Fixed access for authorized users when using API for retrieving `appid` using `gatewayid` . ( API example: GET ['https://dev.api.navifytb01.tb01.rginger.com/platform/app-registry/apps/app?apiGatewayId=XXXX'](https://dev.api.navifytb01.tb01.rginger.com/platform/app-registry/apps/app?apiGatewayId=XXXX))
* Cache headers are added to login/logout/refresh lambda, so users can logout successfully every time.
* Access to user profile API for authorized users when the access token is provided in a cookie

### General Enhancements

* Support for TLS1.2 as lower version had security vulnerabilities.
* Support for communication using web-socket protocol ( link for details in DevX portal for details if available)
* New OKTA templates for Account locked, Password reset denied and Admin password reset scenarios.

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>