# Release Notes

The release notes for platform 1.1.1

## Major feature

Support for active directory (AD) integration using SAML 2.0 protocol which allows users to use their organization credentials to log into the platform.

> **NOTE:** AD integrated users cannot be used for testing platform api’s. These users will work only in UI.

## Enhancements

### Auth lib and Auth UI

* Supports customization to display product specific logo, name, brand, information and background.
* Supports Two-Factor Authentication during login
* Login event is audited.

### Tenant Management

A new endpoint that allows Platform Admin to retrieve a list of tenants subscribed for a given application. `[relevant for platform administrators]`

### Create tenant endpoint updates

* Rexis id changed from required to optional.
* Tenant Alias accepts only lower case letter, number and - (dash).
* Okta group is created using tenant Alias instead of  tenant uuid.
* Subscribes tenant to platform application.  `[relevant for platform administrators]`
* Links tenant to auth application in OKTA.  `[relevant for platform administrators]`

### User Management

* **New endpoint** ["List admin users"](https://docs.platform.navify.com/api-references/tenant-resources-apis/admin-user-management/getusersusingget) that allows Platform Admin to retrieve a list of Admin users for a given tenant.  `[relevant for platform administrators]`
* **New endpoint** to retrieve a ["List of users and their roles for a given application".](https://docs.platform.navify.com/api-references/tenant-resources-apis/role-management/getalluserrolesforappusingget)
* Tenant Admin can now create, update or retrieve details of another Tenant Admin.
* **New endpoint** to ["Retrieve current user profile information".](https://docs.platform.navify.com/api-references/tenant-resources-apis/user-management/getcurrentuserusingget)
* Create User endpoint enhance to create federated user (AD integration).
* All endpoints updated to return Okta status and federated status in the response body.

### Role Management

* ["Me" API endpoint](https://docs.platform.navify.com/api-references/role-app-ui-resources-apis/role-api-for-authorization-and-ui/getuirolesusingget) updated to return tenant Uuid and AppUuid in the response body.
* Platform Admin can assign or delete only Tenant Admin role for a user. `[relevant for platform administrators]`
* Tenant Admin can assign a role to a user only if Tenant is subscribed to that application.
* Assign role endpoint updated to include role name in url. If the request body is provided it will be ignored to maintain backward compatibility.

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>