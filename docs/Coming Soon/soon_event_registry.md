# Platform Event Registry Service

## [Coming soon to platform]

The Event Registry will provides a central repository of events and their schemas that allows applications hosted on the platform or the data exchange streaming pipeline to integrate with each other. It will provide a RESTful interface for applications to store and retrieve event schemas. The Event Registry allows platform hosted applications to register what event schemas they will publish (event producers) and the event schemas they will subscribe to (event consumers). The event schema structure will be defined in the JSON format.

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>