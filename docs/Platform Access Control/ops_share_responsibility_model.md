# Shared Responsibility Model

NAVIFY Platform is not an island, it is a foundation that Products rely on. Much of the Platform team’s work involves the Product teams, either because the work affects them, or because their work is needed. But who has responsibility to resolve a given issue?

In general, if the issue affects the wider operation of the platform, resolution is led by the Platform team. If it is specific only to a single Product, the product team leads. The primary goal of the Platform Team is to ensure the security and operations of the NAVIFY platform. The secondary goal is to serve as partners in achieving the goals of the Product Teams.

The tables below provide high level presentation of the shared responsibility model. More detailed can be found along the specific process or tool documentation.

## DevOps

| Topic / Process | Platform | Product | Shared Model Type | Ownership |
| --- | --- | --- | --- | :---: |
| **Backup & Recovery (HA)** | Design and operate the architecture for platform micro-services High Availability  | Design and operate the architecture for product High Availability | Platform & Product execute for their environment | Shared |
| **Disaster Recovery** | Design and operate the architecture for platform micro-services Disaster Recovery (incl. periodic Disaster Recovery testing) | Design and operate the architecture for product Disaster Recovery (incl. periodic Disaster Recovery testing) | Platform & Product execute for their environment | Shared |
| **IAM Product** | Platform Team provides Admin role to a Product | Product Team provisions the users (Roche Eng roles) | Platform & Product execute for their environment | Shared |
| **IAM (infrastructure)** | Responsible for configuring SSO (and therefore compliance to Roche users lifecycle) for related vendors IAM stacks (AWS, Okta, Sumologic, Cloudflare, Dome9...)<br /><br />Responsible for ensuring only Roche SSO users can access the underlying vendors system (AWS, Dome9...)<br /><br /> Responsible for ensuring AWS users are all authenticated towards one single AWS account using role delegation for accessing other accounts in the same AWS Org<br /><br /> Responsible for generating access tokens for platform and product users through the NAVIFY Platform IAM stack | Responsible for consuming identity and access tokens issued by the NAVIFY Platform backend services. Product team requests Platform team to provision accounts | N/A | Platform |
| **Key Management** | Responsible to manage key lifecycle, key storage, key access for keys within platform environments | Responsible to manage key lifecycle, key storage, key access for keys within Product environments | Platform & Product execute for their environment | Shared |
| **OS Hardening** | Responsible for engineering and maintenance through CI/CD of hardened AMIs for Linux operating system including by default Dome9, ClamAV and SumoLogic log collection inclusion | Design for automating consumption of the latest platform AMIs for plain and ECS EC2 instances and operate and monitor the consumption of the latest hardened AMIs | N/A | Platform |
| **Patch Management** | Platform updates AMIs per release schedule and OS hardening criteria. Also apart of vulnerability management assessments via Inspector, Nessus or Qualys to detect vulnerabilities introduced by our software on top of the hardened AMI, or new CVEs.<br /><br />Critical vulnerabilities are addressed by updating the hardened AMI and pushing a release. Non-critical vulnerabilities are addressed per our AMI release schedule. | Product team determines release schedule for platform released patches Vulnerability Management<br /><br />Product team has to apply patches to Application Level | Platform & Product execute for their environment | Shared |
| **Incident Response** | Contact Product team (Security Champion) considering if it is a Product related Incident | Product team is responsible for the incident, there MUST be a Incident Response plan in place | Platform & Product executes for their environment | Product |
| **Infrastructure Monitoring** | Platform team monitors the Platform Infrastructure | Application team monitors their own Infrastructure | Platform & Product execute for their environment | Shared |
| **Problem Management** | Collaboration between Platform and Product teams | Collaboration between Platform and Product teams | Platform & Product collaborate | Shared |
| **Architecture Design** | | | | |
| **Third Party Account Management** | Platform Team provisions and manage the lifecycle of AWS Accounts | N/A | N/A | Platform |
| **Capacity Planning** | Platform provides the cost transparency in terms of AWS costs consumption |  N/A | N/A | Platform |
| **DevOps Automation** | Platform provides DevOps Automation for Platform infrastructure and services | Product provides DevOps Automation for Product infrastructure and services | Platform & Product execute for their environment | Shared |
| **Environment Deployment** | Platform does for their own infrastructure | Product does for their own infrastructure | N/A | Product |
| **Lambda** | Platform does for their own infrastructure | Product does for their own infrastructure | N/A | Product |
| **Database Services** | Platform does for their own infrastructure | Product does for their own infrastructure | N/A |Product |
| **Application Monitoring** | NA | Product teams monitors their application | N/A | Product |
| **Log Management & Auditing** | Platform Team provides Cyber Security capabitlites (Dome9, SumoLogic, CloudFlare, AMIs ) | Product team manages Application Logs | Platform & Product execute for their environment | Shared |
| **Infrastructure Change Management** | Platform does for their own infrastructure | Product does for their own infrastructure | N/A | Product |
| **Application Change Management** | N/A | Product team manages application changes | N/A | Product |
| **Business Continuity** | Platform has business continuity plan for their own services | Product has business continuity plan for their own services | N/A | Product |
| **Education, Training & Awareness** | Platform team trains Platform Cyber Security team | Platform team shares a baseline but Product team has to train their own cyber security teams | Platform & Product execute for their environment | Shared |
| **Third party assurance** | Platform has their tools qualification for DevSecOps toolchain | Any tools outside Platform toolchain is product team responsibility | Platform & Product execute for their environment | Shared |

## SecOps

| Topic / Process | Platform | Product | Shared Model Type | Ownership |
| --- | --- | --- | --- | :---: |
| **Perimeter Defense** | Configuring Platform back-end micro-services and/or API Gateway to leverage Cloudflare Perimeter defense<br /><br />Responsible for configuring CloudFlare logs collection by SumoLogic | Design Product back-end micro-services and/or AWS API Gateway to leverage platform setup of Cloudflare Perimeter defense<br /><br />Operate this implementation (exceptions, metrics...) | Platform executes based on product inputs | Shared |
| **Data Encryption** | Responsible for configuring AWS KMS for platform back-end services<br /><br />Responsible for ensuring back-end services encryption in transit<br /><br />Responsible for ensuring tenants and user data encryption at rest | Responsible for configuring AWS KMS for product back-end services<br /><br />Responsible for ensuring back-end services encryption in transit<br /><br />Responsible for ensuring data at rest for data persisted within product environments | Platform and Product executes for their environments | Shared |
| **Antivirus** | Platform performs deployment and management of the antivirus agent, as well as antivirus scanning. Baked into the AMIs | N/A | N/A | Platform |
| **Vulnerability Management** | Current - Regular scanning of defined AMIs,using AWS Inspector and Qalys<br /><br />Future - Platform planned to use SysDig for container Security which supports vulnerability scanning in CI/CD | Current- Product is responsible for continues monitoring and vulnerability management process (Product Playbook required) | Platform and Product executes for their environments | Share- Product team monitors |
| **Compliance Monitoring** | Platform team configures the compliance dashboard for Product<br /><br />Platform team runs the account scans on regular basis for Product. | Customize the dashboard, monitor the alerts and remediate as needed | Platform configures, Product monitors | Shared |
| **Data Locality** | | | | |
| **Security Monitoring** | Platform Environments and Infrastructure are monitored by the Platform Cyber Security and Roche MIR team | Applicaton code , quality , security etc. | Platform and Product executes for their environments | Shared |
| **Intrusion Detection & Response** | Platform installs an IDS agent on all instances (using FireEye HX) and get alerted by Roche MIR team | Product informs Security Champion | N/A | Product |
| **Application Security** | Platform team provides Cyber Security capabilities (Dome9, SumoLogic, CloudFlare, AMIs ) | Product teams monitor their application | Platform and Product executes for their environments | Shared |
| **Penetration test** | Platform team provides regular pen testing for platform servies | Product team can request to Platform team for pen testing | Platform and Product executes for their environments | Shared |
| **Penetration Testing**<br />(proactive, perimeter vulnerability assessment) | Platform team provides regular pen testing for platform servies | Product team can request to Platform team for pen testing | Platform and Product executes for their environments | Shared |
| **Data Center Physical & environmental security** | N/A | N/A | N/A | N/A |
| **Legal & Contractual Obligations** | Platfrom team has BAA agreement with AWS, SumoLogic | N/A | N/A | Platform |
| **Risk Management** | Platform for their own services | Product for their own services | Platform and Product executes for their ENV | Product |
| **Information Protection Program** | N/A | N/A | N/A | N/A |
| **Cyber Liability Insurance** | N/A | N/A | N/A | N/A |

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
