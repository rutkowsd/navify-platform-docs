# Platform - Tenant Management

The NAVIFY Platform provides the ability to expose applications to multiple tenants with various levels of privileges.  For this purpose, we expose a Platform Tenant Management service that allows administrators to manage tenants and users on the platform. Basically, it allows for grouping of users under a tenant, where tenants subscribe to an application. Think of it as a marketplace, where each Tenant can look at the applications offered by Roche and then choose to subscribe to one.

Tenant APIs: provide CRUD operations for tenant

User APIs: provide CRUD operations for a user and map it to tenant and role.

## The tenant management implements the following roles

* **PlatformAdmin** role that allows:

  * Create a new tenant in the platform. Each tenant will have at least one Tenant Administrator.
  * Subscribe/Unsubscribe tenant to an application.

  This is currently handled by the platform genius role so please contact your friendly neighborhood Platform Genius

* **TenantAdmin** role that allows (see Tenant Administrator in [terminology](https://docs.platform.navify.com/guides/navify-platform/terminology)):
  * Create/Register new users within their own tenancy.

  This is a role that will be created for administrators of that tenant. They will have the ability to manage users for that tenant.

## Tenant Management API

|Verb | Action | User | Release |
| --- | --- | --- | --- |
| GET | Get Tenant Details | TenantAdmin, PlatformAdmin | 1.0 |
| PUT | Update Tenant Info | TenantAdmin, PlatformAdmin | 1.0 |
| GET | Get list of tenants | PlatformAdmin | 1.0 |
| POST | Create Tenant and subscribe the tenant to platform app | PlatformAdmin | 1.0 |
| DELETE | Delete Tenant | PlatformAdmin | 1.1 |
| PUT | Disable Tenant | PlatformAdmin | 1.x |
| GET | Get a list of tenants subscribed to the given app | PlatformAdmin | 1.1 |

## Tenant Application Management API

|Verb | Action | User | Release |
| --- | --- | --- | --- |
| GET | List Apps Tenant is subscribed to | PlatformAdmin | 1.0 |
| POST | Subscribe tenant to an application | PlatformAdmin | 1.0 |
| DELETE | Unsubscribe tenant from an application | PlatformAdmin | 1.0 |

## Tenant User Management API

|Verb | Action | User | Release |
| --- | --- | --- | --- |
| GET | Get users details by userid | TenantAdmin | 1.0 |
| PUT | [Update User Info](https://docs.platform.navify.com/api-references/tenant-user-mngt/user-api-for-tenant/updateuser) | TenantAdmin | 1.0 |
| DELETE | [Delete User](https://docs.platform.navify.com/api-references/tenant-user-mngt/user-api-for-tenant/deleteuser) | TenantAdmin | 1.0 |
| GET | Get a list of users for given tenantId | TenantAdmin | 1.0 |
| POST | Create User(s) under given Tenant | TenantAdmin | 1.0 |
| GET | Get an admin user by userid | TenantAdmin | 1.0 |
| PUT | Update the admin user | TenantAdmin | 1.0 |
| GET | Get admin user details | TenantAdmin, PlatformAdmin | 1.1 |
| GET | Get a list of admin users for a given tenant | TenantAdmin, PlatformAdmin | 1.1 |
| POST | Create New Admin User </br> TenantAdmin can create Admin users in 1.1| TenantAdmin, PlatformAdmin | 1.0 |
| GET | Get current user information </br> Retrieve current user's detailed information based on user external Id| TenantAdmin | 1.1 |
| DELETE | Delete Admin User | PlatformAdmin | 1.1 |

### Key considerations

* To preserve privacy, we only store information required to identify a user in the Identity Provider (IdP).
* Currently, Okta is used as the default IdP system to authenticate a user
* An Okta group shall be set up and mapped to each Platform Tenant referencing the Tenant UUID (`Tenant Alias` starting from release 1.1). There is an OAuth app in OKTA which is assigned to each group.

  Create Group: com.okta.sdk.resource.group.GroupBuilder

  An Okta user shall be set up and mapped to a Platform User - a Platform User will be identified by a composite key  { email + tenant_uuid }.

  Create User: com.okta.sdk.resource.user.UserBuilder

  An Okta user may belong to one or multiple Okta group/Platform Tenant.

  Product application should not access Okta directly, every interaction with Okta should be done via the platform services.

If required, Multi-Factor Authentication (MFA) can be configured and handled in Okta for that tenant.

### ACL definitions

Access will be controlled by

* Claims included in user token: Platform Authorization service will generate claims based on user role and API Gateway will only allow users with PlatformAdmin claims to hit any of Tenant API.

* Different APIs will require a different set of claims (e.g. for PlatformAdmin).

### Federated External IdPs (Comming with version 1.1 of Platform)

The Okta Identity Providers provides operations to manage federations with external Identity Providers (IdP). This will allow platform users to use their organization credentials to login into platform using SAML 2.0 protocol. At a high-level, the authentication flow of SAML looks like this

[![Federated IdP SAML flow](https://s3.amazonaws.com/user-content.stoplight.io/18223/1567160159082)](https://s3.amazonaws.com/user-content.stoplight.io/18223/1567160159082)

### Federated user use cases (Comming with version 1.1 of Platform)

After configuring the idp and routing rules in okta the federated user can be managed in the following ways

1. Add new federated user
   * User exists in external idp hospital/facility.
   * Create new user in the same facility.
   * If the routing rule email domain matches the new user email domain then federated user is created.
   * During login the user will be redirected to external idp login page.
   * User can access applications to which they have a role assigned to them.

2. Convert existing user to federated user
   * User exists in external idp hospital/facility.
   * If the routing rule email domain matches the new user email domain then user is federated user.
   * During login the user will be redirected to external idp login page.
   * User can access applications to which they have a role assigned to them.

3. Converting federated user to user
   * User may or may not exist in external idp hospital/facility.
   * Deactivate the corresponding routing rules.
   * Deactivate and reactivate the user in okta to allow the users to set the password and security questions.
   * During login the user will be asked to enter the password in platform login page.
   * User can access applications to which they have a role assigned to them.

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
