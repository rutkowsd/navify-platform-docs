# Tenant User Activation

Any time you create a user through the platform APIs a user will be notified via email, that a new account on the NAVIFY Platform has been created for them. The tenant user activation process consists of manual steps taken by the end-user registered under that single tenant.

><p class="info_notes" style="color:#0066cc;"><b>NOTE: </b>The user has 7 days until the link in the email expires.</p>

The email received by the end user will look like this:

![add activation email](https://s3.amazonaws.com/user-content.stoplight.io/18223/1578036251278)

Once they select the `“Activate your account”` button they will be navigated to a page where they will set their password, security question (in case of a forgotten password), and a security image.

![add setup image](https://s3.amazonaws.com/user-content.stoplight.io/18223/1578036307931)

They can then continue with the activation of the account by clicking on the `“Create My Account”` button.

They will be navigated to a page informing them that their account was successfully created and they can close the page continuing to their preferred application.

![add success activation image](https://s3.amazonaws.com/user-content.stoplight.io/18223/1578036326105)

## In the case where multifactor authentication (MFA) policy is enabled for this tenant

The user will be asked to set up their Multifactor Authentication (MFA) by entering their phone number and selecting `“Send Code”`.

![add verifcation code image](https://s3.amazonaws.com/user-content.stoplight.io/18223/1578037418553)

Once their code has been entered and verified, they will be signed into the Roche/Okta platform and allowed to log in to their application.

## Exceptions (only applicable when Okta is primary IdP)

### Not activating the account within 7 days or user account had already been activated

If 7 days have already passed or if the user tries to re-activate the account after already creating it, they will receive an error that their Token has Expired and they must request a new one (sample shown below).  

![token expired](https://s3.amazonaws.com/user-content.stoplight.io/18223/1578036344975)

### Forgotten password

In the case that the user forgets their password, they will be asked for their security question/answer and/or security image to verify their account information. If they are unable to answer correctly, steps will need to be taken to deactivate and reactivate their account (requires Okta administrator involvement).

### User did not receive an activation email

Usually, this happens when an email or username is entered inappropriately. Make sure that they are correct by checking the information in Okta (requires Okta administrator involvement).

### User is locked out

This will happen when the user has surpassed the number of maximum login attempts. In such cases a password reset is needed (requires Okta administrator involvement). Once a password reset is initiated the user will receive an email with steps to reset their password (must be completed within 7 days of receiving).  

### User forgot their password

The user has the option to reset their password by selecting “Forgot your Password?” which will then request their email address.  They will then be sent an email with steps to reset their password.

### User forgot both password and security question

In this case, the user must contact Support directly and request that their account be reactivated (requires Okta administrator involvement).

### The facility is enrolled in MFA but the user did not receive MFA enrollment in the activation page

This may happen if the facility was not properly enrolled as MFA or if the user was activated before the MFA policy was enabled for that facility(requires Okta administrator involvement). Users will need to contact the support team.

### Facility is enrolled in MFA but user is unable to receive OTP code during login

This could mean that the incorrect phone number was entered during user-creation or that the incorrect phone number was entered during user-activation (requires Okta administrator involvement).

## Read more

* [User Management](https://docs.platform.navify.com/guides/platform-access-control/user-management)
* [Guide: How to use User Management API Endpoints](https://docs.platform.navify.com/guides/ac-guides/hto-user-mngt)
* [OKTA Support: End user FAQ](https://support.okta.com/help/s/article/End-User-FAQ)

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>