# Customizing the Auth UI

Not all applications living on the NAVIFY Platform will/or want to use the default login user interface (UI) provided with our Auth library. Therefore, our auth library provides the capability to customize the Authentication UI, by passing the customization elements as a parameter call when instantiating the Auth object from the `@dia/auth` library.

### Auth(arg0, arg1) constructor

This is the main object form '@dia/auth' NAVIFY Auth Lib.

The first parameter `arg0` is used to provide the IdP related information. You need this to point to the correct instance of the Platform IdP provider Okta, and the configured platform and Auth UI URLs.

The second parameter `arg1` allows you to change the default behaviour of the library by providing customization to the different elements.

### AuthConf configuration

Import the `AuthConf` from `@dia/auth` and use the following structure which you can later pass as `arg1` in your code.

```javascript
import { AuthConf } from '@dia/auth';

const authConf: AuthConf = {
    provider: {
        type: string, // IDP type (e. g. "okta")
        url: string, // IDP instance URL
        clientId?: string, // optional IDP client identifier
    };
    platform: {
        url: string, // Navify Platform URL
    };
    app: {
        url: string, // Auth-UI URL
    };
}
```

### AuthOptions options

Import the `AuthOptions` and use the available variables to define and customize the Auth login UI.

```javascript
import { AuthOptions } from '@dia/auth';

const authOptions: AuthOptions = {
  sessionExpirationBeforeTime: number, // how much time before expiration should the library emit the BEFORE_SESSION_EXPIRATION event (milliseconds)
  sessionPollingIntervalTime: number,  // how often should the library send a polling request to check the existing session (milliseconds)
  redirectUrlUseHash: boolean,         // flag deciding whether to use URL hash instead of query parameters
  identificationFormSkip: boolean,     // flag deciding whether to force skip the identification step
  customPageTitle: string,             // `title to show on top`
  customBackgroundStyle: string,       // `{css for background properties}`
  customHeader: string,                // `<div>Custom sanitized HTML</div>`
  customHeadline: string,              // `<div>Custom sanitized HTML</div>`
  customCaption: string,               // `<div>Custom sanitized HTML</div>`
  customFooter: string,                // `<div>Custom sanitized HTML</div>`
};
```

The customization options support HTML. The values are sanitized before being rendered in the Auth UI application. All inline CSS is removed in the process. You can make use of predefined styles for:

* img
* sup
* .left
* .center
* .right
* .separator
* .bigger
* .big
* .small

### Auth(arg0, arg1) instantiation

With the configuration object you can now create an instance of authentication implementation:

```javascript
import { Auth } from '@dia/auth';

const auth = new Auth(authConf, authOptions);
```

#### Example of providing customization to 5 different elements on the Auth UI login page

Since the HTML is sanitized, we may not provide inline style. However, you may provide `<img>` tag in the sanitized HTML to display customized text.

Here is an example:

```javascript
customHeader:
`<div class=”right”>
	<a href=”http://roche.com”>
		<img src=”${rocheLogoImageSrc}” class=”big” />
	</a>
</div>`
```

Background style uses CSS definition for the property background, example:

```javascript
customBackgroundStyle:
    `url(“${landingPage}”) center no-repeat / cover`,
```

Specify the location of header, footer, & caption using the following class

* left
* right
* center

Change size of `<img>` with the following class

* bigger
* big
* small

You may make a separator by adding separator to a <span> tag, example:

```html
<span class="separator big"></span>
```

### Some live examples

* Platform - Hello World App: (click the login button)  
    [https://www.helloworld-demo-kau.platform.navify.com/appdev-demo-tenant-9-alias](https://www.helloworld-demo-kau.platform.navify.com/appdev-demo-tenant-9-alias)

* Platform - Demo Application (click the login button)  
[https://npdemo.demoapp.npappperf.platform.navify.com/home](https://npdemo.demoapp.npappperf.platform.navify.com/home)

## Read more

* [Authentication UI & Library](https://docs.platform.navify.com/guides/platform-access-control/pac-authentication-intro)
* [How uo use Authentication Library](https://docs.platform.navify.com/guides/ac-guides/how-to-use-auth-lib)

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>