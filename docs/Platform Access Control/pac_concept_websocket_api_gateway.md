# Platform Product Authorization Support For AWS WebSocket API gateway

## Introduction

AWS supports WebSocket API gateway.  Products can use WebSocket API gateway to send/receive messages to and from the server backend, e.g. pushing a notification to a client.  Just like the HTTP API gateway, the WebSocket API gateway also supports custom authorizer for authorizing a WebSocket connection. Unlike the HTTP API gateway, the authorization here is done per connection instead of per-message request. Platform product authorizer has been enhanced to support WebSocket authorization requests.

## Platform Support

The current platform product authorizer checks the user has permission to access the application then generates and returns an authorization policy along with a backend token. The backend token contains the user roles and claims. For Supporting WebSocket protocol, the authorizer has been enhanced to:

* Look for the required tenant alias in the cookie. This is done because the current limitation in the native JavaScript connection API does not support passing custom header. So the client has to set the tenant alias `X-Navify-Tenant` in the cookie.

* Recognize WebSocket requests and generate the appropriate authorization policy.

## Product WebSocket API gateway setup

The product now can setup its own WebSocket API gateway in their account and have the authorizer invoke platform product authorizer lambda.  The AWS WebSocket API gateway provides three default routes, $connect, $default and $disconnect.  For more information on AWS WebSocket API gateway, please refer to [https://docs.aws.amazon.com/apigateway/latest/developerguide/apigateway-websocket-api.html](https://docs.aws.amazon.com/apigateway/latest/developerguide/apigateway-websocket-api.html).

The authorizer can only be configured in the `$connect` route request section.

![add image01 here](https://s3.amazonaws.com/user-content.stoplight.io/18223/1581347721648)

![add image02 here](https://s3.amazonaws.com/user-content.stoplight.io/18223/1581347742067)

Please note that invoking platform product authorizer requires cross-account permission. This will be part of the product on-boarding process.

In addition to configuring the custom authorizer in the `$connect`, the product can also configure handler in the request integration section to have application logic such as persist the connection id and user in DB.

![add image03 here](https://s3.amazonaws.com/user-content.stoplight.io/18223/1581347760563)

Additional application logic to handle `$disconnect` and `$default` request messages can be configured as well.

Once the WebSocket API gateway is created, it’s crucial to create a custom DNS name with the same domain as the web application so the secure cookies can be shared.  
For example, [maven.upath.platform.navify.com](maven.upath.platform.navify.com) is the uPath webapp. The uPath WebSocket DNS name would be [upath-websocket.upath.platform.navify.com](upath-websocket.upath.platform.navify.com).

![add image04 here](https://s3.amazonaws.com/user-content.stoplight.io/18223/1581347772477)

For cloudformation example, please see [https://bitbucket.org/rochedis/navify-platform-demo-websocket/src/master/](https://bitbucket.org/rochedis/navify-platform-demo-websocket/src/master/)

```yaml

#shameless cribbed from https://github.com/aws-samples/simple-websockets-chat-app/blob/master/template.yaml
AWSTemplateFormatVersion: '2010-09-09'
Description: A simple websocket for testing purposes

Parameters:
  Rochestack:
    Type: String
    Description: Rochestack name of this deployment
  AuthorizerUri:
    Type: String
    Description: The invocation ARN for the Authorizer Lambda.

Resources:
  WebsocketApi:
    Type: AWS::ApiGatewayV2::Api
    Properties:
      Name: !Sub "${Rochestack}-demo-ws-mock"
      ProtocolType: WEBSOCKET
      RouteSelectionExpression: "$request.body.message"

  Authorizer:
    Type: AWS::ApiGatewayV2::Authorizer
    Properties:
      Name:
        Fn::Join:
        - ""
        - - !Ref Rochestack
          - "-demo-ws"
      ApiId: !Ref "WebsocketApi"
      AuthorizerUri: !Ref AuthorizerUri
      AuthorizerType: "REQUEST"
      IdentitySource:
        - "route.request.header.Cookie"
      #IdentitySource: "method.request.header.Authorization,method.request.header.X-Navify-Tenant"

  DefaultRoute:
    Type: AWS::ApiGatewayV2::Route
    Properties:
      ApiId: !Ref WebsocketApi
      RouteKey: $default
      AuthorizationType: NONE
      OperationName: DefaultRoute
      Target: !Join
        - '/'
        - - 'integrations'
          - !Ref DefaultInteg
  DefaultInteg:
    Type: AWS::ApiGatewayV2::Integration
    Properties:
      ApiId: !Ref WebsocketApi
      Description: Default Integration
      IntegrationType: MOCK
      TemplateSelectionExpression: 200
      RequestTemplates:
        "200": '{"statusCode" : 200, "connectionId" : "$context.connectionId"}'
  DefaultIntegResp:
    Type: AWS::ApiGatewayV2::IntegrationResponse
    Properties:
      ApiId: !Ref WebsocketApi
      IntegrationId: !Ref DefaultInteg
      IntegrationResponseKey: "$default"
      #ResponseParameters: Json
      ResponseTemplates:
        "200": '{"statusCode" : 200, "connectionId" : "$context.connectionId"}'
      TemplateSelectionExpression: "${integration.response.statuscode}"

  ConnectRoute:
    Type: AWS::ApiGatewayV2::Route
    Properties:
      ApiId: !Ref WebsocketApi
      RouteKey: $connect
      AuthorizationType: CUSTOM
      AuthorizerId: !Ref "Authorizer"
      OperationName: ConnectRoute
      Target: !Join
        - '/'
        - - 'integrations'
          - !Ref ConnectInteg
  ConnectInteg:
    Type: AWS::ApiGatewayV2::Integration
    Properties:
      ApiId: !Ref WebsocketApi
      Description: Connect Integration
      IntegrationType: MOCK
      TemplateSelectionExpression: 200
      RequestTemplates:
        "200": '{"statusCode" : 200}'
  ConnectIntegResp:
    Type: AWS::ApiGatewayV2::IntegrationResponse
    Properties:
      ApiId: !Ref WebsocketApi
      IntegrationId: !Ref ConnectInteg
      IntegrationResponseKey: "$default"
      #ResponseParameters: Json
      #ResponseTemplates: Json
      TemplateSelectionExpression: "${integration.response.statuscode}"

  Deployment:
    Type: AWS::ApiGatewayV2::Deployment
    DependsOn:
    - ConnectRoute
    #- SendRoute
    #- DisconnectRoute
    Properties:
      ApiId: !Ref WebsocketApi
  Stage:
    Type: AWS::ApiGatewayV2::Stage
    Properties:
      StageName: Prod
      Description: Prod Stage
      DeploymentId: !Ref Deployment
      ApiId: !Ref WebsocketApi


```

> Please note the test script in the repo is using a standalone tool wscat to test the WebSocket API gateway. The focus of the test is to exercise the platform product authorizer.

```bash

#!/usr/bin/env bash
#set -x

ADDRESS=$1
TENANT=$2

OKTA_DOMAIN=$3
CLIENT_ID=$4

REDIRECT_URL=$5

USERNAME=$6
PASSWORD=$7

echo "Getting okta token..."

SESSION_URL="$OKTA_DOMAIN/api/v1/authn"
AUTHORIZE_URL="$OKTA_DOMAIN/oauth2/default/v1/authorize"

# STATE:
#{
#  "providerType": "okta",
#  "returnBy": "postMessage"
#}
STATE="ewogICJwcm92aWRlclR5cGUiOiAib2t0YSIsCiAgInJldHVybkJ5IjogInBvc3RNZXNzYWdlIgp9"


RESPONSE=`curl --fail -s -X POST -H "Content-Type: application/json" -d "{\"username\": \"$USERNAME\", \"password\": \"$PASSWORD\"}" $SESSION_URL`
if [ $? != "0" ]; then
    echo "failed getting session token"
    exit 1
fi
SESSION_TOKEN=`echo $RESPONSE | jq -r ".sessionToken"`
echo "session token: $SESSION_TOKEN"


TOKEN=`curl --fail -s -c - -L -G -d "client_id=$CLIENT_ID&response_type=code&prompt=none&scope=openid%20offline_access%20profile&redirect_uri=$REDIRECT_URL&sessionToken=$SESSION_TOKEN&state=$STATE" $AUTHORIZE_URL | grep 'access_token\t.*' | awk -F$'\t' '{print $7}'`
if [ $? != "0" ]; then
    echo "failed logging in"
    exit 1
fi

COOKIE_STRING="access_token=$TOKEN"
echo "cookie: $COOKIE_STRING"

wscat -x "test" -H "Cookie:$COOKIE_STRING" -H "X-Navify-Tenant:$TENANT"  -c $ADDRESS

```

`wscat -x "test" -H "Cookie:$COOKIE_STRING" -H "X-Navify-Tenant:$TENANT"  -c $ADDRESS`

Where:

* Cookie_string contains the access_token
* X-Navify-Tenant is the tenant alias
* Address is the websocket api gateway connect url, 
  
  e.g.  `wss://upath-websocket.upath.platform.navify.com`

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>