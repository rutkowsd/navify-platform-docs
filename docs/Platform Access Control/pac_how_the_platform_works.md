# How the platform works

## How Requests Flow Through The System

APIs on the platform consist of two API Gateways.

1. **The Platform Gateway**, colloqually known as the "Front Door", which lives in our Platform Services Account
2. **The Product Gateway**, which lives in your Product Account

All requests for all products come to the Front Door, so we can centrally protect against DDoS or other types of attacks. After that initial validation has been done, the Front Door proxies the request to your Product Gateway. Your Product Gateway takes that request, does some more validation, adds a JWT hearder, and finally proxies that call to your private backend service.

It looks a little something like this:

![Diagram - How the platform works](https://s3.amazonaws.com/user-content.stoplight.io/18223/1555074611416 "Diagram - How the platform works")

The details of this functionality are explored a more in-depth fashion in the following sections.

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>