# User Management

This page provides you the key concepts about User Management. Complete guide on how to use the API endpoints is provided on a dedicated page:

- ["How to Use the User Management APIs"](https://docs.platform.navify.com/guides/ac-guides/hto-user-mngt)

## Roles and Accountabilities

The accountability of managing users for a given tenant belongs to the role of **Tenant Administrator(s)**. In scope of the on-boarding process a default **tenant** and related **tenant admin** user are created.

You can think of the **tenant administrator** as the person who works at the single Lab or Clinic and manages access to your application, adding or removing new users (lab technicians, medical doctors, etc.) that work in that specific Lab or Clinic.

## User and multi-tenancy

An user (one single email address) is always unique on the platform but can be member of different tenant. He/She has a single set of credentials. Tenant admin has the possiblitiy to assign different roles per user for different tenant and product.

- Read more about [Multi-tenancy](https://docs.platform.navify.com/guides/navify-platform/multi-tenancy).

## Creation of new Tenant

Users within a tenant can be managed by the tenant administrator using our API endpoints but the creation of a new tenant needs to be requested to our [Support Team](https://navifypoc.atlassian.net/servicedesk/customer/portal/1/group/-1).

## User management

See the page ["How to Use the User Management APIs"](https://docs.platform.navify.com/guides/ac-guides/hto-user-mngt) to find a complete guide on how to use our different endpoints.

### New user and activation

Depending on the product or applications approach to integrate with our service, whether it’s Federated access through SAML, or using the OpenID connect flow with the platform’s IdP. It is more than likely there will be some sort of activation process of Tenant Users. Since we only managed end-to-end the second approach, we provide you a more detailed step by step flow in the [Tenant User Activation guide](). In case the user already exists in some other tenant, the user will not need any further activation step.

### Delegating tenant administrators activities

You can delegate the tenant administration activities to another user or to a deputy by creating a new tenant administrator user for your tenant.

## Read more

* [Guide: How to use User Management API Endpoints](https://docs.platform.navify.com/guides/ac-guides/hto-user-mngt)
* [Multi-tenancy Introduction](https://docs.platform.navify.com/guides/miscellaneous/multi-tenancy)
* [Tenant Management Concept](https://docs.platform.navify.com/guides/platform-access-control/tenant-management)
* [Roles Management Concept](https://docs.platform.navify.com/guides/platform-access-control/role-management)

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
