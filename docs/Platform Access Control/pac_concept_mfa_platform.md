# Multi-Factor Authentication provided by Platform

The NAVIFY Platform allows tenants (a.k.a facilities, hospitals, clinics, etc.) to enable Multi-Factor Authentication (MFA) for their tenant users. The MFA is based on SMS verification (text messages), where an OTP code is sent to a mobile phone of the tenant user during the login process. Since MFA is enabled at the tenant level, any application that the tenant is subscribed to will require MFA to log in. Product teams can request MFA for a tenant via the [support center]().

> <p class="info_notes"><b>NOTE: </b>This feature is only available when tenants have not configured other IdP sources as their main IdP provider and are sticking to the Okta IdP used by the platform.</p>

*Sample SMS verification code*

![sms-auth-text](//s3.amazonaws.com/user-content.stoplight.io/18223/1576490992223)

## Complete tenant user profile

MFA requires that a tenant user is active (i.e. has gone through the steps to activate their user profile) and the profile is complete with the tenant user’s mobile phone. This means that any user going through the activation process must provide a valid mobile phone number.

## Platform APIs and MFA

The platform does not offer an API endpoint to manage MFA, however, our Platform Authentication UI is configured to support this 2FA via SMS (text message). If the consuming application is using our authentication library (and UI elements) then the activated user would be able to see the 2 factors every single time (SMS + username/password)

## Read more

* [Concept User Management](https://docs.platform.navify.com/guides/platform-access-control/user-management)
* [User Activation Steps]()

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
