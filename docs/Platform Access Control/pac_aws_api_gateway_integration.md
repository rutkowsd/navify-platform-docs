# AWS API Gateway Integration

## Platform Authorizer

The first step in the request chain, the Front Door, will take an HTTP request, find the Okta token on it (preferably from a HttpOnly cookie, but we also support finding it on the Authorization header), and validate this JWT. If it’s a valid token, we forward the request to the particular Product Gateway.

## Product Authorizer

Products on the platform utilize a central  to provide requests with an identity and authorization context. We call this authorizer the **Product Authorizer**. API Gateway endpoint that utilizes the Product Authorizer will have its Okta validated, and have its corresponding Platform Key returned in the authorizer context. The lambda that powers this authorizer lives in the central Platform Services Account, so there’s no additional code for you to deploy or maintain. Just point your authorizer to use our lambda.

Here’s an example authorizer from our API Gateway Cloudformation template

``` yaml
Authorizer:
  Type: AWS::ApiGateway::Authorizer
  Properties:
    Name:
      Fn::Join:
      - ""
      - - !Ref ApiName
        - "-authorizer"
    RestApiId: !Ref MyRestApi
    AuthorizerUri: !Ref AuthorizerLambdaUri
    Type: "REQUEST"
    IdentitySource: "method.request.header.Authorization"
```

## Backend Service Integration

[AWS: Set up HTTP Integrations in API Gateway](https://docs.aws.amazon.com/apigateway/latest/developerguide/setup-http-integrations.html)

On methods in your Product Gateway, you can map this value from the authorizer’s context to a header in the integration request. Here’s an example in Cloudformation

``` yaml
ProxyResourceRootANY:
  Type: AWS::ApiGateway::Method
  Properties:
    RestApiId: !Ref RestApiId
    ResourceId: !Ref ResourceId
    HttpMethod: ANY
    AuthorizationType: CUSTOM
    AuthorizerId: !Ref AuthorizerId
    Integration:
      Type: HTTP_PROXY
      IntegrationHttpMethod: ANY
      Uri: !Ref ProxyUrl
      RequestParameters:
        "integration.request.header.X-Platform-Key": "context.authorizer.platform_key"
      ConnectionType: VPC_LINK
      ConnectionId: !Ref VpcLink
```

[AWS: Build an API with API Gateway Private Integration](https://docs.aws.amazon.com/apigateway/latest/developerguide/getting-started-with-private-integration.html)

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
