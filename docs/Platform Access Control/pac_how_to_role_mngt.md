# Using the platform role APIs

We provide multiple ways of trying out our APIs, so feel free to choose your preferred option:

* You can play with the APIs directly in our developer portal [API Reference](https://docs.platform.navify.com/api-references)

* Download our postman collection (the easiest and most comprehensive way to experience our APIs so far)

If you’d like, you can also download the swagger files themselves. [Link out to swagger download page]

> <p class="info_notes" style="color:#0066cc; font-weight:bold">NOTE:</p>
> <p class="info_notes" style="color:#0066cc;">In general all calls to the roles APIs are in the context of a single tenant and use one specific token for a specific tenant, so you need the X-Navify-Tenant in your calls header.</p>

It is **very important** to have the following information handy for your calls to succeed. So it is useful to store these values somewhere until you fully integrate with our Auth UI and Auth Lib.

Path information

* `TenantId:` This is UUID generated automatically for your tenant during creation
* `AppId:` This is the UUID generated automatically for the application

> <p class="info_notes" style="color:#0066cc; font-weight:bold">NOTE:</p>
> <p class="info_notes" style="color:#0066cc;">you can retrieve this information easily by calling the <b>`/me`</b> endpoint</p>

Header information

* `Authorization:` the JWT bearer identity token generated for the specific user calling the endpoint
* `X-Navify-Tenant:` the Alias you specified for your tenant during tenant creation e.g. "my-hello-world-001". This is needed when making any calls in context of a single tenant.

## Resource flows

The platform provides two flows for role resources:

* **Role management**, which is intended to provide endpoints for assigning, removing, and listing user roles. These endpoints will be used by users with the tenant administrator role [API References](https://docs.platform.navify.com/api-references)

* **Application UI role resource**, which are intended to provide a logged in user role context in you applications UI [API References](https://docs.platform.navify.com/api-references)

> <p class="info_notes" style="color:#0066cc; font-weight:bold">NOTE:</p>
> <p class="info_notes" style="color:#0066cc;">The role management APIs have been moved under the tenant service and can be accessed in the context of the tenant resource.</p>

## Here are some test calls and expected responses

### Calling the `/me` endpoint

#### GET: /platform/app/:appAlias/me

The `/me` endpoint is a very useful way to get the current user roles for a specific application in a given tenant. It will provide you with information of all the current roles that are assigned to the user calling the endpoint. It is very useful when you need to find out your tenant and app uuid.

The intended use is to use the call in your application UI, to retrieve the roles context of the logged in user.

```bash
curl --request GET \
  --url https://us.api.appdev.platform.navify.com/platform/app/platform/me \
  --header 'Authorization: Bearer eyJraWQiOiJ0eGFlTENzVm4zRXEzZEc0Z3F0eEVfNVBCWWlGakU3MS1DZlVsM0FBcE1rIiwiYWxnIjoiUlMyNTYifQ.eyJ2ZXIiOjEsImp0aSI6IkFULnhuVEsyT1QtUnFsUUFmWkVSSlpxOGZZWmpZQmhDX3BDVEtwRDh2ZThWUDQuZzVBaHM2aEZXc0FRU0tzb2lJdXpuQ2NVRTJYY0VlWTJaU1MwdDFZNkZ2az0iLCJpc3MiOiJodHRwczovL3JvY2hlcmFwaWQtdGVzdC5va3RhLmNvbS9vYXV0aDIvZGVmYXVsdCIsImF1ZCI6ImFwaTovL2RlZmF1bHQiLCJpYXQiOjE1NzQ0MjI4MjMsImV4cCI6MTU3NDQyNDYyMywiY2lkIjoiMG9hMjcxZDEyZEd2dGhXd3QyOTciLCJ1aWQiOiIwMHUyN2psc2NheVR0S2FFczI5NyIsInNjcCI6WyJwcm9maWxlIiwib2ZmbGluZV9hY2Nlc3MiLCJvcGVuaWQiXSwic3ViIjoicm9jaGVwbGF0Zm9ybXVzZXJAeWFob28uY29tIn0.MslvbfhqXsKo7LeJnqoHb0tQ89iSlABUet-tbPlXyapYwUIq1CckQ7zSFdBW6CSQHKeqh8W_cOq023yoAP7QznZzscbC4NVJu31NZhcCE3w4LtE9Xj8EFrra1IURzkvZSJ0Qb6EK5R8W_Ynvl3OLB7vDMaKq2iN6__80-LMCLPNhG6k8qMzIys7aTWe23UAGMQYWjU_3gbW95RRWvQpF8TPsUr0buLJDzpx8Ukxn1mV3QsmTy5ex8UdQDZE3AGerX1FUyknnRJbhnEMfB_cNZBwiYPNG_muJR65MgV9nOck9zSmPwMuPCDfc9wQ7wqZratk2PDtLEQnAzRkYdoCVRw' \
  --header 'X-Navify-Tenant: devx-tenant'
```

> <p class="info_notes" style="color:#0066cc; font-weight:bold">NOTE:</p>
> <p class="info_notes" style="color:#0066cc;">Notice the URL in the call above, in this case the appAlias = platform.</p>

#### Sample response

```bash
{"appUuid":"617c4dcb-b17e-4b06-b846-a4c08b9d6e9d","tenantUuid":"92282ea5-a0f8-4a74-91f4-db13b51e0572","userUuid":"2767d7b0-f4ee-4a68-abe5-e7a4a616c277","roles":["tenantAdmin"]}
```

```json
{
    "appUuid": "617c4dcb-b17e-4b06-b846-a4c08b9d6e9d",
    "tenantUuid": "92282ea5-a0f8-4a74-91f4-db13b51e0572",
    "userUuid": "2767d7b0-f4ee-4a68-abe5-e7a4a616c277",
    "roles": [
        "tenantAdmin"
    ]
}
```

### Calling the user roles and claims endpoint

#### GET: /platform/app/:appAlias/userroles

Similar to the `/me` endpoint the `/userroles` endpoint it is and endpoint that is intended to be used from backend only, to retrieve the roles along with the claims associated with that role for the logged in user.

```bash
curl --request GET \
>   --url https://us.api.appdev.platform.navify.com/platform/app/617c4dcb-b17e-4b06-b846-a4c08b9d6e9d/userroles \
>   --header 'Authorization: Bearer eyJraWQiOiJ0eGFlTENzVm4zRXEzZEc0Z3F0eEVfNVBCWWlGakU3MS1DZlVsM0FBcE1rIiwiYWxnIjoiUlMyNTYifQ.eyJ2ZXIiOjEsImp0aSI6IkFULjlsN3dWVlJiZjFQb2s5VmxiN2VEUDhMV05wRW51N2tnVUFPanZhaGF5dXMuNG5yUHdkZ01NWm9aOWE4M2czKy9QOURPWlRWMTVlM2ZUSEJxbjlnVmlRYz0iLCJpc3MiOiJodHRwczovL3JvY2hlcmFwaWQtdGVzdC5va3RhLmNvbS9vYXV0aDIvZGVmYXVsdCIsImF1ZCI6ImFwaTovL2RlZmF1bHQiLCJpYXQiOjE1NzQ0MjM0NjQsImV4cCI6MTU3NDQyNTI2NCwiY2lkIjoiMG9hMjcxZDEyZEd2dGhXd3QyOTciLCJ1aWQiOiIwMHUyN2psc2FrYVFVMVBESTI5NyIsInNjcCI6WyJvZmZsaW5lX2FjY2VzcyIsInByb2ZpbGUiLCJvcGVuaWQiXSwic3ViIjoiZGF3aWQucnV0a293c2tpQHJvY2hlLmNvbSJ9.aloFNrmN9toLJ0-BB9b1PehR71U1l-9Z0nyT8KeNYk-WgLD8MsV0woLo4vo9EM-dfgntx0sXgaMZyBwUHg8FR7y3hA5w2HPx_nkB8O2lrqdfU_Y9D6ffHxw907URjGA8O7evJhMaXSE14abCIiFJKIKdVF6VCLHiJrYFN6S_11ozNbQUd037BOHjEHhU2sYna1h5Zpww8mxy2HkJPCIsDeFeCLkKKl-TVQXbe2sX_xWAVbrTizeRA1zMcF8ArR5bDbKJn9ngWTXfUVm3tGIDVryugcOpXy0ernBq6T8VYUVuc4rXm5ZMMt62NGQpGxnVyBY8EWSqZfcFzcNZRCacYg' \
>   --header 'X-Navify-Tenant: devx-tenant'
```

#### Sample response

```bash
{"userId":"2767d7b0-f4ee-4a68-abe5-e7a4a616c277","tenantId":"92282ea5-a0f8-4a74-91f4-db13b51e0572","roles":["tenantAdmin"],"claims":["readUserRoles","deleteUserRoles","assignUserRoles","readAppsSubscriptions","readUsers","updateTenant","updateUser","createUsers","readUser","readApp","readTenant"]}
```

```json
{
    "userId": "2767d7b0-f4ee-4a68-abe5-e7a4a616c277",
    "tenantId": "92282ea5-a0f8-4a74-91f4-db13b51e0572",
    "roles": [
        "tenantAdmin"
    ],
    "claims": [
        "readUserRoles",
        "deleteUserRoles",
        "assignUserRoles",
        "readAppsSubscriptions",
        "readUsers",
        "updateTenant",
        "updateUser",
        "createUsers",
        "readUser",
        "readApp",
        "readTenant"
    ]
}
```

## Role management sample call

**Role management**, which is intended to provide endpoints for assigning, removing, and listing user roles, is part of our tenant resource structure. This means that you can access role management only in the context of a single tenant.  [API References](https://docs.platform.navify.com/api-references)

### Assigning a role to a user

#### POST /platform/tenants/{tenantId}/apps/{appId}/users/{userId}/roles/{roleName}

One of the most common use cases is assigning a role to an end user. You are able to do this by calling the “Assign a role to user” endpoint.

```bash
  curl --request POST \
  --url https://us.api.appdev.platform.navify.com/platform/tenants/92282ea5-a0f8-4a74-91f4-db13b51e0572/apps/f4ac2b28-affe-4f19-83de-2bce785050c7/users/2767d7b0-f4ee-4a68-abe5-e7a4a616c277/roles/Baker \
  --header 'Authorization: Bearer eyJraWQiOiJ0eGFlTENzVm4zRXEzZEc0Z3F0eEVfNVBCWWlGakU3MS1DZlVsM0FBcE1rIiwiYWxnIjoiUlMyNTYifQ.eyJ2ZXIiOjEsImp0aSI6IkFULjZjOHM4UE1zZ1BoY2dFbi1jb1FaME5MSndOQ01VcS0xWjNvM0hXeVlkTUkuS2wycHU5QnBqa0hXRzFETStBN2NrQ1lMem1SemJMYThPcGhhWm1tN0JuWT0iLCJpc3MiOiJodHRwczovL3JvY2hlcmFwaWQtdGVzdC5va3RhLmNvbS9vYXV0aDIvZGVmYXVsdCIsImF1ZCI6ImFwaTovL2RlZmF1bHQiLCJpYXQiOjE1NzQ0Mjg2MjQsImV4cCI6MTU3NDQzMDQyNCwiY2lkIjoiMG9hMjcxZDEyZEd2dGhXd3QyOTciLCJ1aWQiOiIwMHUyN2psc2FrYVFVMVBESTI5NyIsInNjcCI6WyJvZmZsaW5lX2FjY2VzcyIsIm9wZW5pZCIsInByb2ZpbGUiXSwic3ViIjoiZGF3aWQucnV0a293c2tpQHJvY2hlLmNvbSJ9.DBLgU3aNLAAi4oN1CAAuSpd7iRHPHYHRJfDe5IiTyrwgCOWykE4_Ag1jNo-lwkLqs-28Us9uGldLdkZ6HlSh-HRCGhYnpGzGKn5K5RdDWQ8tQZ361GwR7kw82vMMHD2uwf6BBWmqquKNORw_tK5AxHNlCxOfSccP8Yf-0wCGEDvGRgvnlAB5JWzwTtMvgPs2q__qRKScU2KX1q1QgaTzj_48W9vKUKorfM8_ZOVdMpjvhNaKX02nmMcdCpcDJIv7KIQK32bhAOQ8guhZiss2ZJqmuMnq-OtdQ5yQEDm3ypfQTtdrH-jVD3y0EpqsIL-NrBeOz8XBHbCjqPt18onZIg' \
  --header 'X-Navify-Tenant: devx-tenant'
```

> <p class="info_notes" style="color:#0066cc; font-weight:bold">NOTE:</p>
> <p class="info_notes" style="color:#0066cc;">In the example below, we are assigning the user with user id <b>`2767d7b0-f4ee-4a68-abe5-e7a4a616c277`</b> from tenant <b>`92282ea5-a0f8-4a74-91f4-db13b51e0572`</b> a role <b>`Baker`</b> in app  <b>`f4ac2b28-affe-4f19-83de-2bce785050c7`</b></p>

#### Sample response

```json
{
    "userId": "2767d7b0-f4ee-4a68-abe5-e7a4a616c277",
    "tenantId": "92282ea5-a0f8-4a74-91f4-db13b51e0572",
    "roles": [
        "Baker"
    ],
    "claims": [
        "Selling",
        "Baking"
    ]
}
```

## Read more

* [Registering an app with platform](https://docs.platform.navify.com/guides/product-registration/app-registry)
* [The manifest file](https://docs.platform.navify.com/guides/product-registration/manifest-file)
* [Role API references](https://docs.platform.navify.com/api-references/tenant-resources-apis/role-management/)

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
