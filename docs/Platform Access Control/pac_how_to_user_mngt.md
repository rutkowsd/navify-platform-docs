# Using the platform user management APIs

We offer you three different ways to discover, play around and test our API endpoints:

- Download our [Postman collection](https://docs.platform.navify.com/guides/developer-tools/postman-swagger).
- Download our [Swagger files](https://docs.platform.navify.com/guides/developer-tools/postman-swagger).
- Read our [online API References](https://docs.platform.navify.com/api-references/tenant-resources-apis/user-management/getcurrentuserusingget).

## Pre-requisites

The following information are important and required as parameter or header to make your calls successful:

- **Tenant Administrator** credentials
- **TenandId**: the UUID generated automatically at tenant creation.
- **Authorization header**: JWT bearer token of user calling the endpoint.
- **X-Navify-Tenant header**: the Alias of your tenant.

All calls to User Management endpoints are always in the context of a tenant. Thus the `X-Navify-Tenant` has to be set only once in the header.

The generation of the JWT token is automated if you use the postman collection we provide otherwise please refer to [this page](https://docs.platform.navify.com/api-references/getting-started-apis/platform-apis-start)

## Examples

The following examples show you how to call some of our endpoints from linux terminal using the `curl` command

### How to get current user information

This is a handy API call when lost count and don't remember which user you currently using. Probably something you’ll experience initially when you’re switching between users to test different access rights in your application.

This call uses the [Get current user API endpoint](https://docs.platform.navify.com/api-references/tenant-user-mngt/user/getcurrentuser)

```bash
curl --request GET \
  --url https://us.api.appdev.platform.navify.com/platform/tenants/userprofile \
  --header 'Authorization: Bearer eyJraWQiOiJ0eGFlTENzVm4zRXEzZEc0Z3F0eEVfNVBCWWlGakU3MS1DZlVsM0FBcE1rIiwiYWxnIjoiUlMyNTYifQ.eyJ2ZXIiOjEsImp0aSI6IkFULlRXcURQbVphc3VVMDRJSVJrU3R0R2JROFJ4THBDMXEyaVhvNmZIVU1jYW8uS25MUzRwZkVsT0dVWjg0dSt5Y3djbmpKSUN6TU9RenFRRjVlSHF2Vk9kTT0iLCJpc3MiOiJodHRwczovL3JvY2hlcmFwaWQtdGVzdC5va3RhLmNvbS9vYXV0aDIvZGVmYXVsdCIsImF1ZCI6ImFwaTovL2RlZmF1bHQiLCJpYXQiOjE1NzI4Nzc1MjAsImV4cCI6MTU3Mjg3OTMyMCwiY2lkIjoiMG9hMjcxZDEyZEd2dGhXd3QyOTciLCJ1aWQiOiIwMHUyN2psc2FrYVFVMVBESTI5NyIsInNjcCI6WyJvcGVuaWQiLCJvZmZsaW5lX2FjY2VzcyIsInByb2ZpbGUiXSwic3ViIjoiZGF3aWQucnV0a293c2tpQHJvY2hlLmNvbSJ9.F6rnNK4ZLCJ6oeYWJ43MRuFxOvBjAyq8QG3Pxf9kfSw6Lz3vDAJgiTYc_ZgkkGGeHw9AkOvAo7tXur1bPfbqoLDGcuu_i78-6ZfZkeRkmyK92Mh0tMvooUXngWWiiiYt-S6FLridkT-RjaspbVNayAQFpCBZ3QCftOGDKhvtXDeH1F1I0sAx1xXC1f4aHCMcIlknHL9AMz4Xb54MjkcWnm5rhOJU3pwQlKG8swbPxrMA2KosmRet2MbAjRUqfWKwSRguVA1RVtegfKFqkmFk0fxcmDK3mdnbQHd1_OhA37XNUI_ypz1MeACSTJDe8iFtNyDV60tSNFEgM0god_GAtw' \
  --header 'Host: us.api.appdev.platform.navify.com'
```

**Sample response (JSON)**

```json
{
    "firstName": "John",
    "lastName": "Hancock",
    "userEmail": "john.hancock@hospital.com",
    "federated": false
}
```

### Delegate Tenant Administrator Activities

The tenant administrations activities do not have to be conducted by only one person. The tenant administrator can create other tenant administrators in the specific tenant.

This call uses the [Create new admin user API endpoint](https://docs.platform.navify.com/api-references/tenant-user-mngt/user/createadminuser)

```bash
curl --request POST \
  --url https://us.api.appdev.platform.navify.com/platform/tenants/92282ea5-a0f8-4a74-91f4-db13b51e0572/admin-users \
  --header 'Authorization: Bearer eyJraWQiOiJ0eGFlTENzVm4zRXEzZEc0Z3F0eEVfNVBCWWlGakU3MS1DZlVsM0FBcE1rIiwiYWxnIjoiUlMyNTYifQ.eyJ2ZXIiOjEsImp0aSI6IkFULlF2eUN5NVczSld4YklLQmR5dUpnMHFydGpvcHR2SUxrVkRBSEc2bmQtTm8uSTdycWphMDJ0dFVqR3ZNZVBDSkFzZ1I1NFN2WWt2R2JGcW04OGEyZkpIUT0iLCJpc3MiOiJodHRwczovL3JvY2hlcmFwaWQtdGVzdC5va3RhLmNvbS9vYXV0aDIvZGVmYXVsdCIsImF1ZCI6ImFwaTovL2RlZmF1bHQiLCJpYXQiOjE1NzI4Nzg3NzMsImV4cCI6MTU3Mjg4MDU3MywiY2lkIjoiMG9hMjcxZDEyZEd2dGhXd3QyOTciLCJ1aWQiOiIwMHUyN2psc2FrYVFVMVBESTI5NyIsInNjcCI6WyJvZmZsaW5lX2FjY2VzcyIsInByb2ZpbGUiLCJvcGVuaWQiXSwic3ViIjoiZGF3aWQucnV0a293c2tpQHJvY2hlLmNvbSJ9.gjLy_fuP_XPWzE90-pLdakiE4MBxJji0U4yHv4pnFLGNsewALGhs935KNA2PqldZ_4Kn8bR7-imifv4uA8oHoLmGWP7cdZ7gGDt3CA6YhE8h-VkczTowZqBuwGjSVa3LK6FL7R3Mo0xx9_DP8wnkE_T5k_8ApdPEldHhWNw3ttW9PXWh5XfiIZrlGymQ1_2lH04seVD8q00LkCyPIKcDWnv1Td7g4ZGPhgpVKWW7je7XcbUej5LfQfM6L9PhFj2nDC3DH5O67WMSaaNxbWjs1e3fMCMx32a9UsPsOXsTId_p1K6GIHktr19hLAYvL3WowpOYKdglIXbMypdRUTxLJg' \
  --header 'X-Navify-Tenant: <<YOUR TENANT ALIAS GOES HERE>>' \
  --data '{\n    "firstName": "Admin",\n    "lastName": "Tenant",\n    "userEmail": "admin.tenant@roche.com",\n    "externalId": "some_random_string_if_needed"\n}'
```

### List all users

This can come in handy in multiple scenarios, like for example checking if the user you think you created on Friday is still in the DB or if it was just a glitch in the matrix ...

This call uses the [Get list of users API endpoint](https://docs.platform.navify.com/api-references/tenant-user-mngt/user/getlistofusers)

Good news is that it returns also the **`userUuid`** and **`externalId`** which comes in handy with the other calls

```bash
curl --request GET \
  --url 'https://us.api.appdev.platform.navify.com/platform/tenants/92282ea5-a0f8-4a74-91f4-db13b51e0572/users?limit=10&offset=0' \
  --header 'Authorization: Bearer eyJraWQiOiJ0eGFlTENzVm4zRXEzZEc0Z3F0eEVfNVBCWWlGakU3MS1DZlVsM0FBcE1rIiwiYWxnIjoiUlMyNTYifQ.eyJ2ZXIiOjEsImp0aSI6IkFULlF2eUN5NVczSld4YklLQmR5dUpnMHFydGpvcHR2SUxrVkRBSEc2bmQtTm8uSTdycWphMDJ0dFVqR3ZNZVBDSkFzZ1I1NFN2WWt2R2JGcW04OGEyZkpIUT0iLCJpc3MiOiJodHRwczovL3JvY2hlcmFwaWQtdGVzdC5va3RhLmNvbS9vYXV0aDIvZGVmYXVsdCIsImF1ZCI6ImFwaTovL2RlZmF1bHQiLCJpYXQiOjE1NzI4Nzg3NzMsImV4cCI6MTU3Mjg4MDU3MywiY2lkIjoiMG9hMjcxZDEyZEd2dGhXd3QyOTciLCJ1aWQiOiIwMHUyN2psc2FrYVFVMVBESTI5NyIsInNjcCI6WyJvZmZsaW5lX2FjY2VzcyIsInByb2ZpbGUiLCJvcGVuaWQiXSwic3ViIjoiZGF3aWQucnV0a293c2tpQHJvY2hlLmNvbSJ9.gjLy_fuP_XPWzE90-pLdakiE4MBxJji0U4yHv4pnFLGNsewALGhs935KNA2PqldZ_4Kn8bR7-imifv4uA8oHoLmGWP7cdZ7gGDt3CA6YhE8h-VkczTowZqBuwGjSVa3LK6FL7R3Mo0xx9_DP8wnkE_T5k_8ApdPEldHhWNw3ttW9PXWh5XfiIZrlGymQ1_2lH04seVD8q00LkCyPIKcDWnv1Td7g4ZGPhgpVKWW7je7XcbUej5LfQfM6L9PhFj2nDC3DH5O67WMSaaNxbWjs1e3fMCMx32a9UsPsOXsTId_p1K6GIHktr19hLAYvL3WowpOYKdglIXbMypdRUTxLJg' \
  --header 'X-Navify-Tenant: <<YOUR TENANT ALIAS>>'
```

**Sample response (JSON)**

```json
{
    "offset": 0,
    "hasMore": false,
    "users": [
        {
            "userUuid": "addf245c-86b0-49cd-9c71-2f3b70042310",
            "tenantUuid": "92282ea5-a0f8-4a74-91f4-db13b51e0572",
            "firstName": "Admin",
            "lastName": "DevX-Tenant",
            "userEmail": "dawid.rutkowski+03@roche.com",
            "externalId": "00u2ftnj1b5wyqQw2297",
            "status": "ACTIVE",
            "federated": false,
            "_links": {
                "GET": {
                    "href": "http://tenants:8080/platform/api/v1/tenants/92282ea5-a0f8-4a74-91f4-db13b51e0572/users/addf245c-86b0-49cd-9c71-2f3b70042310"
                },
                "UPDATE": {
                    "href": "http://tenants:8080/platform/api/v1/tenants/92282ea5-a0f8-4a74-91f4-db13b51e0572/users/addf245c-86b0-49cd-9c71-2f3b70042310"
                },
                "DELETE": {
                    "href": "http://tenants:8080/platform/api/v1/tenants/92282ea5-a0f8-4a74-91f4-db13b51e0572/users/addf245c-86b0-49cd-9c71-2f3b70042310"
                }
            }
        },
        {
            "userUuid": "2767d7b0-f4ee-4a68-abe5-e7a4a616c277",
            "tenantUuid": "92282ea5-a0f8-4a74-91f4-db13b51e0572",
            "firstName": "Dawid",
            "lastName": "Rutkowski",
            "userEmail": "dawid.rutkowski@roche.com",
            "externalId": "00u27jlsakaQU1PDI297",
            "status": "ACTIVE",
            "federated": false,
            "_links": {
                "GET": {
                    "href": "http://tenants:8080/platform/api/v1/tenants/92282ea5-a0f8-4a74-91f4-db13b51e0572/users/2767d7b0-f4ee-4a68-abe5-e7a4a616c277"
                },
                "UPDATE": {
                    "href": "http://tenants:8080/platform/api/v1/tenants/92282ea5-a0f8-4a74-91f4-db13b51e0572/users/2767d7b0-f4ee-4a68-abe5-e7a4a616c277"
                },
                "DELETE": {
                    "href": "http://tenants:8080/platform/api/v1/tenants/92282ea5-a0f8-4a74-91f4-db13b51e0572/users/2767d7b0-f4ee-4a68-abe5-e7a4a616c277"
                }
            }
        },
        {
            "userUuid": "d487bcef-8d69-4baa-bc71-e741eb1182b9",
            "tenantUuid": "92282ea5-a0f8-4a74-91f4-db13b51e0572",
            "firstName": "jonathan",
            "lastName": "hegy",
            "userEmail": "jonathan.hegy+1@roche.com",
            "externalId": "00u2jbmqjeIg5we9Y297",
            "status": "ACTIVE",
            "federated": false,
            "_links": {
                "GET": {
                    "href": "http://tenants:8080/platform/api/v1/tenants/92282ea5-a0f8-4a74-91f4-db13b51e0572/users/d487bcef-8d69-4baa-bc71-e741eb1182b9"
                },
                "UPDATE": {
                    "href": "http://tenants:8080/platform/api/v1/tenants/92282ea5-a0f8-4a74-91f4-db13b51e0572/users/d487bcef-8d69-4baa-bc71-e741eb1182b9"
                },
                "DELETE": {
                    "href": "http://tenants:8080/platform/api/v1/tenants/92282ea5-a0f8-4a74-91f4-db13b51e0572/users/d487bcef-8d69-4baa-bc71-e741eb1182b9"
                }
            }
        }
    ]
}
```

### Create a user

Finally create a regular tenant user probably one of the main endpoints you'll want working correctly.

This call uses the [Create new user API endpoint](https://docs.platform.navify.com/api-references/tenant-user-mngt/user/createuser)

``` bash
curl --request POST \
  --url https://us.api.appdev.platform.navify.com/platform/tenants/92282ea5-a0f8-4a74-91f4-db13b51e0572/users \
  --header 'Authorization: Bearer eyJraWQiOiJBbHc5bmMxYUlua2hscUtFMjRZai1kSDRMekVxN3h1ZGwzcWNDZGk1NGRBIiwiYWxnIjoiUlMyNTYifQ.eyJ2ZXIiOjEsImp0aSI6IkFULmI4cTFOcG41MzJmSWU1X1RqSzBLUXdkc0djUFNxY1FrQTAxZHA0aDZyZkUuSnVxUDhYZHBjV2NCdENLTjU4aHQwSGU5b1dSZkdYcnZSd1JiV3NXUXdrRT0iLCJpc3MiOiJodHRwczovL3JvY2hlcmFwaWQtdGVzdC5va3RhLmNvbS9vYXV0aDIvZGVmYXVsdCIsImF1ZCI6ImFwaTovL2RlZmF1bHQiLCJpYXQiOjE1NzYxNDEyMDYsImV4cCI6MTU3NjE0MzAwNiwiY2lkIjoiMG9hMjcxZDEyZEd2dGhXd3QyOTciLCJ1aWQiOiIwMHUyN2psc2FrYVFVMVBESTI5NyIsInNjcCI6WyJvcGVuaWQiLCJvZmZsaW5lX2FjY2VzcyIsInByb2ZpbGUiXSwic3ViIjoiZGF3aWQucnV0a293c2tpQHJvY2hlLmNvbSJ9.Q-mjMM7NnsALTAs4bqecpAANhRvGqTAeHVTpMEej7Iw_ZAg9hUTw5gJdv8L4sVDthQr8e_L8g5U-Ct4uIRAXdyGLvJ1go5yj7wYtPAPJsk9-50qnzAp5eaIPrT62JA3b7bB09A5MYxwRoSmVKlR89-vVyEOOuZhWC8O50bR5EHib27SU3FkSUJTfc5iz9NqR20-B826_L7Ocsrlmek-X37RmPAOZSmTblSOgBVOYVp9JkG5gQM3Jn26MVlNFcz5IgReWkv8UplRdZvwo3HkSrfFOQo-kQk6ItE4ZMOtxXqoQBGEjQA350yKLK4s8JGqUgXNDP944tYXczftuHn3h7A' \
  --header 'Content-Type: application/json' \
  --header 'X-Navify-Tenant: devx-tenant' \
  --data '{\n    "firstName": "userFirstN",\n    "lastName": "userLastN",\n    "userEmail": "userFirstN.userLastN@hospital.com"\n}'
```

**Sample response**

```json
{
    "userUuid": "8090738e-438d-4501-a8d9-8b900f939bca",
    "tenantUuid": "92282ea5-a0f8-4a74-91f4-db13b51e0572",
    "firstName": "Dawid01",
    "lastName": "Rutkowski01",
    "userEmail": "dawid.rutkowski+1@roche.com",
    "externalId": "00u2myqm762jwN8BS297",
    "status": "INACTIVE",
    "federated": false,
    "_links": {
        "GET": {
            "href": "http://tenants:8080/platform/api/v1/tenants/92282ea5-a0f8-4a74-91f4-db13b51e0572/users/8090738e-438d-4501-a8d9-8b900f939bca"
        },
        "UPDATE": {
            "href": "http://tenants:8080/platform/api/v1/tenants/92282ea5-a0f8-4a74-91f4-db13b51e0572/users/8090738e-438d-4501-a8d9-8b900f939bca"
        },
        "DELETE": {
            "href": "http://tenants:8080/platform/api/v1/tenants/92282ea5-a0f8-4a74-91f4-db13b51e0572/users/8090738e-438d-4501-a8d9-8b900f939bca"
        }
    }
}
```

## Integration with the Auth UI

Once you fully integrate with our Auth Library and Auth UI this information will be automatically injected into your calls towards one single tenant. Therefore you don’t have to worry about developing this logic in your app yourself. However be mindful that the tenant alias that is in the header of the URL like for example ‘tenant-alias.webapp.roche.com’ means that you are always in the context of that specific single tenant. The Auth library passes and refreshes identity tokens through a cookie named access-token rather than the Authorization HTTP header. Still, you will be responsible for passing `X-Navify-Tenant` header.

Read more [Authentication UI & Auth Lib](https://docs.platform.navify.com/guides/platform-access-control/authentication-openid/pac-authentication-intro)

## Read more

* [User Management](https://docs.platform.navify.com/guides/platform-access-control/user-management)
* [Tenant User Activation](https://docs.platform.navify.com/guides/platform-access-control//howto-activate-tenant-user)
* [How The Platform Works](https://docs.platform.navify.com/guides/platform-services/how-the-platform-works-folder/how-the-platform-works)

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>