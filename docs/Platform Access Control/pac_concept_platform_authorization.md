# Platform Authorization

## Description

One of the main responsibilities of the platform is to provide mechanisms to control access to services and functionalities offered by the platform itself, as well as applications built on the platform. The services and functionalities that a user is authorized to use must be represented in a common way to ensure that access can be properly controlled at all levels of the platform architecture.

The platform authorization service creates platform back-end tokens  that can be used by platform applications to verify and authorize access to its back-end services. We will maintain a public and private key pair in S3 that the authorization service will use to sign the JWTs it generates, rotated on a regular schedule. The API gateways of products on the platform have custom authorizer lambda functions that will call the auth service whenever a request with a valid Okta token and tenant header is presented. The auth service will gather all of the necessary information from other platform services–such as the Role service–then create and sign the JWT and return it to the lambda where it will be attached as a header and be available for use from then on in the product back-end services. The auth service will also provide access to the public key necessary to validate the token it produces so other services can check the token’s integrity.

## What you should know

The Authorization service gives out valid platform backend tokens when called with an appropriately configured user, tenant, and app combination.
It will return a 403 when the set of roles and claims for the user/tenant/app combination is empty
Verification of the backend token happens via our authorization API endpoints

[...Missing pictures...]

0. The PEM file used to sign platform tokens and the corresponding public key are stored in S3 and accessed by the Authorization service on service startup and when key rotation/cache invalidation events occur.
1. Traffic from a product UI, intended for a product backend service, comes to the platform public API gateway. The gateway checks that the request has a valid Okta token and has the X-Navify-Tenant header. 
2. The request is routed to the particular product’s private API gateway, which then executes a custom authorizer lambda function.
3. The custom authorizer lambda will send a request to the platform auth service, hitting the new /app/{applicationId}/token endpoint via the platform product API Gateway
4. This auth service endpoint in the gateway will be configured to not cause token generation (this would cause an infinite loop)
5. The auth service then calls an endpoint from the platform Role service in order to get the user’s roles and claims, as well as the actual platform internal user and tenant ids. It wraps these values up with TTL configurations and signs the JWT with the private key configured in step 0, and returns the new backend token for this request.
6. The lambda authorizer in the product API gateway attaches this new token as the X-Platform-Token header and caches it for a short period of time.
7. The traffic now arrives at the targeted backend service. The service can use the user, tenant, role, and claim information in the token to grant or deny access to the API in question.

> **Authorization**
>
> All requests must have the following extension X-Navify-Tenant and an authorization header

## GET /app/{applicationId}/token

See our reference doc [here](https://docs.platform.navify.com/api-references/auth-backend-resources/authorizer/gettokenusingget)

Response:

``` json
{
 "iss": "https://platform.navify.com", //Platform backend token issuer
 "iat": 1540849078, //Issued at
 "exp": 1540852678, //Expiration time
 "tenant_id": "82d54082-4984-11e8-a891-8fda9c42a437", //Platform tenant scope for this request
 "roles: [ //Platform roles for the given application and tenant
 "reviewer",
 "patient"
 ],
 "claims": [ //Platform claims that the user's roles confer to them
  "patientService",
  "presentationService",
  "meetingService"
 ],
 "sub": "15406dc0-f59f-49fa-9d6c-ce5822c9f53b" //Platform internal user id for this user
}
```

## GET /keys

See our reference doc [here](https://docs.platform.navify.com/api-references/auth-backend-resources/authorizer/getpublickeysusingget)

Response:

``` json
(JSON Web Key Set)
{
    "keys": [ JSON Web Key Definition ]
}
```

## Security & compliance considerations

As mentioned above, the Auth service gives out tokens in a user/tenant/application context, and users will only be given tokens with claims if their tenant can access the application in question, and if any roles have been given to that user by the tenant admin. In the non-malicious cases where the user’s tenant has not been licensed to use the app, or if the user has not been given roles to access the app, or in malicious cases where a user is attempting to access an app or tenant to which they do not belong, the auth service will deny access as basic functionality, and in this way it is fail-safe.

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
