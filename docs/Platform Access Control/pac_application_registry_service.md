# Application Registry Service

## Introduction

Registering a product is done through the Platform Application Registry Service. It is responsible for maintaining the list of applications on the platform, as well as their details and configurations. This service provides endpoints that allow applications to configure and register themselves with the platform.

When an application submits a manifest file describing its details, the application registry service will store the file in S3 (versioned, per application), parse it and store values in the service database, then trigger automation scripts to deploy or modify any cloud assets. Since the platform requires an API Gateway to be configured in each product’s AWS account, which provides routing and visibility options as well as authorization. The application registration process will create a Product API Gateway for the specific product in the specified account.

## What features does the service offer?

- Endpoints for app configuration (used by Platform Administrators)
- Endpoints for getting app descriptions (used by Product teams and Platform Administrators)
- Triggers deployment of assets (see architecture diagram) in Products AWS account

## Application registration diagram

![Diagram - Application Registration](https://s3.amazonaws.com/user-content.stoplight.io/18223/1560430998798 "Diagram - Application Registration")

The blue lines show the path for APIs that retrieve information about apps for use in the platform management UI.

- Traffic originates from the platform UI
- Traverses the gateways
- Gets authorization/backend token
- Hits App Registry service
- Queries DB for relevant info

The red lines represent all of the things that the automation scripts need to touch in order to properly deploy/configure an application on the platform.

- The flow starts when a manifest file is sent to the App Registry service endpoint
- The file is stored in S3
- Then parsed and stored in the App Registry database
- This triggers automation scripts that use the configuration values in the manifest/db deploy and/or configure:
  - Platform Public API Gateway with any new or modified product endpoints that are meant to be public
  - Product Private API Gateway with all endpoints
  - NLB for each defined product service
  - Route 53 DNS entry for app alias + platform hostname
- The automation scripts will update the deployed_assets table as well as the deployment state table as it proceeds

## Technical risks and mitigations on product side

### Access to Application AWS account:

**Risk**: Products will want (and legally will be required) to control or own their AWS account, and will be hesitant to allow backdoor access.

**Mitigation**: The product will create a Role for the app registry automation scripts to assume when doing work. Since the application team determines when a new manifest file is pushed, they can keep the Role disabled until a new deployment happens, and then disable it again afterward.

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
