# Platform Authentication

The NAVIFY Platform offers a single mechanism to authenticate your application users. We’ve created an authentication module that adheres to OpenID connect flow and provides a common library to provide a streamlined solution to complete the authentication process. Currently, we leverage [OKTA](https://www.okta.com/) as the identity provider (IdP). It utilizes a simple JSON-based identity tokens (JWT), delivered via OAuth 2.0 flows designed for web, browser-based and native/mobile applications. It supports Single Sign-On across multiple applications with the same Tenant as well as across Tenants.

> **NOTE**: The platform does not store any additional user credentials, it uses the combination { email + tenant_uuid } to identify users.

The Platform Authentication mechanism consists of the following elements

[![architecture](//s3.amazonaws.com/user-content.stoplight.io/18223/1568728479341)](//s3.amazonaws.com/user-content.stoplight.io/18223/1568728479341)

> **NOTE**: All authenticated network traffic will go through the Platform API

## Authentication UI application

This is the common login screen that is NAVIFY® Branded (see below an example login screen from our Demo Application). The UI application provides a url_redirect based API, where the intent is for client UI applications to use it to perform operations with the Navify Authentication module.
Any application using the Authentication UI need to redirect to this URL for authentication into NAVIFY Platform. Once Authenticated, NAVIFY Authentication will redirect back with an access token present in a httpOnly cookie.

[![demo_auth_ui](//s3.amazonaws.com/user-content.stoplight.io/18223/1568728466877)](//s3.amazonaws.com/user-content.stoplight.io/18223/1568728466877)

### Routes

* `/authreq` - route for setting the Authentication Request (used by the Auth Library)
* `/authres` - route for setting the Authentication Response (used by the Auth Library)
* `/login` - login form (credentials, MFA)
* `/recover` - recover form (identification, MFA, new credentials)
* `/return` - route used after redirection back from external IDP

### Arguments

The authentication UI application does not have it's own configuration. It is configurable through arguments of the Auth Library `loginRedirect` call. The configuration related Request Id is passed as `authreq` query parameter.

### Return

When redirecting back to a UI application which initiated authentication flow, the `authres` output query param is added to the URL. It is an identifier of the authentication result. The Auth Library `getLoginReturn` method, when called, returns the result object.

## Authentication library

This Javascript library contains all the front-end source code of the Authentication library meant to be used by various client UI applications for user authentication with the NAVIFY Platform. It provids APIs for all authentication related operations that products may need. It is meant to be framework agnostic.

> **NOTE:** To use the common authentication mechanism, you must import and use the Auth Lib

See [How To Use Authentication Library](https://docs.platform.navify.com/guides/ac-guides/how-to-use-auth-lib) for more information

## Read more

* [How the Platform works](https://docs.platform.navify.com/guides/platform-services/how-the-platform-works-folder/how-the-platform-works)
* [Platform Login](https://docs.platform.navify.com/guides/platform-services/how-the-platform-works-folder/platform-login)
* [API Gateway Integration](https://docs.platform.navify.com/guides/platform-services/how-the-platform-works-folder/api-gatway-integration)
* [HTTP Requests](https://docs.platform.navify.com/guides/platform-services/how-the-platform-works-folder/http-requests)

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
