# HTTP Requests

## X-Navify-Tenant

All requests to protected API endpoints must carry the X-Navify-Tenant header. On the platform, a “tenant” is some institutional group a user belongs to. It can be a hospital, a university, a lab…just some grouping of people who have access to applications on the platform. This headers helps us have a one-to-many mapping for a user->a group. All requests to apps on the platform are in context of a “tenant”, and the JWT we generate is unique to that user in that tenant’s context.

## An Example Request

When your endpoint has the product authorizer and integration mapping set up, your backend service call will carry the X-Platform-Key header. This value will be a Bearer JWT that is known as the Platform Key.

For example, let’s say a request comes to the frontdoor like this

``` http
GET /example-service/whatever HTTP/1.1
Host: example.api.platform.navify.com
X-Navify-Tenant: cf3488b9-74b9-4e51-95ca-b456055fdb97
```

your backend service will receive a request that looks like this

``` http
GET /whatever HTTP/1.1
Host: example-service.my-product-domain.internal
X-Navify-Tenant: cf3488b9-74b9-4e51-95ca-b456055fdb97
X-Platform-Key: Bearer abc123...
```

When you decode that X-Platform-Key and verify it against the Platform Public Key, it’ll contain a payload like so

``` json
{
  "iat": 1554409494,
  "exp": 1554409854,
  "iss": "https://platform.navify.com",
  "sub": "0f7694e7-f288-412f-ad02-bf4e0beefc0d",
  "tenant_id": "cf3488b9-74b9-4e51-95ca-b456055fdb97",
  "roles": [
    "myAppAdmin"
  ],
  "claims": [
    "doStuff",
    "beAwesome"
  ]
}
```

The subject (sub) is this user’s unique guid for the platform. The tenant_id matches the X-Navify-Tenant of the request. If this user was not a member of this tenant, the authorizer would’ve failed the call and no token would’ve been generated. The roles and claims for this particular user come from the roles and claims assigned to that user+tenant+app combination in the Platform Services.

You can now make authorization decisions in your app based on the roles and claims presented in this token!

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
