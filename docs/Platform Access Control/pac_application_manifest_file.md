# Manifest File

[Download the manifest file](https://stash.intranet.roche.com/stash/projects/NAVIFY_PRODUCT_STARTER/repos/np_developer_experience/browse/navify-platform-demo-manifest.json) for the NAVIFY Platform Demo App from our Bitbucket repository.

> The manifest file was created for our HelloWorld (demo) application. It is annotated with additional information about what we as the Platform expect within the file. Please use this information as reference only.

``` json
NAVIFY Demo App - Hello World
{
  # .name (String): Unique name of the application, used to initially register the app
  "name": "NPDemo",
 
  # .displayName (String): Human-readable name of the app, to be shown in UIs that list apps
  "displayName": "Navify Platform Demo",
  
  # .alias (String - URL-friendly):  Used in the URL of the application to route traffic (e.g. tenant.[alias].navify.com)
  "alias": "npdemo",
  
  # .imageAtBase64 (String - Base64 encoded bytes): Image that represents the app (icon, etc), used with displayName when apps are shows in UIs
  "imageAtBase64": "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAIAAAD8GO2jAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjEuNWRHWFIAAAMMSURBVEhL7ZVbSBRRGMeNICh6iK4UFFGRQRIWVNBDBb0IPVr0Ui8VJFmQmWF5yVpJSgkyAg0KtKgoiVXT0sLSEsVc93531Xaddd32Ns467uy1/+we9/ayrhW97I95ON935nz/M9/5HyYr9I/JCKQkI5CS/yHwSUWvK5bsKJen9WBJp9xFSsSRLKCemttZoRi3eUi8YIx2LrtCoTSzJJ4nQcDG+HZXKftHGRKnyeCYG8t/MT4Sh4kJcL7g0Trt80E7iRfFyyH7kTotSpE4XuBs00SZcBKDCRtXJqQEHWaJKfl7ozAef0mLiQSJVLRS55omSBAVuN9lOdFoCAR55T49s+2mHJnVRWLWG4i8kAT6sPLyCAkSQRGUquu2REJeQChxHqhRz3KkVp+OQYjB2quSSSfXKnHmNxiaw63rUtL5DaOPv1qtjG/ZRdHpp2OVbRR29UU7c7LR0DRgCxcIoRQqvJfxpsoSG2ezKxWU0xuZAxBYUyTJe6g7WKPm/MFVV8RtUhfEUHR9seStyLHpuhTnubRg+J3YiTw6ufWGrEXk2FIqMzm4SBHsbFelQk6xWfvvqltGHJFsBAhgDseFxbppD4oiua9a1S5zrbjEt2XEOIt8pEXIf1TSSy4MH3+kx7Y+q2f4EmEgj+/IGhp35wpUdnfMW9EWYUdYABmM9wpUHxT08kK+KBoFv0cFulX00gKR1MQ29Fp/2skXuFg/pr6PMvwZvBl2HHug9fqJt6ICuQJlv8G9sURa3TmF3TlZ//YyOZoOSQUVE+jVMTlVynIhteGaFG1E0hcI5tXrX4SPjbiousN8vpl4C+17/YOfQ7uxI9kke6vdPGBwI6OxzGHco5lhuQCOGhl0Eu/rrZ6qdvM3Pbmhha+MMGtkTATghDPPxmq7iLf+hPqe6VNPDGHD8xAB4PEFDtdqYFkSL4pOOX3onib+9sQEAK7PnjtKmITEaSKj2Jzbyik65niQIABU5jmcWPy1WCAW2ovqODASz5MsAGC7zaUy2COtB0s6FvI/+OtkBFKSEUhBKPQb4HMSy9UdM28AAAAASUVORK5CYII=",
  
  # .regions (List of Strings): Cloud provider regions that the app will be deployed in
  "regions": [
    "us-east-2"
  ],
  
  # .costCenter (String):  Identifier or name of cost center for billing (not hooked to anything yet; fill out as best as possible)
  "costCenter": "12345",
  
  # .infraConfiguration: The AWS account information
  "infraConfiguration": {
    # .provider (String - Enum): Cloud provider name, currently only AWS supported
      "provider": "AWS",
    # .accountId (String): Provider account ID, used to generate deployment scripts
      "accountId": "roche-dis-tb01"
  },
  
  # .frontend: is the URL to the front end of the application (here an example of the Hello World Demo UI)
  "frontend": [
    {
    # .name (String): Identifier for frontend portion of the application
    "name": "platform-demo-ui",
    # .url (String): Location of the frontend application deployment (in order to route traffic)
    "url": "https://np100demo-astack.demoapp.tb01.rginger.com"
    }
  ],
  
  # .backend (List): Configuration of backend services. 
  # Each element will define how and which traffic is routed to the described service from the product API gateway
  "backend": [
    {
      # .name (String): Identifier for this particular backend service; used to route traffic from the product API gateway
      "name": "navify-platform-demo-service",
      # .networkLoadBalancerName (String): Name of the NLB that should receive traffic for this service 
      # (use "functionName" to identify the lambda function to call if LAMBDA "type" is used)
      "networkLoadBalancerName": "platf-MyLoa-132EREK070TA",
      # .port (Intiger): port that will receive traffic
      "port": "443",
      # .healthUrl (String - url): It is the URL that will be called by the load balancer to determine if the node is healthy 
      "healthUrl": "https://platform-demo-service/platform-demo-service/health",
      # .type (String - Enum): VPCLINK or LAMBDA; Type of connection that the product API gateway will use to route traffic to the service
      "type": "VPCLINK",
  
      "routes": [
      {
        # .path (String): Wildcard partial URL defining a certain subset of URL paths to route to this service
        "path": "*/platform-demo-service/good-patients", # will accept * as path 
        # .visibility (String - Enum):  "public" or "private"; whether or not public internet traffic should be routed for this route
        "visibility": "public", 
        # .requiredClaims (List of Strings): (*note: Not used yet; not required) 
        # When added, will be the list of claims that the platform will require that the user has to access this route
        "requiredClaims": ["viewPatient"],
        # .allowedMethods (List of String - Enum): List of HTTP verbs to allow on this path, or "ANY" to allow all methods
        # common verbs  "GET", "HEAD", "POST", "PUT", "PATCH", "DELETE", "OPTIONS", "TRACE", "ANY"      
        "allowedMethods": [
          "GET"
        ]
      },
      {
        "path": "*/platform-demo-service/good-patients-extended",
        "visibility": "public",
        "allowedMethods": [
          "GET"
        ]
      }
    ]
    }
  ],
  
  # .roles (List): Roles are meant to describe a logical user role (doctor, nurse, lab technician, etc.), and map to a set of claims
  "roles": [
    {
      # .name (String): Identifier for this role
      "name": "commonUser",
      # .displayName (String): User-friendly identifier for this role, used in UIs that list roles
      "displayName": "Common User",
      # .description (String): User-friendly description of the role, used in UIs that list roles
      "description": "A common user of the demo application",
      # .claims (List of Strings): List of claims that this role should confer to the uer that is granted the role
      "claims": [
        "viewPatient"
      ]
    }
  ],

  # .Claims (List): Claims are meant to be specific backend permissions,
  # often used to grant read or write access to a particular resource, or access to a particular API
  "claims": [
    {
      # .name (String): Identifier for this claim. This is what should be checked in application backend code in order to authorize calls
      "name": "viewPatient",
      # .displayName (String): User-friendly name for this claim
      "displayName": "View Patient Claim",
      # .description (String): User-friendly description of what this claim does
      "description": "Ability to view patients"
    },
    {
      "name": "viewPatientExtended",
      "displayName": "View Patient Claim Extended",
      "description": "Ability to view patients extended"
    }
  ]
}
```

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
