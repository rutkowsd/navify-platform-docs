# Using the platform Authentication Library

## Prerequisites

To start using the NAVIFY Access Control microservices and the authentication library to get a Platform Access Token

* Your application has to be registered with the NAVIFY Platform [see How To Register Your App](https://docs.platform.navify.com/guides/product-registration/app-registry)
* It should have a domain

### Steps

1. Download and Install the navify-npm artifacts
2. Import the @dia/auth in your products login page

## How To Get and Install Library?

Set up an account in jFrog and get the library

* [See jFrog Artifact Management](https://docs.platform.navify.com/guides/developer-tools/jfrog-artifact-management/jfrog-introduction)
* Set up `NPM/Artifactory` on your OS
[https://navify.jfrog.io/navify/webapp/#/artifacts/browse/tree/General/navify-npm](https://navify.jfrog.io/navify/webapp/#/artifacts/browse/tree/General/navify-npm)
</br>(click “Set Me Up” in the top right corner and follow the instructions)

* Make sure you have **Node.JS** version 10+ installed in your OS, it’s very likely you will have to downgrade.
* Run `$ npm i` command in the root directory.

Good now you are set up!

### Known Issue

> <p style="color:#0066cc; font-weight:bold; padding:10px 3px 0px;">NOTE</p>
> <p style="color:#0066cc; padding:0px 3px 10px;">The library may not work <b>Node.js version 12+</b>, so you may need to downgrade your node version to <b><i>`node@10`</i></b></p>

## Platform UI Interceptor

The platform UI expose the platform interceptor that automatically inject the `X-Navify-Tenant` in the header when the client makes subsequence call to the platform after login.

```javascript
as providers: [
    AuthService,
    AuthGuardService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: NavifyPlatformInterceptor,
      multi: true
    },
    PatientService,
    GiphyService,
    ],
 bootstrap: [AppComponent]
})
```

## Using the Auth Lib

Our Hello World application uses all the features of the NAVIFY platform. You can see how the Hello World app implements the Auth Lib which redirects to the Auth UI (unified login) by following this link: [https://roche.demoapp.tb01.rginger.com/home](https://roche.demoapp.tb01.rginger.com/home)

### Configuration

Authentication configuration has the following structure

```javascript
import { AuthConf } from '@dia/auth';

const authConf: AuthConf = {
    provider: {
        type: string, // IDP type (e. g. "okta")
        url: string, // IDP instance URL
        clientId?: string, // optional IDP client identifier
    };
    platform: {
        url: string, // Navify Platform URL
    };
    app: {
        url: string, // Auth-UI URL
    };
    client: {
    appAlias: '...',     // Client application alias
    tenantAlias: '...',  // Client tenant alias
  },
}
```

### Options

You may want to change the default library behavior through options [[see Customizing Auth UI](https://docs.platform.navify.com/guides/ac-guides/howto-cust-auth-ui)]

```javascript
import { AuthOptions } from '@dia/auth';

const authOptions: AuthOptions = {
    sessionExpirationBeforeTime?: 10000,  // How much time before expiration should the library emit the BEFORE_SESSION_EXPIRATION event (milliseconds)
    sessionPollingIntervalTime?: 10000,   // How often should the library send a polling request to check the existing session (milliseconds)
    redirectUrlUseHash?: false,           // Flag deciding whether to use URL hash instead of query parameters
    identificationFormSkip?: false,       // Flag deciding whether to force skip the identification step
    customPageTitle?: '...',              // Page title customization HTML
    customBackgroundStyle?: '...',        // Background style customization CSS
    customHeader?: '...',                 // Header customization HTML
    customHeadline?: '...',               // Headline customization HTML
    customCaption?: '...',                // Caption customization HTML
    customFooter?: '...',                 // Footer customization HTML
  };
```

Please see the annotated source code for what options are supported.

### Instantiation

With the configuration object you can now create an instance of authentication implementation

```javascript
import { Auth } from '@dia/auth';

const auth = new Auth(authConf, authOptions);
```

## API

Now you can use the authentication implementation. Here's a list of examples:

### **Initialize** the authentication implementation instance

```javascript
await auth.init();
```

### **Get** authentication session

```javascript
import { AuthSession } from '@dia/auth';

const session: AuthSession = await auth.getSession();
```

### Refresh the authentication session

```javascript
const session: AuthSession = await auth.refreshSession();
```

### Redirect to the login form

```javascript
import { LoginRedirectInput } from '@dia/auth';

const input: LoginRedirectInput = {
  returnTo: location.href,
  state: 'something to preserve',
};
await auth.loginRedirect(input);
```

### Get login result after being redirected back

```javascript
import { LoginReturnOutput } from '@dia/auth';

const output: LoginReturnOutput = await auth.getLoginReturn();
/* {
  success: true,
  state: 'something to preserve',
}; */
```

### Log out and destroy the authentication session

```javascript
await auth.logout();
```

### Set up **authentication events handler**

```javascript
import { AuthEvent } from '@dia/auth';

import {
  AuthEvent,
  AUTH_EVENT_SESSION_START,             // type of event triggered when there is an active session
  AUTH_EVENT_BEFORE_SESSION_EXPIRATION, // type of event triggered before the session expire
  AUTH_EVENT_SESSION_EXPIRATION,        // type of event triggered when the session has expired
  AUTH_EVENT_SESSION_END,               // type of event triggered after logout
  AUTH_EVENT_WINDOW_LOGIN,              // type of event triggered when a child window requests login redirection (use event result)
} from '@dia/auth';

const unsubscribe: Function = auth.subscribe((event: AuthEvent) => {
  switch (event.type) {
    /* event handling */
  }
});
```

Please see the annotated source code for what events are emitted (look for `AUTH_EVENT_*`).

### Some authentication events make use of **event result**

```javascript
import { AuthEvent, AUTH_EVENT_WINDOW_LOGIN } from '@dia/auth';

auth.subscribe((event: AuthEvent) => {
  if (event.type === AUTH_EVENT_WINDOW_LOGIN) {
    event.setResult({
      returnTo: window.location.href,
      state: 'something to preserve',
    });
  }
});
```

### **Destroy** the authentication implementation instance

```javascript
await auth.destroy();
```

Please see the annotated source code for reference.

## Iframe link support

The Authentication library contains support for iframes. It means that if there is an active instance of the library in the parent window and there is one in an iframe, the instances establish a **link** through which the instances communicate in both ways.

* All the **library calls** go through the link and are executed in the parent window
* All the network requests are triggered from the parent window
* The login redirection happens in the parent window

It is worth noting that even though the login redirection happens in the parent, the `state` value provided in the `loginRedirect` call in a child instance is preserved and returned properly by the `getLoginReturn` call.

## Quick and Dirty

This is a quick and dirty react implementation of how you can use the Auth UI in your application.

```javascript
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

// Import the classes for NAVIFY Platform authentication
import {
   Auth,
   AuthConf,
   AUTH_EVENT_SESSION_END,
   AUTH_EVENT_SESSION_EXPIRATION
} from '@dia/auth';

//Configuration
const auth = new Auth({
   provider: {
       type: 'okta', // IDP type (e. g. "okta")
       url: "https://dev-678843.okta.com", //"https://dev-418089.oktapreview.com", //url: "https://dev-678843.okta.com", // IDP instance URL
       clientId: "0oakphtdf6ijbUayY356" // "0oahv66jxhm2H4dde0h7" //"0oakphtdf6ijbUayY356" // optional IDP client identifier
   },
   platform: {
       url:  "https://us.api.appdev.platform.navify.com" // Navify Platform URL
   },
   app: {
       url: "https://us.auth.appdev.platform.navify.com/", // Auth-UI URL
   },
}, {
   customPageTitle: 'Smarttesting authttest Authentication',
   customHeadline: '<strong>NAVIFY<sup>®</sup></strong> test',
   customFooter: `<div class=""><a href="http://roche.com">Roche</a>CPS</div>`,
}
);

async function doAuth() {
   await auth.init();  // instantiate the library object 
   const session = await auth.getSession(); // check if we have a running session
   const result = await auth.getLoginReturn(); // to know whether you have just been redirected back (to show a message or whatever it is you want to do in such case)

   if (session == null) {
       window.alert("Let's redirect to create a session !")
       auth.loginRedirect({
           returnTo: "http://www.auth.smarttesting.navify.com",
           state: {
              /* "providerType": "okta",
               "returnBy": "postMessage"*/
           },
       });  
   } else {
       window.alert("Session with headers: " + JSON.stringify(session));
       const response = await fetch('https://us.api.appdev.platform.navify.com/smarttesting-auth/api/ms-patients/patient/2', {
           credentials: 'include',
           method: 'GET',
           mode: 'cors',
           headers: {
               "X-Navify-Tenant": "test-tenant"
           }
       });
       if (response.ok) {
           const myJson = await response.json();
           window.alert(JSON.stringify(myJson));
       }
       else window.alert("Back-end call failing with error " + response.status + " " + response.statusText)
   }
}

doAuth();
ReactDOM.render(<App />, document.getElementById('root'));
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
```

### FAQ

**Q:** Why do we need to specify `X-Navify-Tenant` in requests?  Otherwise you will get a 401 response. Assumed that the tenant could be looked up from the auth token?

**A:** We need to know the context of the requested tenant to know how to make that token, to bake it in there in the first place. The okta token only tells us who you are. We want for a single user to belong to multiple tenants, so you have to tell us the tenant context of this request, we check membership, then if that’s good, we make the auth token and send it along in the request.

## Read more

* [Platform Authentication](https://docs.platform.navify.com/guides/platform-access-control/pac-authentication-intro)
* [How the Platform works](https://docs.platform.navify.com/guides/platform-services/how-the-platform-works-folder/how-the-platform-works)
* [Platform Login](https://docs.platform.navify.com/guides/platform-services/how-the-platform-works-folder/platform-login)
* [API Gateway Integration](https://docs.platform.navify.com/guides/platform-services/how-the-platform-works-folder/api-gatway-integration)
* [HTTP Requests](https://docs.platform.navify.com/guides/platform-services/how-the-platform-works-folder/http-requests)

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
