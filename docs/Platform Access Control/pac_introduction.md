# Platform Access Control Introduction

The goal of the NAVIFY Platform Access Control solution is to offer a consistent user experience for users when logging into products (SaaS applications) living on the Platform. What’s more, we make the integration of these platform services and APIs as easy as possible for all SaaS applications developing on the NAVIFY Platform.

If you have decided to use Platform Access Control you will need to get a good understanding of the underlying concepts and how to use them with your SaaS application. This document will provide you a high-level overview of concepts that are at the foundation of Platform Access Control and in building SaaS solutions on the NAVIFY Platform, for example, multi-tenancy, authentication & authorization, and pre-requisites such as registering your application with the platform.

The **Platform Access Control (PAC)** consists of:

## Multi-tenancy

At the core of most SaaS applications is building a robust and efficient identity and isolation model. This is why the Platform implements multi-tenancy, allowing for:

* Each product to define its own security roles and claims independently of other products
* Each customer to manage separately his users and their assignment to product roles

Read more: [Multi-tenancy](https://docs.platform.navify.com/guides/miscellaneous/multi-tenancy)

## Application Registration

This is a service provided by the platform that allows product teams to register their SaaS application with the Platform. It keeps a consistent track of the application configuration and will automatically trigger the creation of some cloud assets within your product account. 

Read more: [Application Registry](https://docs.platform.navify.com/guides/product-registration/app-registry)

## Platform Authentication

In the case of web-based applications, it is available through an npm JavaScript package offering platform Auth UI. Products can integrate the platform @dia/auth Lib into their products, to easily create a connection with our Auth UI and to generate a platform access token.

In the case of non-web based (mobile apps…), OpenID can directly be used for authenticating users and generating/refreshing tokens according to industry standards

Read more: [Authentication Introduction](https://docs.platform.navify.com/guides/platform-access-control/pac-authentication-intro)

## Platform Authorization

The platform authorization service creates platform back-end tokens that can be used by platform applications to verify and authorize access to its back-end services. These authorization-tokens will replace authentication tokens sent by front-end systems and include on top of the identity of the user (its email and tenant identifier) the list of roles and claims assigned to this user for the given application being served.

Read more: [Authorization Introduction](https://docs.platform.navify.com/guides/platform-access-control/authorization-service/introduction)

## Managing a tenant

The NAVIFY Platform provides the ability to expose applications to multiple tenants with various levels of privileges. Allowing administrators to manage tenants and users of their applications. It allows for a grouping of users under a tenant, where tenants subscribe to an application. It defines specific roles such as tenant administrator, and tenant user.  

Read more: [Tenant management](https://docs.platform.navify.com/guides/platform-access-control/tenant-management)

## Managing users in the context of a tenant

One of the main components of tenant management is the management of users in that tenant. Our user management APIs allow your tenant administrators to manage the users within a specific tenant.

Read more: [User Management](https://docs.platform.navify.com/guides/platform-access-control/user-management)

## Managing user roles and claims

The other main component of tenant management is the management of roles and claims that a user has in a specific application in the context of a tenant. This is an integral part of the authorization that the platform performs and the configuration of the application done during application registration.

Read more: [Roles management concept]

If you want to do a deep dive into things start with reading Platform and [How it Works](https://docs.platform.navify.com/guides/platform-services/how-the-platform-works-folder/how-the-platform-works) and then go directly to our [API References](https://docs.platform.navify.com/api-references) page.

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
