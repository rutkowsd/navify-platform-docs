# Platform Role Management

## Defining roles and claims

Roles and Claims are **defined for each application** as part of the Application Registry manifest. The roles and claims defined for an application are required whenever a request is received for either:

* to generate a new Platform token (via token endpoint)
  * This roles, claims, user uuid, and tenant uuid data will be used to generate the Platform token and will only live in back end services.
* or, calls from an application UI component
  * When called from a UI component, the request will return a smaller set of data that will be available to Front End clients and consist only of roles (claims, User Uuid, and Tenant Uuid will not be exposed). The request will still require the user to be signed in and have proper credentials.

## How we define Roles and Claims

| Concept | Definition  |
| --- | --- |
| Role | Role is a set of Claims: assigning a Role to a user will provide this user with associated Claims.|
|Claim | Claims are a method of providing information about an user rather than group of users. Claims are internal data and are typically not exposed to end users.|

## How our roles service works

The Roles management service matches the claims to specific roles as defined by the product team in the manifest file. The mapping of what access level is granted to a user within the application based on the role assigned is **up to the application team**

An example is shown below for how our HelloWorld is configured. In table 1 the application is HelloWorld which has two defined roles: Clinician and Administrator. The two roles have multiple claims assigned to them such as , viewProducts and addProducts.

Table1: Example of Roles and Claims for HelloWorld App
| Application | Role | Claims |
| --- | --- | --- |
| HelloWorld | Clinician | viewProducts, addProducts |
| HelloWorld | Administrator | manageClinicians, addProducts, viewProducts |

Once your roles are setup managing your application roles and claims on the NAVIFY platform is possible through calls to the platform APIs. Tenant administrators manage individual tenant user roles by assigning these users an application roles.

## Read more

* [Registering an app with platform](https://docs.platform.navify.com/guides/product-registration/app-registry)
* [The manifest file](https://docs.platform.navify.com/guides/product-registration/manifest-file)
* [How to use the role management APIs](https://docs.platform.navify.com/guides/ac-guides/howto-role-apis)
* [Role API references](https://docs.platform.navify.com/api-references)

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
