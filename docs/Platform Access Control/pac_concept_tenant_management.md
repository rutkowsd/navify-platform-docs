# Tenancy and Tenant Management

Think of a marketplace, where each tenant (hospital, clinic, facility) can view applications offered by Roche. That tenant can then choose the applications they want to subscribe to and this way providing access to it for all its users.

The NAVIFY Platform provides the ability to expose applications to multiple tenants with various levels of privileges. For this purpose, we expose a Platform Tenant Management service that allows tenant administrators to manage their tenants and the users of those tenants.

At its core our tenant management service offers the capabilities to

* Manage your tenant subscriptions to available applications

* Manage your tenant information
* Manage your users of that tenant (including tenant administrators)

Currently not all **CRUD** operations are available to product application teams, meaning some of the operations are still restricted only to Platform Administrators. This means that you must submit a request to our team to execute that action.

See our API Reference documentation for available operations and roles which can perform the operation. [Tenant Resources](https://docs.platform.navify.com/api-references/tenant-resources-apis)

## Platform features and actions available for tenants

### Requesting a tenant

Request the creation of a tenant by contacting one of our platform Genius or through our [support center](https://navifypoc.atlassian.net/servicedesk/customer/portal/1). The diagram below shows the basic tenant creation flow and resources the tenant administrator can manage.

>NOTE: When requesting a tenant ideally you would have registered an application beforehand. Without an application and its defined roles you will not be able to assign a role to tenant users. See [Application Registry Service](https://docs.platform.navify.com/guides/product-registration/app-registry) for more information.

[![Tenant Management](https://s3.amazonaws.com/user-content.stoplight.io/18223/1567163910637)](https://s3.amazonaws.com/user-content.stoplight.io/18223/1567163910637)

### Tenant Administrators

When a tenant is created a tenant administrator is created for it based on the initial tenant creation request. The tenant administrator should have received an activation email when during the tenant creation process.

Tenant administrators can easily delegate their responsibilities by creating new tenant administrators via our apis [endpoint: [Create admin user](https://docs.platform.navify.com/api-references/tenant-resources-apis/admin-user-management/createusingpost)]

The operations that a tenant administrator can perform are listed in our [API reference](https://docs.platform.navify.com/api-references/tenant-resources-apis) pages for each endpoint

### Setting up multi factor authentication (MFA) for a tenant

The platform offers the capability for a tenant to enable 2FA based on a text (SMS) message to a tenant users mobile phone. This is a feature (policy) that is enabled for a tenant and not per product. Hence any applications that the tenant is subscribed or will subscribe in the future to will require 2FA once enabled.

Read more on setting up 2FA for your tenant [TO DO: Create separate doc and link to]

### Setting up federated access for a tenant

The platform offers the capability for a tenant to set up their prefered Identity Provider (IdP). The configuration uses the SAML 2.0 protocol to enable the management of users with for example the facilities internal active directory.

Read more on setting up federated access for your tenant [TO DO: Create separate doc and link to]

## Read more

* Setting up a tenant prefered IdP
* [Setting up Multi Factor Authentication (MFA)](https://docs.platform.navify.com/guides/platform-access-control/multi-factor-authentication)
* User Activation Steps
* [Multitenancy concept](https://docs.platform.navify.com/guides/miscellaneous/multi-tenancy)

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
