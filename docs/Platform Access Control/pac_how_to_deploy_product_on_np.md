# Deploying a product on the NAVIFY Platform

In this document we will go through the process from the very beginning i.e. knowing who to contact first, then the deployment of product onto an AWS account, next registering the product with the platform, and from there trying out calls to the NAVIFY platform access control services.

## Before we get started

The NAVIFY Platform has 2 offerings:

* The **“Secure Cloud Infrastructure”** offering: we offer a SecOps stack - you are provided with an AWS account that offers secure infrastructure + tooling around: hardened AMI, system logs aggregation, account monitoring, HITRUST and HIPAA compliance

* The **“Platform Access Control”** offering: in addition to Secure Cloud Infrastructure offering, we offer micro-services as foundation features provided by the platform: authentication, access control and user management

Therefore in this document we will touch upon both of the offerings as we deploy on secure cloud infrastructure and register our application with the NAVIFY platform and subscribe it to a tenant.

Read more on [being on the platform](https://docs.platform.navify.com/guides/getting-started/being-on-the-platform)

## Step 1: Get started

Each team who wants to host a new application on the platform is called a **“product team”**.
The product team will be assigned a **“Genius”**; this is a dedicated person to support you on your journey to adopt the platform.

To do so, use the Service Desk (SD) portal to get in contact with a genius ([submit a request](https://navifypoc.atlassian.net/servicedesk/customer/portal/1/group/7/create/27)):

The platform genius will come back to you to present you the NAVIFY platform, and to analyze your needs.

## Step 2: Get a product AWS Account

The genius will request a new AWS account and user creation for you. You will be able to follow the progress of the process as your email address will be set as a watcher for this request ([submit request](https://navifypoc.atlassian.net/servicedesk/customer/portal/1/group/1/create/28)).

The platform genius will need specific information in order to place the request ([read more here](https://docs.platform.navify.com/guides/platform-secure-cloud-infrastructure/platform_account/get-account)).

![Diagram - How to request your platform account](https://s3.amazonaws.com/user-content.stoplight.io/18223/1562586138772 "Diagram - How to request your platform account")

You will be provisioned a new AWS account to host your application. You can find the details of what you can expect in the NAVIFY Platform account [read more here](https://docs.platform.navify.com/guides/platform-secure-cloud-infrastructure/platform_account/np-provisioned-account).

**In case you are not familiar with AWS**, here is a blog article describing how to login to your account, and how to deploy applications:
* https://info.platform.navify.com/2019/10/18/welcome-to-navify-platform-and-now-what/
* Also [read more here](https://docs.platform.navify.com/guides/platform-secure-cloud-infrastructure/platform_account/how-to-work-with-your-account)

## Step 3: Registering a domain name

The concept is to deploy your API gateway (called **Product Gateway**), as entry point for your back-end (ECS cluster, lambdas, load-balancer, etc...)

Read more on [How Platform Works](https://docs.platform.navify.com/guides/platform-services/how-the-platform-works-folder/how-the-platform-works)

Your application will be deployed behind an API Gateway, so you need now to create a domain name to make it publicly available on the internet with a friendly name.

Your domain name must end with `“platform.navify.com”`.

> <p class="info_notes"><b>NOTE: </b>being under the <b>navify.com</b> domain is a hard requirement from the platform. This is because our SecOps team has control over the domain and second it is also coupled with our platform access control services.</p>

The naming convention for your application DNS is: `<YOUR_APPLICATION_NAME>`.platform.navify.com

To do so, login to your AWS account with provided credentials, open the admin console and go to Route 53.

Create a new Hosted Zone in AWS, using the domain **platform.navify.com**:

![Route53 console pic](https://s3.amazonaws.com/user-content.stoplight.io/18223/1576682529682)

Read more here:
* Step by step instructions from [AWS on creating hosted zones](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/CreatingHostedZone.html)
* More on [AWS hosted zones in AWS](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/hosted-zones-working-with.html)

The next step will be to place a service desk request to add DNS delegation on the name servers (NS) ([submit request](https://navifypoc.atlassian.net/servicedesk/customer/portal/1/group/1/create/85)):

### Sample of a filled out request below

![DNS Delegation request](https://s3.amazonaws.com/user-content.stoplight.io/18223/1576682548308)

In the request, copy the values of your NS record:

* Name for the **“Product Domain name”**
* Value for the **“Server names”**

All requests to domain **platform.navify.com** will land on the platform. This delegation allows to redirect the calls to `<YOUR_APPLICATION_NAME>.platform.navify.com`, from the platform to your newly created hosted zone (in your AWS account).

## Step 4: Deploy your application

In the AWS account that has been provisioned for you (your **Product Account**), deploy your application:

* A load-balancer
* The back-end
* The front-end

The network diagram looks like this:

![network diagram](//s3.amazonaws.com/user-content.stoplight.io/18223/1576682564121)

*The API gateways are provided by the NAVIFY Platform team.*

The requests are flowing through cloudflare to reach the NAVIFY platform APIGW (called **“Front door”**). The NAVIFY platform checks the authentication, and forwards the requests to your application (named **“product”** in the NAVIFY Platform terminology).

To know more, read [how the platform works](https://docs.platform.navify.com/guides/platform-services/how-the-platform-works-folder/how-the-platform-works)

### Start with a demo product

**Tip:** an example is provided with the **“helloworld”** application, which shows:

* How to use TerraForm to deploy your infrastructure and your application
* How to use the `@dia/auth/lib` to authenticate on the platform from front-end

We do recommend to deploy the hello world application as the first product, as it will illustrate the process for onboarding an app on the platform, and will provide a coding example. The source code can be found [here](https://stash.intranet.roche.com/stash/projects/NAVIFY_PRODUCT_STARTER/repos/helloworld_product/browse), you can follow the README.md file for guidance.

All the necessary information is provided in the README.md file of the **Hello World Product** project which you can access in our public repository [Hello World Product repo](https://stash.intranet.roche.com/stash/projects/NAVIFY_PRODUCT_STARTER/repos/helloworld_product/browse)

> <p class="info_notes"><b>NOTE: </b>the provided TerraForm scripts create an SSL certificate for your domain. <b>Domain registration must be completed for this.</b></p>

### Front-end authentication library

The source code for `@dia/auth` library can be found here [dia.auth](https://bitbucket.org/rochedis/auth-ui/src/427b020ad3ec665b0bf1928d6bc58cfc5d073152/libs/dia/auth/)

Read more on the [Auth library and UI](https://docs.platform.navify.com/guides/platform-access-control/pac-authentication-intro) as well as the Auth Library Guides: [Using Auth Library](https://docs.platform.navify.com/guides/ac-guides/how-to-use-auth-lib) and [Customizing Auth UI](https://docs.platform.navify.com/guides/ac-guides/howto-cust-auth-ui)

## Step 5: Register your application

You are probably ready now to register your application with the platform. This means creating a record in the Platform for the application you deployed, in order to manage the users & roles, and to link your domain & your product API Gateway to the platform.

First step is to create a manifest file for your application. It will contain all the necessary information to register your product configuration with the platform. It is basically a JSON file that gives required configuration. Here is some help to explain [how to fill out the manifest file](https://docs.platform.navify.com/guides/product-registration/manifest-file).

The next step is to submit a SD request “Register Product” ([submit request](https://navifypoc.atlassian.net/servicedesk/customer/portal/1/group/1/create/24))

After your application is registered, you will be provided with your **application ID** (application alias is taken from the Manifest file you provided)

## Step 6. Request a Tenant (if you don’t already have one)

### Tenancy concept

Your application does exists in the scope of a “Tenant”. A tenant is a facility, e.g. a hospital,  clinic, lab, etc. Hence you will need a Tenant that will subscribe to your application. Creating a tenant is very simple, raise a request here ([submit request](https://navifypoc.atlassian.net/servicedesk/customer/portal/1/group/1/create/23))

Here is a list of properties you should have handy when requesting a Tenant (checkout the API Reference and the [Create a tenant endpoint](https://docs.platform.navify.com/api-references/tenant-resources-apis/tenant-management/createusingpost-1):

| Property | Required | Additional Information |
| --- | --- | --- |
| Address | False | "type": "string", "minLength": 0, "maxLength": 256 |
| Alias | True | "type": "string", "minLength": 1, "maxLength": 256, "pattern": "^[a-z0-9-]*$" |
| City | False | ,  "minLength": 0, "maxLength": 256|
| countryCode | False |  "type": "string",  "minLength": 0, "maxLength": 2|
| groupType | True | "type": "string", Allowed values: PLATFORM, GOOGLE|
| name | True | Type: string, Min Length: 1, Max Length: 256 |
| postalCode | False | Type: string, Min Length: 0, Max Length: 20 |
| rexis | False | Type: string, Min Length: 0, Max Length: 256 |
| tenantEmail | False | Type: string, Min Length: 0, Max Length: 256 |

Also you will need to provide the initial Tenant Admin user (checkout the API Reference and the [Create admin user](https://docs.platform.navify.com/api-references/tenant-resources-apis/admin-user-management/createusingpost) endpoint):

| Property | Required | Additional Information |
| --- | --- | --- |
| firstName | True | "type": "string", "minLength": 1, "maxLength": 256 |
| lastName | True | "type": "string", "minLength": 1, "maxLength": 256 |
| userEmail | True | "type": "string", "minLength": 1, "maxLength": 256 |

After your tenant is registered, you will be provided with:

* The tenant details: tenant alias and tenant ID
* A tenant admin user: it is a special user that is allowed to create new users in your tenant, and assign them roles. You will get username and password to login.

## Step 7. Subscribe your application with your tenant

Now that you have your application registered with the NAVIFY platform and a tenant created you can subscribe your tenant to your application. 
This will allow you to retrieve information about users and roles for your application within the context of that specific tenant. 

### Test our platform APIs

Try out our postman collection and test the available endpoints. Everything is ready to use out of the box with the Postman collections. Find more information and resources on our [postman page](https://docs.platform.navify.com/guides/developer-tools/postman-swagger)

The Postman collections includes the end points for Hello World demo product. You can see how this is working, and what is required in the request headers.

> <p class="info_notes"><b>NOTE: </b>Examples for the Hello World demo are in collection “Hello-World_app”</p>

## Step 8: Manage users and roles for your application

Your application has been registered with the Manifest file. In this file, you have defined the roles you need for your application.

To do so, you will need your tenant admin account to manage users in your tenant, and give them some of the roles defined. You can:

* Manage users in your tenant, with the Postman collection named `“NP_Tenant_Microservice/UserResource”`
* Assign/Unassign Roles to users, with the Postman collection named `“NP_Role_Microservice/Tenants”`

Alternatively, you can use the NAVIFY CLI tool, if you are more familiar with command line. Behind the scenes, it targets the same API.

You can find this NAVIFY CLI tool following this link: 
[https://bitbucket.org/platformtrainingteam/navifycli/src/master/](https://bitbucket.org/platformtrainingteam/navifycli/src/master/), instructions are in the ReadMe.md

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>