# Platform login

This is abstracted from you in the Auth-UI library, but in case you were curious, this is how login to the platform works for browsers.

1. [Get Okta Session Token](https://developer.okta.com/docs/api/resources/authn/#primary-authentication) (via username+password, SAML federation, or whatever methods Okta supports)
2. Send request to [Okta's OIDC authorize endpoint](https://developer.okta.com/docs/api/resources/oidc/#authorize), carrying that session token.
3. Your request will be redirected back to the platform’s /login endpoint, which will exchange your auth token for an access key, and set that value in a cookie. Now all user requests on the platform domain will carry this cookie.

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
