# How to register your application

1. [Download](https://stash.intranet.roche.com/stash/projects/NAVIFY_PRODUCT_STARTER/repos/np_developer_experience/browse/navify-platform-demo-manifest.json) the sample application manifest file

2. Fill in the details specific to your product configuration

3. [Contact](https://navifyplatform.roche.com/how-can-we-help/) your friendly neighbourhood Platform Genius

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
