# Shared Responsibility Model - Roles & Responsibilities

## Platform Team

| Role | Description |
| --- | --- |
| Platform Owner | Accountable leader for the security and operations of the NAVIFY Platform. Has primary decision-making power regarding changes and operation of this platform. |
| Platform Architect | Responsible leader for the design / architecture of the interlocking toolchain components, including integrations, versioning, qualification, and quality assurance. |
| Platform SecOps Team | Primarily responsible for ensuring the cybersecurity of the Platform and the products it hosts, these engineers focus on ensuring that appropriate access is maintained for the AWS-hosted cloud space, and that indicators of cyberattacks are tracked and managed. |
| Platform DevOps Team | Primarily responsible for the performance and availability of the Platform, these engineers focus on monitoring load and capacity of the AWS platform. |
| Platform Genius | These representatives act as the ambassadors between Product and Platform Teams, and are the first point of contact when Product teams have concerns or development needs from the Platform Team. Unlike the other roles below, one Genius may support multiple products. |

## Product Team

| Role | Description |
| --- | --- |
| Product Owner | Accountable leader / product manager for a given Product. |
| Product Security Champion | Accountable representative for security of an individual product. Interfaces most often with the NAVIFY SecOps Engineers. |
| Product DevSecOps Team | Builds / tests / deploys / administers innovative new software products. |

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
