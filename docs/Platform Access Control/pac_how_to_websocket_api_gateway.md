# AWS Websocket API Gateway Implementation

## uPath example

The uPath application implements push notification via AWS WebSocket API gateway. The uPath web application creates a WebSocket connection with the uPath WebSocket API gateway then waits to be notified by the server.  The uPath WebSocket API gateway `$connect` hander stores the connection id and user id in the DB.  Once there is a message to be pushed, the server service will query the DB for the user id to get the connection(s) and use them to send the messages back to the client(s). On `$disconnect`, uPath query the DB and remove the entry.

Similar to uPath, the following example manages the connection ids in Dynamodb and uses a java pojo to send messages back to the user.

* Client issues a secure websocket connection to the websocket api gateway,  passing in `access token` and `x-navify-tenant` in cookie. e.g. [wss://upath-websocket.upath.platform.navify.com](wss://upath-websocket.upath.platform.navify.com).   
> Please note: if using javascript websocket library, users must put `X-Navify-Tenant` in the secured cookie because it does not support passing custom header.
* Websocket api gateway `$connect` route invokes the Platform Product authorizer with `X-Navify-Tenant` and `Access Token` in cookie.  Platform product authorizer validates the user and issues a policy for websocket.
* Api gateway created a websocket connection id, invokes the lambda  configured in the `$connect` request integration. The `$connect` lambda store connection id and user id in Dynamo database.
* Client now connected and waiting for notification.
* A Java PoJo search user id in the DynamoDB to get WebSocket connection id(s) and send a message to the client.  
Note: make sure the Java PoJo has an Execute API role assigned. If connection is no longer valid, an error will be raised. The PoJo can then delete the connection from the table.

* When the client disconnects from the websocket api gateway, the `$disconnect` route is executed.  The disconnect lambda configured in the request integration will be invoked to clean up the entry in the db.

![insert image01 here](https://s3.amazonaws.com/user-content.stoplight.io/18223/1581347787046)

## Websocket API Gateway Configuration Steps
  
1. Login AWS console -> lambda and deploy the attached lambda functions.
2. Create a DynamoDB table with user id and connection id columns.
3. Login AWS console -> API Gateway and create an api gateway with websocket type.
4. After saving, there should be 3 default routes, `$connect`, `$default`, `$disconnect`.
5. Create an authorizer as described above, pointing it to platform product authorizer lambda arn. e.g `arn:aws:lambda:us-west-2:468280580845:function:navifyappdev-product-authorizer`

![insert image02 here](https://s3.amazonaws.com/user-content.stoplight.io/18223/1581347799646)

6. Go to routes -> `$connect`, under route request, select the authorizer created in step 3.

![insert image03 here](https://s3.amazonaws.com/user-content.stoplight.io/18223/1581347808101)

7. Go to routes -> `$connect`, under integration route request, select as below and add connection lambda arn in the lambda function text box. Choose everything as in the image.

![insert image04 here](https://s3.amazonaws.com/user-content.stoplight.io/18223/1581347818198)

8. For other routes, we dont configure the authorizer.  This only support in the `$connect` as authorization is at the connection.  To configure the `@disconnect` lambda handler go to routes-> `$disconnect` and under integration route request specify the lambda arn.

![insert image05 here](https://s3.amazonaws.com/user-content.stoplight.io/18223/1581347829226)

## Connect Lambda

``` python

# Copyright 2010-2019 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License"). You
# may not use this file except in compliance with the License. A copy of
# the License is located at
#
# http://aws.amazon.com/apache2.0/
#
# or in the "license" file accompanying this file. This file is
# distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF
# ANY KIND, either express or implied. See the License for the specific
# language governing permissions and limitations under the License.


import json
import logging
import os
import boto3
from botocore.exceptions import ClientError

# Set up logging
logging.basicConfig(format='%(levelname)s: %(asctime)s: %(message)s')
logger = logging.getLogger()
logger.setLevel(logging.INFO)


def lambda_handler(event, context):
    """Example WebSocket $connect Lambda function

    :param event: Dict (usually) of parameters passed to the function
    :param context: LambdaContext object of runtime data
    :return: Dict of key:value pairs
    """

    # Log the values received in the event and context arguments
    logger.info('$connect event: ' + json.dumps(event, indent=2))
    if 'connectionId' in event:
        connectionId = event["connectionId"]
    else:
        connectionId = event["requestContext"]["connectionId"]

    logger.info(f'$connect event["requestContext"]["connectionId"]: {connectionId}')

    # Retrieve the name of the DynamoDB table to store connection IDs
    table_name = os.environ['TableName']

    # Was a user name specified in a query parameter?
    user_name = 'Anon'
    if 'queryStringParameters' in event:
        if 'name' in event['queryStringParameters']:
            user_name = event['queryStringParameters']['name']

    # Store the connection ID and user name in the table
    item = {'connectionId': {'S': connectionId},
            'userName': {'S': user_name}}
    dynamodb_client = boto3.client('dynamodb')
    try:
        dynamodb_client.put_item(TableName=table_name, Item=item)
    except ClientError as e:
        logger.error(e)
        raise ConnectionAbortedError(e)

    # Construct response
    response = {'statusCode': 200}
    return response


```

## Disconnect Lambda

```python

# Copyright 2010-2019 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License"). You
# may not use this file except in compliance with the License. A copy of
# the License is located at
#
# http://aws.amazon.com/apache2.0/
#
# or in the "license" file accompanying this file. This file is
# distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF
# ANY KIND, either express or implied. See the License for the specific
# language governing permissions and limitations under the License.


import json
import logging
import os
import boto3
from botocore.exceptions import ClientError

# Set up logging
logging.basicConfig(format='%(levelname)s: %(asctime)s: %(message)s')
logger = logging.getLogger()
logger.setLevel(logging.INFO)


def lambda_handler(event, context):
    """Example WebSocket $disconnect Lambda function

    :param event: Dict (usually) of parameters passed to the function
    :param context: LambdaContext object of runtime data
    :return: Dict of key:value pairs
    """

    # Log the values received in the event and context arguments
    logger.info('$disconnect event: ' + json.dumps(event, indent=2))
    if 'connectionId' in event:
        connectionId = event["connectionId"]
    else:
        connectionId = event["requestContext"]["connectionId"]
    logger.info(f'message event["requestContext"]["connectionId"]: {connectionId}')

    # Retrieve the name of the DynamoDB table to store connection IDs
    table_name = os.environ['TableName']

    # Remove the connection ID from the table
    item = {'connectionId': {'S': connectionId}}
    dynamodb_client = boto3.client('dynamodb')
    try:
        dynamodb_client.delete_item(TableName=table_name, Key=item)
    except ClientError as e:
        logger.error(e)
        raise ValueError(e)

    # Construct response
    response = {'statusCode': 200}
    return response

```

## Notification PoJo

```java

import com.amazonaws.services.apigatewaymanagementapi.*;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.apigatewaymanagementapi.model.*;
import java.nio.charset.*;
import java.nio.CharBuffer;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.*;
import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.spec.*;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;


public class sendmsg {
  private static final String apiUrl = "https://ha9dsppok1.execute-api.us-west-2.amazonaws.com/test";

 //  private static final String apiUrl = "https://ixffmxvrt8.execute-api.us-west-2.amazonaws.com/test";
  private static final String region = "us-west-2";
  protected String connectionTBL = "websocket-connection-poc";
  protected String tesCconnectionId;

  protected void getUserConnections(String userId) {
    AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard()
            .withRegion(Regions.US_WEST_2).build();
  
    Map<String, AttributeValue> expressionAttributeValues = new HashMap<>();
    expressionAttributeValues.put(":name", new AttributeValue().withS(userId));


    ScanRequest scanRequest = new ScanRequest()
            .withTableName("websocket-connection-poc")
            .withFilterExpression("userName = :name")
            .withExpressionAttributeValues(expressionAttributeValues)
            .withProjectionExpression("connectionId, userName")
            .withLimit(10);


      ScanResult scan = client.scan(scanRequest);

      for (Map<String, AttributeValue> item : scan.getItems()){
          System.out.println("sending message to connection: " + item.get("connectionId"));
          send(item.get("connectionId").getS(),"Hello "+item.get("userName").getS()+ " , you have a status update on your order.");
      }

  }

   public static PostToConnectionResult send( String connectionId, String msg) {

     AmazonApiGatewayManagementApiClientBuilder builder = AmazonApiGatewayManagementApiClient.builder();
     EndpointConfiguration config = new EndpointConfiguration(apiUrl, region);
     builder.setEndpointConfiguration(config);
     AmazonApiGatewayManagementApi client = builder.build();
     PostToConnectionRequest request = new PostToConnectionRequest();
     request.setConnectionId(connectionId);
     Charset charset = Charset.forName("UTF-8");
     CharsetEncoder encoder = charset.newEncoder();
     try {
       ByteBuffer buff = encoder.encode(CharBuffer.wrap("sendmsg java: " + msg));
       request.setData(buff);
       PostToConnectionResult result = client.postToConnection(request);
       System.out.println("Result of websocket send:\n" + result);
       return result;
     }catch (Exception e) {
       e.printStackTrace();
     }
      return null;
 }

   public static void main(String[] args) {
     String userId="";
     if(args[1] == null || args[1].trim().isEmpty()) {
       System.out.println("Missing require user Id");
       System.exit(1);
     } else {
       userId = args[0];
     }

     sendmsg client = new sendmsg();
     client.tesCconnectionId = args[1];
     client.getUserConnections(userId);
 }
}

```

```xml

<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
   <modelVersion>4.0.0</modelVersion>

   <groupId>websocket-poc</groupId>
   <artifactId>server-wsclient</artifactId>
   <version>1.0-SNAPSHOT</version>

   <packaging>pom</packaging>

   <name>serverwsclient</name>
   <description>AWS Server websocket client</description>

   <modules>
       <module>serverwsclient</module>
   </modules>

   <properties>
       <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
       <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
       <!-- TODO change to '11' later when build slave docker image is available -->
       <java.version>1.8</java.version>
       <maven.compiler.source>${java.version}</maven.compiler.source>
       <maven.compiler.target>${java.version}</maven.compiler.target>
       <build.maven-compiler-plugin.version>3.8.0</build.maven-compiler-plugin.version>
       <infrastructure.maven.compiler.source>${maven.compiler.source}</infrastructure.maven.compiler.source>
       <infrastructure.maven.compiler.target>${maven.compiler.source}</infrastructure.maven.compiler.target>
       <spring-cloud.version>Greenwich.M2</spring-cloud.version>
       <artifactory.snapshot.repo>libs-snapshot-local</artifactory.snapshot.repo>
       <output.dir>${project.basedir}/src/docs</output.dir>
   </properties>

   <dependencyManagement>
       <dependencies>
           <dependency>
               <groupId>com.amazonaws</groupId>
               <artifactId>aws-java-sdk-bom</artifactId>
               <version>1.11.657</version>
               <type>pom</type>
               <scope>import</scope>
           </dependency>
       </dependencies>
   </dependencyManagement>

   <dependencies>
       <dependency>
           <groupId>com.amazonaws</groupId>
           <artifactId>aws-java-sdk-ec2</artifactId>
       </dependency>
       <dependency>
           <groupId>com.amazonaws</groupId>
           <artifactId>aws-java-sdk-s3</artifactId>
       </dependency>
       <dependency>
           <groupId>com.amazonaws</groupId>
           <artifactId>aws-java-sdk-dynamodb</artifactId>
       </dependency>
       <dependency>
           <groupId>com.amazonaws</groupId>
           <artifactId>aws-java-sdk-apigatewaymanagementapi</artifactId>
       </dependency>

   </dependencies>
   <build>
       <pluginManagement>
           <plugins>
           <plugin>
               <groupId>org.apache.maven.plugins</groupId>
               <artifactId>maven-compiler-plugin</artifactId>
               <version>${build.maven-compiler-plugin.version}</version>
               <configuration>
                   <source>${infrastructure.maven.compiler.source}</source>
                   <target>${infrastructure.maven.compiler.target}</target>
               </configuration>
           </plugin>
           <plugin>
               <groupId>org.springframework.cloud</groupId>
               <artifactId>spring-cloud-contract-maven-plugin</artifactId>
               <version>${spring-cloud.contract.plugin.version}</version>
           </plugin>
           <plugin>
               <groupId>org.apache.maven.plugins</groupId>
               <artifactId>maven-assembly-plugin</artifactId>
               <version>${maven.assembly.plugin.version}</version>
           </plugin>
           </plugins>

       </pluginManagement>
       <plugins>
           <plugin>
               <groupId>org.apache.maven.plugins</groupId>
               <artifactId>maven-surefire-plugin</artifactId>
               <version>2.22.1</version>
               <configuration>
                   <systemPropertyVariables>
                       <io.springfox.staticdocs.outputDir>${output.dir}</io.springfox.staticdocs.outputDir>
                   </systemPropertyVariables>
               </configuration>
           </plugin>
       </plugins>


   </build>

</project>

```

## $default - Optional configuration

Optionally, users can configure the `$default` integration request to specify the sendMsg lambda which will respond to a websocket message.

```python

// Copyright 2010-2019 Amazon.com, Inc. or its affiliates. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License"). You
// may not use this file except in compliance with the License. A copy of
// the License is located at
//
// http://aws.amazon.com/apache2.0/
//
// or in the "license" file accompanying this file. This file is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF
// ANY KIND, either express or implied. See the License for the specific
// language governing permissions and limitations under the License.


const AWS = require('aws-sdk');

const ddb = new AWS.DynamoDB.DocumentClient({ apiVersion: '2012-08-10' });

const { TableName } = process.env;

exports.lambda_handler = async (event, context) => {
  // console.log(`Environment Variables:\n` + JSON.stringify(process.env, null, 2));
  // console.log(`WebSocket sendmsg: DynamoDB Table: ${TableName}`);
  console.log(`WebSocket sendmsg: event argument:\n` + JSON.stringify(event,null,2));

  // Retrieve all current connections
  let connectionData;
  try {
    connectionData = await ddb.scan({ TableName: TableName,
                                      ProjectionExpression: 'connectionId' }).promise();
  } catch (e) {
    console.log(e.stack);
    return { statusCode: 500, body: e.stack };
  }

  // Create management object for posting the message to each connection
  const apigwManagementApi = new AWS.ApiGatewayManagementApi({
    apiVersion: '2018-11-29',
    endpoint: event.requestContext.domainName + '/' + event.requestContext.stage
  });

  // Retrieve the message
  const message = JSON.parse(event.body).msg;
  console.log(`WebSocket sendmsg: Received message "${message}"`);

  // Send the message to each connection
  const postCalls = connectionData.Items.map(async ({ connectionId }) => {
    try {
      await apigwManagementApi.postToConnection({ ConnectionId: connectionId, Data: message }).promise();
    } catch (e) {
      if (e.statusCode === 410) {
        console.log(`Found stale connection, deleting ${connectionId}`);
        await ddb.delete({ TableName: TableName, Key: { connectionId } }).promise();
      } else {
        throw e;
      }
    }
  });

  try {
    await Promise.all(postCalls);
  } catch (e) {
    return { statusCode: 500, body: e.stack };
  }

  return { statusCode: 200, body: 'Data sent.' };
};


```

## Test

To test the websocket api gateway, users can install websocat utility.

`Websocat wss://upath-websocket.upath.platform.navify.com --header “authorization: sssss” --header “X-Navify-Tenant labcorb”`

To send a message just type {“msg”: “anything here”}.

To disconnect, type ctrl-d.

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
