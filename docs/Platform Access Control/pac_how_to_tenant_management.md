# Using the platform tenant APIs

Platform currently exposes only API endpoints for managing tenants, users, and roles. There is no user interface (UI) available for products yet.

Products (applications) will use the Tenant Administrator role to call the majority of the API endpoints. The operations should allow you the freedom to manage your tenant and its users without the need to contact our service desk. The operations that can only be called via the Platform Administrator role can be requested through our service desk portal.

**NOTE:** With the next steps we assume that you have already received your tenant detail information (i.e. a tenant is created), also your application is registered with the platform [link out to platform App Reg.].

## Try out our postman collection

We provide you a complete ready-to-go postman collection that you can use to ease your development or testing.

The collection also includes

* a collection of platform environment configurations.
* pre-request scripts that take care of generating the necessary Okta token.
* post-request scripts to validate the endpoints answers.

Download the [NAVIFY Platform Postman Collection](https://stash.intranet.roche.com/stash/projects/NAVIFY_PRODUCT_STARTER/repos/np-postman/browse) and see the readme file for instructions.

## Calling endpoints without using postman

In general all calls you make to the platform will be within the context of a single tenant. The endpoints that you use will always have the `X-Navify-Tenant` header and use one specific token for a specific tenant. The only exception is the `/userprofile` endpoint that can be called without the `X-Navify-Tenant` header.

It is good to have the following information handy for your calls to succeed. It’s useful to store these values somewhere until you fully integrate with our Auth UI and Auth Lib.

### Path information

* `TenantId`: This is UUID generated automatically for your tenant during creation

### Header information

* `Authorization`: the JWT bearer identity token generated for the specific user calling the endpoint
* `X-Navify-Tenant`: the Alias you specified for your tenant during tenant creation e.g. “my-hello-world-001”. This is needed when making any calls in context of a single tenant.

You can also use our portals native Http Request Maker to post requests to one of our environments.

## Integration with the Auth UI

Once you fully integrate with our Auth Library and Auth UI this information will be automatically injected into your calls towards one single tenant. Therefore you don’t have to worry about developing this logic in your app yourself. However be mindful that the tenant alias that is in the header of the URL like for example ‘tenant-alias.webapp.roche.com’ means that you are always in the context of that specific single tenant. The Auth library passes and refreshes identity tokens through a cookie named access-token rather than the Authorization HTTP header. Still, you will be responsible for passing `X-Navify-Tenant` header.

## Tenant Subscriptions

The management of tenant subscriptions is still done through the [service desk portal](https://navifypoc.atlassian.net/servicedesk/customer/portal/1), so if you’d like to subscribe a tenant to an application or unsubscribe an application from a tenant, you should place a request via our [support center portal](https://navifypoc.atlassian.net/servicedesk/customer/portal/1).

As a tenant administrator what you can do is make sure that you are subscribed to a specific app by calling:

**GET** /platform/tenants/{tenantId}/apps

List all the applications this tenant is subscribed to.

**Sample request**

```bash
curl --request GET \
  --url https://us.api.appdev.platform.navify.com/platform/tenants/92282ea5-a0f8-4a74-91f4-db13b51e0572/apps \
  --header 'Authorization: Bearer eyJraWQiOiJBbHc5bmMxYUlua2hscUtFMjRZai1kSDRMekVxN3h1ZGwzcWNDZGk1NGRBIiwiYWxnIjoiUlMyNTYifQ.eyJ2ZXIiOjEsImp0aSI6IkFULmJ6UXVibUdnaUxsbXc3ZEU0S3BCREJlbDkxTkpfR1pDZkljVXNKbWRqWm8uNHU5KytNRWFsSjJPNUcrZjVBZWsvd2t3d1JQNzBOME1nYVVYREtrb1oyRT0iLCJpc3MiOiJodHRwczovL3JvY2hlcmFwaWQtdGVzdC5va3RhLmNvbS9vYXV0aDIvZGVmYXVsdCIsImF1ZCI6ImFwaTovL2RlZmF1bHQiLCJpYXQiOjE1NzU0NzE3NzQsImV4cCI6MTU3NTQ3MzU3NCwiY2lkIjoiMG9hMjcxZDEyZEd2dGhXd3QyOTciLCJ1aWQiOiIwMHUyN2psc2FrYVFVMVBESTI5NyIsInNjcCI6WyJvZmZsaW5lX2FjY2VzcyIsInByb2ZpbGUiLCJvcGVuaWQiXSwic3ViIjoiZGF3aWQucnV0a293c2tpQHJvY2hlLmNvbSJ9.FJ0vsVXpukOsHC3ghyBvIi6kqvk-3iZ0R5wgabhp-w9WynxO1tDxWMM0NmUzLfKp3cFX13bcjXrx0R88FQAzNA0KS4rrB8z5Xa_ZEcP8OjwNAxSbBe1pic0uWNEemppqWscrpngMmMzHsnHxOqOat-vZwh4wo82D2WTjaWkLe9AK740QVLU_gl1rGYDdFiicJFjqd-n6NLagiEiPIN3A_JlYrO22rtd2-quty9S8Rya2nhIn_4CtvyUfcz_plVF66hvCPm3gP7uE_0uAmgo4nr1i7oP73cLHyqgDWEPM3s7F2LW2rWBnjMHtb-m8OD4SNFvPtPVZ1GY-zdsk6jgI0Q' \
 --header 'X-Navify-Tenant: devx-tenant'
```

**Sample response**

```json
{
    "apps": [
        {
            "appUuid": "f4ac2b28-affe-4f19-83de-2bce785050c7",
            "name": "BDD-Smartesting-20192211012921398-105",
            "displayName": "BDD-Smartesting-20192211012921398-105",
            "alias": "BDD-Smartesting-20192211012921398-105_alias",
            "links": [
                {
                    "rel": "DELETE",
                    "href": "http://tenants:8080/platform/api/v1/tenants/92282ea5-a0f8-4a74-91f4-db13b51e0572/apps/f4ac2b28-affe-4f19-83de-2bce785050c7",
                    "hreflang": null,
                    "media": null,
                    "title": null,
                    "type": null,
                    "deprecation": null
                }
            ]
        },
        {
            "appUuid": "617c4dcb-b17e-4b06-b846-a4c08b9d6e9d",
            "name": "navifyPlatform",
            "displayName": "Nafivy Platform",
            "alias": "platform",
            "links": [
                {
                    "rel": "DELETE",
                    "href": "http://tenants:8080/platform/api/v1/tenants/92282ea5-a0f8-4a74-91f4-db13b51e0572/apps/617c4dcb-b17e-4b06-b846-a4c08b9d6e9d",
                    "hreflang": null,
                    "media": null,
                    "title": null,
                    "type": null,
                    "deprecation": null
                }
            ]
        },
        {
            "appUuid": "f25407bd-2c09-422d-b33a-7996f37233d0",
            "name": "BDDPRODUCT--Drutex-20190411032054649-977",
            "displayName": "BDDPRODUCT--Drutex-20190411032054649-977",
            "alias": "BDDPRODUCT--Drutex-20190411032054649-977_alias",
            "links": [
                {
                    "rel": "DELETE",
                    "href": "http://tenants:8080/platform/api/v1/tenants/92282ea5-a0f8-4a74-91f4-db13b51e0572/apps/f25407bd-2c09-422d-b33a-7996f37233d0",
                    "hreflang": null,
                    "media": null,
                    "title": null,
                    "type": null,
                    "deprecation": null
                }
            ]
        }
    ]
}
```

## Tenant Management

## Tenant User Management

## Tenant Admin Users Management

## Read more

* [Tenancy and tenant management concept](https://docs.platform.navify.com/guides/platform-access-control/tenant-management)
* [API references](https://docs.platform.navify.com/api-references)

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
