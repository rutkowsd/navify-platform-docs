# Developer Tools

In this section we provide you with useful and self-service set of developer tools.

| Tool Name | Purpose |
| --- | --- |
| [jFrog Artifact Management](https://docs.platform.navify.com/guides/developer-tools/jfrog-artifact-management/jfrog-introduction) | jFrog is used by platform to store all their artifacts (e.g. AuthUI library & UI).|
| [Postman Collection & Swagger](https://docs.platform.navify.com/guides/developer-tools/postman-swagger) | A complete ready-to-go postman collections and swagger files to test the platform access control microservices. |
| [Hello World App](https://docs.platform.navify.com/guides/developer-tools/hello-world-app) | A small demo web application that leverage some of the microservices. |
| NP CLI | A command line interface to interact with platform microservices.<br/>Work in progress. |
| NP Java Libraries | Work in progress |
| NP BDD Java Package | Work in progress |

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>