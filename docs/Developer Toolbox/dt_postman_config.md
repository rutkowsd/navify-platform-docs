# Postman Collections

## Postman collections

Here we provide you a complete ready-to-go postman collection that you can use to ease your development or testing.

The collection also includes:

* a collection of platform environment configurations
* pre-request scripts that take care of generating the necessary Okta token.
* post-request scripts to validate the endpoints answers.

Download the [NAVIFY Platform Postman Collection](https://stash.intranet.roche.com/stash/projects/NAVIFY_PRODUCT_STARTER/repos/np-postman/browse) and see the readme file for insctructions.

## Swagger files

You can also build your own collection by downloading the swagger files directly:

| Resource | Swagger Download page |
| --- | --- |
| Application Registry | [Download](https://docs.platform.navify.com/api-references/app-registry)|
| Tenant Resources</br>(incl. Tenant, User, & Roles)| [Download](https://docs.platform.navify.com/api-references/tenant-resources-apis/tenant-resources-apis.oas2.json)|
| Role Resources App UI Management | [Download](https://docs.platform.navify.com/api-references/role-app-ui-resources-apis/role-app-ui-resources-apis.oas2.json) |

### Steps for creating a collection from a swagger

1. Click on the **Import** button in the top left corner of Postman UI.
2. You will see multiple options to import the API doc. Click on the **Choose Files****.
3. Find the downloaded swagger file and click import.
4. Select the **Generate a Postman Collection** checkbox and click **Next**.
5. You will see all your APIs as 'Postman Collection' and can use it from the Postman.

### Steps to configure Postman environment for Platform AppDev

1. Copy and past the below json into a new json file and save it localy.
2. Open Postman and click on the **Manage Environments** gear icon.
3. Select the import button.
4. Select the json file you created in step 1.
5. You are ready to go...

The configuration necessary for you to connect with Postman to our appDev environment  
[See also Platform Environment list](https://docs.platform.navify.com/guides/miscellaneous/platform-environments)

``` json
{
	"id": "e9b009a0-96df-4e7c-b4f6-09ecad410070",
	"name": "appDev",
	"values": [
		{
			"key": "AUTHORIZATION_TOKEN",
			"value": "",
			"enabled": true
		},
		{
			"key": "X-Navify-Tenant",
			"value": "devx-tenant",
			"enabled": true
		},
	
		{
			"key": "baseUrl",
			"value": "https://us.api.appdev.platform.navify.com",
			"enabled": true
		}
	],
	"_postman_variable_scope": "environment",
	"_postman_exported_at": "2019-12-02T13:48:50.926Z",
	"_postman_exported_using": "Postman/7.12.0"
}

```

## Read more

* Read more about Postman on its [offcial documentation](https://learning.getpostman.com/docs/postman/launching-postman/introduction/)

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>