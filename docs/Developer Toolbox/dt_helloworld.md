# Hello World App

## Pre-requisites

The following information is required to proceed with this tutorial. If any of them is missing to you, please contact your Platform Genius.

### Your AWS Credentials

First, make sure you have access to your AWS account. We highly recommend you to enable Multi-Factor Authentication. 

### Your Tenant Information

As the platform lives in a multi-tenancy architecture the following information is required for any usage of our API endpoints:

- **Tenant**: the tenant is used to identify who is using the API. It is usually a facility (lab, hospital, etc…). You should be provided with:
  - The “tenant alias” (e.g. “appdev-demo-tenant-9-alias”): that usually is passed as the “X-Navify-Tenant” HTTP header to every NAVIFY microservices request.
  - The “tenant UUID” (e.g. “8ea0af2e-b1c3-4502-8aec-15eb5884f735”): that usually is passed in URL/resource paths of the NAVIFY tenant management APIs 
- **Tenant admin user**: this is a user-account with admin privileges for your tenant. The user is able to create and manage user accounts within a tenant... You should be provided with Username (being an e-mail address) and Password.

> **Good to know:**
A user-account can only exist in the context of a tenant.
Behind the scenes, users are created in Okta (including the tenant admin users).
Authentication is done through oAuth flow.

## What is the Hello World app?

The “Hello World” app is a minimalistic and small demo web application. It loads a list of drug from a database and render it in a web page with support of authentication against the platform and refresh-token management. Additionally you can create new drugs in the list.

The “Hello World” app goal is not to provide you the full picture of all the platform capabilities but to give you a concrete example on how to onboard a product on the platform using the Access Control microservices and to see how the request flow works for GET and POST requests.

It is a serverless architecture design composed by a
Front-end (a single html page and a single javascript file deployed in a S3 bucket), a back-end (a single python script deployed in AWS Lambda) and Terraform files to deploy both back-end and front-end in an automated way.

### How to Deploy

You can checkout our Roche public repository:
[https://stash.intranet.roche.com/stash/projects/NAVIFY_PRODUCT_STARTER/repos/helloworld_product/browse](https://stash.intranet.roche.com/stash/projects/NAVIFY_PRODUCT_STARTER/repos/helloworld_product/browse)

and follow the readme.txt instructions.

## Read more

- [Multi-tenancy Concept]()
- [Tenant Management Concept]()
- [User Management Concept]()

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>