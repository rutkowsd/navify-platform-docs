# jFrog Actifact Management

JFrog Artifactory is an artifact repository manager, which is entirely technology agnostic and fully supports software created in any language or using any tool.

The platform team stores all the platform artifacts in this DIS managed instance [https://navify.jfrog.io/](https://navify.jfrog.io/).

## Repository Type

Artifactoy has 3 types of repository, namely:

* **Local** : physical and locally managed repository which you can deploy artifacts
* **Remote**: physical repository that serves as a caching proxy for repository managed at remote URL, e.g. [https://registry.npmjs.org](https://registry.npmjs.org), [http://jcenter.bintray.com](http://jcenter.bintray.com). You can remote artifact from a remote repository but you cannot deploy artifact to it
* **Virtual**: an aggregated repository which groups several repositories with the same package type under a common URL

## How to Authenticate

SaaS Artifactory [https://navify.jfrog.io/](https://navify.jfrog.io/) is integrated with Google OAuth 2.0 Provider for Single Sign-On and auto creation of Artifactory users is enabled. For any new user login to [https://navify.jfrog.io/](https://navify.jfrog.io/) via GoogleAuth for the first time, an Artifactory user account will be created.

![jFrog Authentication](https://s3.amazonaws.com/user-content.stoplight.io/18223/1568709672635)

### Troubleshotting

If you get the following error message:

`Received invalid OAuth2 response. state is null`

![jFrog Authentication](https://s3.amazonaws.com/user-content.stoplight.io/18223/1568709687442)

Try to log in by openning a private browser window and start again.

* Chrome, go to menu bar  File → New Incognito Window
* Safari, go to menu bar File → New Private Window
* Firefox, go to menu bar File → New Private Window

## Security and Permission

* Anonymous access is not permitted.
* All new user created in Artifactory will automatic join the `dis-engineering` group.  This group provides read-only permission to all local repositories and deploy/cache permission to all remote repositories.
* To grant or revoke any specific permission, user is required to submit [https://jira.intranet.roche.com/jira/browse/DISDEVOPS](https://jira.intranet.roche.com/jira/browse/DISDEVOPS) ticket

### Groups

| Group | Comment |
|---|---|
| dis-cicd | For build agents |
| dis-engineering | For Developers and QEs |
| readers | For read-only users |

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>