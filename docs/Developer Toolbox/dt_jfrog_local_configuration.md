# jFrog Local Configuration

To compile a NodeJS project locally, you are required to configure .npmrc with your own credentials. Follow the instructions below to generate them.

## Generate API Key

1. Login to [https://navify.jfrog.io/](https://navify.jfrog.io/)
2. You need to generate an API key if you haven't done so. Click on your login name at the top right corner to edit your user profile

    [![login](//s3.amazonaws.com/user-content.stoplight.io/18223/1568709552315)](//s3.amazonaws.com/user-content.stoplight.io/18223/1568709552315)

3. Click on the gear icon to generate an API key

    [![login](//s3.amazonaws.com/user-content.stoplight.io/18223/1568709567032)](//s3.amazonaws.com/user-content.stoplight.io/18223/1568709567032)

4. Copy the API to a text editor. You need this for settings.xml and .npmrc configuration on your local

    [![login](//s3.amazonaws.com/user-content.stoplight.io/18223/1568709581489)](//s3.amazonaws.com/user-content.stoplight.io/18223/1568709581489)

## Configure .npmrc

1. Perform the command below to configure npm registry on your local machine

    ```bash
    npm config set registry https://navify.jfrog.io/navify/api/npm/navify-npm/
    ```

2. Generate basic authentication with the command given and copy its output to `~/.npmrc`

    ```bash
    curl -u<username>:<your_api_key> https://navify.jfrog.io/navify/api/npm/auth
    ```

    **where username is as in previous paragraph - your email id WITHOUT the domain @roche.com.**
    [![login](//s3.amazonaws.com/user-content.stoplight.io/18223/1568709624330)]((//s3.amazonaws.com/user-content.stoplight.io/18223/1568709624330))

3. If you are working with scoped package such as `"@dia"`, generate the basic authentication with the command given and append the output to ~/.npmrc

    ```bash
    curl -u<username>:<your_api_key> https://navify.jfrog.io/navify/api/npm/navify-npm-local/auth/<scope>
    ```

    [![login](//s3.amazonaws.com/user-content.stoplight.io/18223/1568709624330)](//s3.amazonaws.com/user-content.stoplight.io/18223/1568709624330)

4. The end result of your `~/.npmrc` should look like the screenshot below.

    [![login](//s3.amazonaws.com/user-content.stoplight.io/18223/1568709642799)](//s3.amazonaws.com/user-content.stoplight.io/18223/1568709642799)

## Retention policy

| Artifact Types | Snapshot | Release |
| --- | --- | --- |
| Common Libraries (Java) | Last 3 unique snapshots | Last 6 releases |
| Microservice artifacts | Last 3 unique snapshots | Last 2 releases (to be reviewed. Need to align with PDP and GxP audit process) |
| DIA NPM packages | Last 3 unique snapshots | Last 6 releases |

## Support

Submit a [https://jira.intranet.roche.com/jira/browse/DISDEVOPS](https://jira.intranet.roche.com/jira/browse/DISDEVOPS) ticket if you have any question, issue or if you need to add new external library/package into DISDEVOPS managed Artifactory ([https://navify.jfrog.io/](https://navify.jfrog.io/))

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>