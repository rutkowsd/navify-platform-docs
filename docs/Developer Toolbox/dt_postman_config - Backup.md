# Postman env. configuration and collections

This page provides the minimal information that you will need to begin working with the NAVIFY API endpoints using Postman.

Read more ... [Working with Postman](https://learning.getpostman.com/docs/postman/launching-postman/installation-and-updates/)

### Configure Postman environment

1. Copy and past the below json into a new json file and save it localy
2. Open Postman and click on the `Manage Environments` gear
3. Select the import button
4. Select the json file you created in step 1.
5. You are ready to go...

The configuration necessary for you to connect with Postman to our appDev environment  
[See also Platform Environment list](https://docs.platform.navify.com/guides/getting-started/platform-environments)

``` json
{
	"id": "e9b009a0-96df-4e7c-b4f6-09ecad410070",
	"name": "appDev",
	"values": [
		{
			"key": "PLATFORM_TOKEN",
			"value": "",
			"enabled": true
		},
		{
			"key": "AUTHORIZATION_TOKEN",
			"value": "",
			"enabled": true
		},
		{
			"key": "X-Navify-Tenant",
			"value": "",
			"enabled": true
		},
		{
			"key": "APP-REGISTRY-BASE-URL",
			"value": "https://8v48ljxv81.execute-api.us-west-2.amazonaws.com/test/public/platform/api/v1/app-registry",
			"enabled": true
		},
		{
			"key": "TENANT_SERVICE_BASE_URL",
			"value": "https://8v48ljxv81.execute-api.us-west-2.amazonaws.com/test/public/platform/api/v1",
			"enabled": true
		},
		{
			"key": "ROLE_SERVICE_BASE_URL",
			"value": "https://8v48ljxv81.execute-api.us-west-2.amazonaws.com/test/public/platform/api/v1",
			"enabled": true
		},
		{
			"key": "",
			"value": "",
			"enabled": true
		}
	],
	"_postman_variable_scope": "environment",
	"_postman_exported_at": "2019-11-06T15:29:01.438Z",
	"_postman_exported_using": "Postman/7.10.0"
}

```

## Postman collections

You can find all our endpoint swagger files for download on the API References page. See the list below.

| Resource | Swagger Download page |
| --- | --- |
| Application Registry | [Download](https://docs.platform.navify.com/api-references/app-registry)|
| Tenant Subscription | [Download](https://docs.platform.navify.com/api-references/tenant-app-mngt)|
| Tenant Management | [Download](https://docs.platform.navify.com/api-references/tenant-mngt)|
| User Management | [Download](https://docs.platform.navify.com/api-references/tenant-user-mngt) |
| Role Management | [Download](https://docs.platform.navify.com/api-references/role-mngt) |

### Creating a collection from a swagger

1. Click on the 'Import' button in the top left corner of Postman UI.
2. You will see multiple options to import the API doc. Click on the 'Choose Files'.
3. Find the downloaded swagger file and click import.
4. Select the "Generate a Postman Collection" checkbox and click "Next".
5. You will see all your APIs as 'Postman Collection' and can use it from the Postman.

## Read more

## Comments

Discuss the content of this page on our [NAVIFY Platform Community](https://community.platform.navify.com)