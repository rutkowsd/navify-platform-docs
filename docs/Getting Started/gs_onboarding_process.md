# Product Onboarding Process

[![Onboarding process overview](https://s3.amazonaws.com/user-content.stoplight.io/18223/1566996425521)](https://s3.amazonaws.com/user-content.stoplight.io/18223/1566996425521)  
*(Click image to enlarge)*

## Read more

* [How to deploy a product on the NAVIFY platform](https://docs.platform.navify.com/guides/ac-guides/howto-deploy-app-on-platform)

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>