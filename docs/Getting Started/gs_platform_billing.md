# AWS Billing and Costs

> <p style="color:#0066cc; font-weight:bold">Note:</p>
> <p style="color:#0066cc;">This article looks only at the AWS Billing and Costs for products. You are responsible for the cost of the AWS services used while running your products from Development to Production.</p><p style="color:#0066cc;"> For cost estimates, see the pricing pages for each AWS service you will be using. AWS restricts the right to change Prices.</p>

## What you should know

The NAVIFY Platform will chargeback products on any AWS usage to the cost center provided during account creation. 2019 AWS costs will be entirely charged back before year-end, December 2019 charges will be based on November actuals and charged back also within this calendar year. Depending on the amount consumed by the product teams, the chargeback will happen either monthly or in one single payment in December based on discussions with local financial controllers.

Additional information about platform billing can be read on our information site [Platform Billing](https://info.platform.navify.com/navify-platform/platform-billing-model/).

## AWS Biling Console

Each account that the platform provisions has access to the AWS Billing Console.

The AWS documentation is the most up to date source on what is available in the Billing Console. It also provides infomation on your usage and billing reports.

See some AWS documentation here:

* [Viewing Your Bill](https://docs.aws.amazon.com/en_pv/awsaccountbilling/latest/aboutv2/getting-viewing-bill.html)
* [AWS Cost and Usage Report](https://docs.aws.amazon.com/en_pv/awsaccountbilling/latest/aboutv2/billing-reports-costusage.html)
* [Monitoring Your Usage and Costs](https://docs.aws.amazon.com/en_pv/awsaccountbilling/latest/aboutv2/monitoring-costs.html)

AWS also produces a complete whitepaper around biling:

* [WHITEPAPER: How AWS pricing works](https://d1.awsstatic.com/whitepapers/aws_pricing_overview.pdf?did=wp_card&trk=wp_card)

## How to view your AWS costs

The platform team currently assigns the `[product]-Devops` role with a policy to view your account billing. So when you switch to your product `[product]-Devops` role you will be able to select the Billing Dashboard from the menu.

You can find your billing console by clicking on your user name in the right hand corner and selecting My Billing Dashboard from the menu.

### Getting Started with AWS Billing & Cost Management

* Manage your costs and usage using [AWS Budgets](https://console.aws.amazon.com/billing/home#/budgets)
* Visualize your cost drivers and usage trends via [Cost Explorer](https://console.aws.amazon.com/billing/home#/costexplorer)
* Dive deeper into your costs using the [Cost and Usage Reports](https://console.aws.amazon.com/billing/home#/reports) with [Athena integration](https://docs.aws.amazon.com/awsaccountbilling/latest/aboutv2/athena.html)
* Learn more: Check out the [AWS What’s New webpage](https://aws.amazon.com/new/#cost-management)

### Do you have Reserved Instances (RIs)

* Access the RI Utilization & Coverage reports and RI purchase recommendations—via [Cost Explorer](https://console.aws.amazon.com/billing/home#/costexplorer).

![BillingDashboard](http://s3.amazonaws.com/user-content.stoplight.io/18223/1571132693373)

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
