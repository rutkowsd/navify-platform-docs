# Being on the platform

NAVIFY Platform is designed to host your decision support products in the cloud (AWS to be specific).

The platform provides application developers and DevOps practitioners features that will accelerate their development, reduce the complexities and uncertainties around cloud adoption, and provide some opinionated configurations and best practices to help you start your cloud journey on the right foot.

The NAVIFY® Platform allows for products to adopt a phased approach to consuming the full offering of the Platform. 

The platform offers two levels of adoption:

| Adoption Level Name | Usage |
| --- | --- |
| Platform Secure Cloud Infrastructure | Mandatory |
| Platform Access Control | Recommended |

All products that want to benefit from the platform start with the Platform Secure Cloud Infrastructure adoption level. It provides a secure and monitored AWS account under the NAVIFY Platform AWS organization. As a second step we recommend products to adopt the Platform Access Control services which provide a common customizable sign-in page and Identity and Access Management (IAM) services.

## Platform Secure Cloud Infrastructure

![Adoption Level 1 - Platform Secure Cloud Infrastructure](https://s3.amazonaws.com/user-content.stoplight.io/18223/1562584891916 "Adoption Level 1 - Platform Secure Cloud Infrastructure")

The **Platform Secure Cloud Infrastructure** offering of the platform is meant to provide you hosting, protect your platform account and support you in keeping your account and application secure and protected.

- We provide an AWS account under the NAVIFY Platform [AWS organization](https://aws.amazon.com/organizations/).
- We setup [Amazon GuardDuty](https://aws.amazon.com/guardduty/) for threat detection on your account
- We setup [Amazon Inspector](https://aws.amazon.com/inspector/) for security assessments
- We setup [VPC flow logs](https://docs.aws.amazon.com/vpc/latest/userguide/flow-logs.html) that will capture IP traffic going to and from network interfaces in your VPC.
- We setup [AWS Cloudtrail](https://aws.amazon.com/cloudtrail/) in your account to capture and monitor your account
- We provide a hardened AMI that you can safely use without the need to pay for such an offering on the market
- Finally, we give you access to Sumo Logic (central log aggregation) and Dome9 (compliance dashboards) for your account

> Read more about the Security offerings in the chapter Platform Cloud Secure Infrastructure > Security.

## Platform Access Control

![Adoption Level 2 - Platform Access Control](https:///s3.amazonaws.com/user-content.stoplight.io/18223/1562585688414 "Adoption Level 1 - Platform Access Control")

The **Platform Access Control** offering allows you to utilize our Platform Login SSO to have a unified concept of user identity across all apps. You can easily make your app a part of the larger NAVIFY ecosystem. Don’t worry about setting up a whole authentication system, just verify our JWT! The **Platform Access Control** provides all you need for Identity & Access Management: tenant, user, and role management.

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>