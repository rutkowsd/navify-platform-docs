# Platform Environments

The NAVIFY Platform has multiple environments allowing products to move through their product development lifecycle in a managed and secure way.

| Environment Name (AWS Account Name)| OKTA URL | Front Door Domain | List of Regions | General Usage & Comments |
| --- | --- | --- | --- | --- | --- | --- | --- |
| appDev | https://rocherapid-test.okta.com | https://us.api.appdev.platform.navify.com | US-only | Dev environment for onboarding applications |
| appQA (US) | https://npqa-navify.okta.com | https://us.api.navifyappqa.platform.navify.com | US & EU | QA environment for onboarding applications |
| appQA (EU) | https://navifyplatform-appqa-eu.okta-emea.com | https://eu.api.navifyappqaeu.platform.navify.com | US & EU | QA environment for onboarding applications |
| appStage (US) | https://navifyplatform-appstage-us.okta.com | https://us.api.navifyappstageus.platform.navify.com | US & EU | Staging and/or V&V environment for onboarding applications |
| appStage (HK) | https://navifyplatform-appstage-hk.okta.com | https://hk.api.navifyappstagehk.platform.navify.com | HK | Staging and/or V&V environment for onboarding applications |
| appStage (KR) | https://navifyplatform-appstage-kr.okta.com | https://dev.api.navifyappstagekr.platform.navify.com | KR | Staging and/or V&V environment for onboarding applications |
| appPerf | https://npappperf-navify-admin.okta.com/admin/groups | https://dev.api.npappperf.platform.navify.com | | Performance environment for onboarding applications |
| appProd | | | EU, US, UK, JP, KR, SIN, HK | |

## Read more

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>