# Architectural Principles

## Native Cloud Application

The NAVIFY Platform  is a cloud-hosted environment providing

- Cost-efficiency
- Scalability of storage and compute
- Simplified release management
- Centralized operations and maintenance supporting high availability
- Common environment for data aggregation and analytics

## Microservices

Microservices are the architectural framework for creating highly scalable and reliable, SaaS-based products. A microservices-based architecture separates key features and resources into loosely coupled and independently scalable services. Advantages of a microservices architecture include:

- Capabilities can be developed, deployed, and scaled independently of other services
- New services can add functionality to products on the platform like building blocks
- Engineering teams can focus on functional components and minimize system complexity
- Improved fault isolation
- Can build using best suited technology stacks

## Data Compartmentalization

To maintain microservice independence and to isolate sensitive data, each microservice will have its own database for persistent data needed by that service.

- **PHI should be stored in the minimum number of microservices**

## API Gateways

API gateways encapsulate the services contained within layers of both the platform and products. An API gateway

- Interfaces between sets services and their functionality are clearly defined
- Insulates clients from how the platform--and products--are partitioned into microservices
- Simplifies coordination of multiple services

## Multitenancy

A single instance of software hosted in the cloud can support many application customers. The alternative solution would be installing on-premises software or creating separate cloud environments for each customer.

Multitenancy capability

- Simplifies deployment and maintenance
- Maximizes efficient use of cloud resources
- Increases complexity of security and customization.

Read more ... [Multi-tenancy](https://docs.platform.navify.com/guides/miscellaneous/multi-tenancy)

## Platform Architecure

The diagram below represents the NAVIFY Platform architecture

[![Diagram 4](https://s3.amazonaws.com/user-content.stoplight.io/18223/1554989938014)](https://s3.amazonaws.com/user-content.stoplight.io/18223/1554989938014)

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
