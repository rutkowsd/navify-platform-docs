# Multitenancy

Multitenancy is one of the core architectural principles of the NAVIFY Platform, this is why we feel like it's worthwhile to refresh your memory on this. This document is intended to give readers an idea about the multitenancy concept for SaaS applications.

>_Disclaimer_: Each application makes their own decision on their SaaS Architecture. Meaning you decide if you want to build a multi-tenant SaaS or not.

It’s important to understand how multi-tenancy influences an approach to building a robust and efficient identity and isolation model.

In traditional, single-tenant environments, identity tends to have a narrower scope where most of the focus is on managing a user’s credentials and personally identifiable information (PII). These single-tenant solutions typically depend on identity brokers and providers to implement their application’s authentication and authorization model. Meanwhile, in multi-tenant environments, users require additional identity data to accurately resolve the tenant context for a user.

The following diagram provides a conceptual mapping of these concepts. You’ll notice the traits that clearly distinguish user and tenant identities. We use the term SaaS Identity that joins the two concepts enabling the components of your system to have full access to tenant and user context.

![SaaS Identity](https://s3.amazonaws.com/user-content.stoplight.io/18223/1572973372322)

In addition to thinking about how identity context is conveyed throughout your SaaS system, your solution must also consider how that identity will be used to constrain access to resources.
Multi-tenant systems often leverage shared resources as part of their architecture, and, to eliminate any cross-tenant access to these resources, you’ll need to associate policies and roles with each user to scope their view of the system. The following diagram highlights these concepts. Two tenants have access to a series of AWS resources. With security policies, each tenant should be allowed to access only their own resources.

![Isolation](https://s3.amazonaws.com/user-content.stoplight.io/18223/1572973381307)

The key point here is that identity alone does not address the full security requirements of SaaS environments. While being authenticated certainly represents one barrier to entry, you’ll still want to rely on an added layer of policies that ensure that a given tenant never has the opportunity to access another tenant’s resources.

Any discussion of SaaS Identity must also touch on roles. In SaaS environments, you have an extra set of dimensions you must consider when thinking about the roles that are associated with a user. For example, users may be system users or tenant users. A system user is an administrator of a SaaS provider and has access to all tenant data, whereas a tenant user is constrained to managing configuration and data that is part of their environment

The next figure provides a conceptual view of these system and tenant roles. You can see that there are individual tenant administrators who manage their tenant users (each of whom could have their own distinct roles). There are also system roles, which are used to provision additional types of system users (operations, support, and so on). Users in the system role, for example, will have access to all the tenants who are part of the system. While the exact nature of these roles may change from system to system, your identity provisioning and isolation approach must accommodate the varying needs of these different user roles.

![User Roles](https://s3.amazonaws.com/user-content.stoplight.io/18223/1572973390853)

## Applying Roles

SaaS adds some complexity to this model, requiring roles that must accommodate the varying needs of both system and tenant users.

### System Users

A system user is a user who requires access to all tenants. These users represent the ISVs who provide varying types of management, monitoring, and support access to their SaaS tenants. In this category, you will typically find system administrators, operations personnel, and customer support staff with varying levels of access.

#### How does this look from the Platform perspective?

In the context of the NAVIFY Platform, we currently do not provide a support user for applications to manage their Tenants. This is a product application decision, on how they manage their Tenant and User information implement it in their solutions.  

### Tenant Users

A tenant user is a user who is scoped to a specific tenant. These users represent the individual tenant roles that are used to manage tenant configuration and assume the various domain-specific roles of a given application. The initial tenant user, often referred to as the owner, is created during the onboarding process. All subsequent users are then created by this account.

In the context of the NAVIFY Platform, such users are the Tenant Administrator and Tenant User

#### Tenant Administrator

This role has full access to the tenant environment, including the ability to introduce new tenant users into the system. May have access to application functionality depending on the roles assigned.

#### Tenant User

This role represents an application-level user with more constrained access to tenant data and functionality. This user has no ability to manage users and has access to application functionality as defined by their assigned roles.

## Read more

* [Tenant Management Concept](https://docs.platform.navify.com/guides/platform-access-control/tenant-management)
* [Roles Management Concept](https://docs.platform.navify.com/guides/platform-access-control/role-management)
* [User Management Concept](https://docs.platform.navify.com/guides/platform-access-control/user-management)

## References

* [https://aws-quickstart.s3.amazonaws.com/saas-identity-cognito/doc/saas-identity-and-isolation-with-cognito-on-the-aws-cloud.pdf](https://aws-quickstart.s3.amazonaws.com/saas-identity-cognito/doc/saas-identity-and-isolation-with-cognito-on-the-aws-cloud.pdf)
* [https://www.researchgate.net/publication/264942141_Architectural_Concerns_in_Multi-Tenant_SaaS_Applications](https://www.researchgate.net/publication/264942141_Architectural_Concerns_in_Multi-Tenant_SaaS_Applications)

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
