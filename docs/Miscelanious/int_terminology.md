# Terminology

Find below a list of terms commonly used by platform documentation

| Term | Definition |
|---|---|
|**Account Provisioning**|The process of requesting and obtaining access to the Platform Secure Cloud Infrastructure services to onboard the product on NAVIFY.|
|**Adoption level**| The NAVIFY® Platform allows for products to adopt a phased approach to consuming the full offering of the Platform. The platform offers two levels of adoption: Platform Secure Cloud Infrastructure (mandatory) and Platform Access control (recommended).|
|**Application Registry**| The service responsible for maintaining the list of applications, their details and configuration on the platform. The service provides endpoints that applications use to self-configure.|
|**Bastion Host**| Including bastion hosts in your VPC environment enables you to securely connect to your Linux instances without exposing your environment to the Internet. After you set up your bastion hosts, you can access the other instances in your VPC through Secure Shell (SSH) connections on Linux. Bastion hosts are also configured with security groups to provide fine-grained ingress control.|
|**Platform Access Control**| Platform access control refers to the second adoption level of the NAVIFY Platform. This adoption level provides a common sign-in page and Identity and Access Management (IAM) microservice to products hosted on the NAVIFY Platform.This adoption level is recommended.|
| **Developer Tenant**| A development organization that uses the platform to develop products. Dev tenants have access rights to deploy/modify a set of applications. Dev tenant are limited to an organization Zone.|
| **Identity Provider (IdP)**| An identity provider is a system entity that creates, maintains, and manages identity information for principals while providing authentication services to relying applications within a federation or distributed network. Identity providers offer user authentication as a service. The NAVIFY platform uses OKTA as the IdP. Okta is integrated into the Platform Access Control services.|
|**NAVIFY ecosystem**| NAVIFY ecosystem refers to all the products on the NAVIFY platform, the NAVIFY microservices, and tools interacting as a system|
|**NAVIFY Platform Genius**| The NAVIFY Platform Genius is your point of contact for onboarding your product on the NAVIFY Platform.Some of the Genius responsibilities are:<br /> - Understanding the inner working of your product.<br />- Triggering the provisioning of your AWS account.<br />- Triggering the provisioning of your AWS account.<br />- Providing you with your account details.|
|**Platform Authorization Service**| The platform authorization service creates the platform backend tokens that the applications use to authorize access to backend services.|
|**Platform Public API Gateway(PAG)**| The common front door through which products can access the platform microservices.  PAG provides a centralised authorization and authentication.|
|**Platform Services**| It refers to the four service types that the platform provides:<br />- Platform Secure Cloud Infrastructure.<br />- Platform Access Control Microservices for IAM management.<br />- DevOps tools and services for logging and monitoring.<br />-Developer Experience - DevX documentation and community.|
|**PlatformAdmin role**| The role allocated to DevSecOps in the NAVIFY team.|
|**Product API Gateway**| The product gateway that forwards external requests to the relevant application.|
|**Platform Secure Cloud Infrastructure Services**| Platform Secure Cloud Infrastructure refers to adoption level 1 where the product is onboarded and given access to a set of security tools together with the AWS account.|
|**Security Champion**| The single point of contact for security of a product. This person has expert security knowledge, exposure to our security tools, and a really good understanding of their product.|
|**Tenant**| Customer account on the platform. The account  contains:<br />- MetadataRegistry of customer's users<br />- Registry of  all applications that the customer has subscribed to.<br />- User’s roles<br />- Roles membership of the users.|
|**Tenant Administrator**| The Administrator is the person in an organisation that manages users on the  tenant and performs housekeeping tasks.|
|**TenantAdmin Role**| The role allocated to tenant administrators in production. The role can also be allocated to developers during the testing stage.|
|**Zone**| An abstraction layer that isolates applications and services deployed into a single zone from other zones. A platform service is available to everyone if deployed on the platform.<br />OR<br />A platform service is only available to members of the  zone, if deployed into a specified zone.<br />Example  All data pipelines and data lake could be deployed into the DIS zone and be available to DIS applications|

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
