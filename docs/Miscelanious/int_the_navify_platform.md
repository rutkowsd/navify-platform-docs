# The NAVIFY Platform

Before you **Get Started** with the NAVIFY Platform take a minute to read through "What is the NAVIFY Platform?".

## What is the NAVIFY Platform?

The NAVIFY Platform provides common, reusable services and capabilities that help build and develop SaaS-based decision support products.

## Purpose of this document

This document provides an architectural overview of the Platform to help engineers design, build, and implement within the NAVIFY ecosystem.

## Platform Overview

The NAVIFY Platform began as a result of the digital transformation of the Diagnostics business. [TheDiagnostics Divisional Common Platform Guidance](https://docs.google.com/presentation/d/1NgrpNmRb1SnsLPJkLKcwn1wB_Ybvv6zE34FnoEMaqR4) provides the starting point for building the platform. The NAVIFY Platform provides the foundation for an ecosystem of decision support applications, including Data Aggregation and Medical Algorithms.

![Diagram 1](https://s3.amazonaws.com/user-content.stoplight.io/18223/1554989234096 "Diagram 1")

The NAVIFY Platform uses an application-first strategy, in which platform capabilities that are needed by multiple products will be **“harvested”** and **co-developed** as a common, reusable platform capability.

Capabilities can be functional--such as authentication--or operational, like security. Functionality is first assessed against each individual products' requirements. If a common set of requirements can be derived, architectural design can be made satisfying the common set of requirements. Software engineering expertise to develop each capability can reside within the individual product teams, within dedicated platform teams, or a combination.

The conceptual architecture of the platform is summarized in the **UTI** diagram consisting of three levels of capabilities:

1. The **U**: foundational *(non-differentiating)* services needed by all SaaS decision support products,
2. The **T**: non-foundationl *(differentiating)* services developed by one or more products (e.g. terminology service), and
3. The **I**: operational services and engineering ecosystem 

The NAVIFY Platform is defined by the **U** and **I** levels, plus certain **T** capabilities that emerge as re-usable by more than one product (see diagram 2 & 3 which depicts the UTI diagram).

![Diagram 2 - UTI Model](https://s3.amazonaws.com/user-content.stoplight.io/18223/1554989755844 "Diagram 2 - UTI Model")

The next diagram shoes a detailed capabilities overview of a proposed future state for the NAVIFY Platform.

![Diagram 3 - UTI Model Details](https:////s3.amazonaws.com/user-content.stoplight.io/18223/1554989782023 "Diagram 3 - UTI Model Details")

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
