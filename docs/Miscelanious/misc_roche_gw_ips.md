# Roche Internet Access NAT Addresses

## Background

Certain external resources (libraries, ejournals, databases) are “protected” by source IP address. This means that Roche companies can only access these resources if our IP addresses are in their “allow” lists. The list below can be used to identify Roche IP for specific location.

### Why it can be interesting for you?

You can use this information to configure your security groups in AWS. 

| Addres Range  |  Source IP Address | Comments  |
| --- | --- | --- |
| 194.120.84.0 - 194.120.84.31 | 194.120.84.9| Mannheim (Traffic balancing) |
| 194.120.88.0 - 194.120.88.255 | 194.120.88.1 | Mannheim, Meylan, Germany: Penzberg, Stuttgart, Düsseldorf, Berlin, Lampertheim, Edingen, Sulzbach, Bernried, Neuried, France: Meylan, Strasbourg (Dia, Pharma), Austria: Graz, Iceland: Reykjavik,Netherland: Almere, Norway: Oslo, UK: Burgess Hill, New Haven, Turkey: Istanbul, Ankara, Izmir,Ikitelli, Sweden: Bromma, Spain: Sant Cugat, Madrid, Tenerife, Badalona, South Africa: Randburg | 
| 196.3.50.240 - 196.3.50.255 <br><br> 198.21.21.0 - 198.21.21.127 | 196.3.50.241 - 196.3.50.254 <br><br> 198.21.21.21| Basel, Grenzach, Madrid, Welwyn, Kulmbach, Zürich, Bucharest, Belgrad, Budapest,Ljubljana, Sofia, Athen, Istanbul |
| 198.21.31.0 - 198.21.31.255 | 198.21.31.6 <br> 198.21.31.129| Illovo |
| 196.3.37.240 - 196.3.37.255 <br><br> 196.3.43.240 - 196.3.43.255 | 196.3.37.241 <br> 196.3.37.251 <br> 196.3.43.251 | Sao Paulo (Brazil) |
| 196.3.38.240 - 196.3.38.255 | 196.3.38.241 <br> 196.3.38.244 | Argentina (Buenos Aires) |
| 200.32.127.60 - 200.32.127.63 | 200.32.127.62 | Argentina |
| 196.3.54.240 - 196.3.54.247 | 196.3.54.241 <br> 196.3.54.247 | Mexico, Costa Rica, Colombia, Ecuador, Guatemala |
| 206.53.226.0 - 206.53.226.255 | 206.53.226.249 <br> 206.53.226.250 <br> 206.53.226.251 | Indianapolis |
| 206.53.227.0 - 206.53.227.255 | ---- | Indianapolis |
|198.21.24.0 - 198.21.25.255 | 198.21.24.249 <br> 198.21.24.250 <br> 198.21.24.251 | Indianapolis |
| 196.3.49.0 - 196.3.49.255 | 196.3.49.1 <br> 196.3.49.254 | Singapore, APAC (Asiapacific) except: China (see Shanghai), Japan (see Tokyo), New Zealand and Australia (see Deewhy) |
| 196.3.56.0 - 196.3.56.255 | 196.3.56.100 <br> 196.3.56.254 | Dee Why, Castle Hill and New Zealand |
| 211.144.221.0 - 211.144.221.7 | 211.144.221.1 | Shanghai |
| 196.3.44.0 - 196.3.44.255 | 196.3.44.1 | Shanghai |
| 198.21.16.240 - 198.21.16.255 | 198.21.16.100 <br> 198.21.16.254 | Tokyo |
| | 72.34.128.250 | Genentech (Santa Clara), Roche: Pleasanton, Genia, Bina, Ventana (Tucson) |
| | 72.34.128.251 | Genentech (Santa Clara), Roche: Pleasanton, Genia, Bina |
| | 72.34.128.252 | Genentech (Santa Clara), Roche: Pleasanton, Genia, Bina |
| | 72.34.128.249 | Genentech (Santa Clara DMZ traffic to Internet) |
| | 72.34.133.250 | Genentech (Vacaville), Roche: Pleasanton |
| | 72.34.133.251 | Genentech (Legacy Vacaville) moved to Santa Clara |
| | 72.34.133.252 | Genentech (Legacy Vacaville) moved to Santa Clara |
| | 203.135.189.250 | Genentech (Singapore) |
| | 72.34.133.249 | Genentech Mac VPN |
| | 212.103.9.93 | Ventana (France) |
| | 221.245.165.98 | Ventana (Japan) |
| | 203.219.78.158 | Ventana (Australia) |
| 192.12.78.0 - 192.12.78.255 | ---- | Genentech (Santa Clara), Ventana (Tucson) |
| | 192.12.78.250 | Genentech (Santa Clara), (Aspera to HLI) |
| | 47.19.91.178 | Chugai Pharmaceuticals (USA) |
| 194.203.158.0 - 194.203.158.255 | | Chugai Pharmaceuticals Europe (UK) |
| | 81.25.193.102 | Chugai (France) |
| 210.172.233.0 - 210.172.233.63 | 210.172.233.7 | Chugai, Japan (ab 1.1.2004) |
| | 80.146.176.100 | Chugai (Frankfurt) |
| | 113.196.173.150 | Chugai Pharma (Taiwan) |
| | 116.12.185.241 | Singapore “Chugai Pharmabody Research Pte. Ltd"  Fortigate VPN Gateway from Internet |

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
