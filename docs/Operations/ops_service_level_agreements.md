# Service Level Agreements

## Time Zone Coverage

| Location | Time Coverage | Public Holidays |
| --- | --- | --- |
| Belmont / Santa Clara (PST) | Monday - Friday<br />08:00am-06:00pm | N/A |
| Kaiseraugst/ Basel (CET) | Monday - Friday<br />08:00am-06:00pm | N/A |

## Event Management

### Event Response tracking and Metrics

The NAVIFY Platform uses VictorOps to centralize, track and measure the events. The following metrics are used to measure the event response time:

| Metrics | Definition |
| --- | --- |
| **Mean time to acknowledge (MTTA)** | this is the amount of time it takes for an on-call engineer to access the alert that was triggered and turn off the alarm. An on-call engineer does this by acknowledging the alert in the VictorOps UI (with the assumption that that engineer is taking action towards resolving it). |
| **Mean time to resolve (MTTR)** | this is the amount of time it takes for the engineering team to resolve an issue resulting in an alert (e.g. “close the issue”). This is done by changing the status of the alarm from acknowledged to resolved after the necessary prerequisite work has been done to acceptably resolve the issue. Tagging and categorization can be done to add metadata to the event or incident. |

### SLA

| Event/Incident Priority | Event/Incident Impact/Description | Initial Contact | Target Response | Expected Customer Response Time |
| --- | --- | --- | --- | --- |
| **Urgent** (Priority 1) | Critical Fault (no workaround)– Entire Platform (a Product) has halted. The Product is unavailable or unusable. No further execution possible and there are no workarounds from Product Eng team. Security vulnerability detected with a high risk, high probability and high impact. | **15 minutes**<br />(primary pinged every 5m until ack’d, if unanswered, primary and secondary pinged after 10m, every 5m until ack’d) | 24 Hours | 15 Minutes |
| **High** (Priority 2) | Major Fault (with workaround) - A portion of the platform has halted, unavailable or unusable. The problem is isolated to a particular function or impacts a single product. Security vulnerability detected with a moderate risk, moderate probability and moderate impact | 4 Hours<br />(primary pinged every 5m until ack’d, if unanswered, primary and secondary pinged after 1hr, every 15m until ack’d) | 2 Days | 4 Hours |
| **Normal** (Priority 3) | Minor Fault - Feature not working as documented – fault for which there is a reasonable workaround. The fault does not affect the Product in a hazardous way | 2 Days<br />(primary pinged every 5m until ack’d, if unanswered, primary and secondary pinged after 1hr, every 15m until ack’d) | 10 Days | 2 Days |

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>