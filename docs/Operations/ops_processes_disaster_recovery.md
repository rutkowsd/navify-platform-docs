# Disaster Recovery Management

## Definition

|  | Disaster | High Severity |
| --- | --- | --- |
| Definition | An incident cannot be resolved by working with the system in its current state, or data has been permanently lost. It requires creation of a temporary environment to return business operations to an acceptable level. | Platform-wide unavailability; lack of engineer workarounds; SLA impacted. Issue identified with high risk, impact, and probability OR any ongoing cyber attack. |
| Response | Leverage data backups to return the NAVIFY system to a previous, functional state. | All appropriate resources are dedicated to restore service(s). |
| Relationship | A situation that requires Disaster Recovery is a High Severity event. | Only SOME High Severity events will require Disaster Recovery Response. |

## Roles & Responsibilities

Disaster Recovery Management introduces new roles in addition to the ones already defined in the [Roles & Responsibilities page](https://docs.platform.navify.com/guides/operations/roles-responsibilities).

| Role | Description |
| --- | --- |
| DR Assessment and Response Team | Led by the Operations lead, this team is assembled with the singular purpose to investigate the overall impact to the system. The deliverable from this team will guide the DRC in determining if the event is a full-on disaster requiring DR protocols. |
| Disaster Recovery Chief (DRC) | The DRC has decision-making responsibility and ultimate accountability for the disaster and returning the business to full operation. By default, it is the chief’s responsibility to assign and confirm all other roles are delegated appropriately. |
| Legal Support | Contact in legal department to provide information around SLA infractions (service provider or customer) or data breach issues. |

## Process Flow

![Disaster Recovery Management Process Flow](https://s3.amazonaws.com/user-content.stoplight.io/19596/1571832043119)

## RACI Matrix

| Process Step | Incident Commander | Disaster Recovery Chief | Navify Support Team | Product Support Team | Legal Team |
| :---: | :---: | :---: | :---: | :---: | :---: |
| Declare Disaster Recovery Situation, name Disaster Recovery Chief | AR | | C | I | I |
| Build Action Plan for Disaster Recovery; Execute on Plan | A | R | R | I | I |
| Post Disaster Recovery, monitor health of system and perform infrastructure testing (e.g. requalification) | | A | R | | |

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
