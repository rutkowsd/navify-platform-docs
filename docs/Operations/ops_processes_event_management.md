# Event Management

The following page describes how the NAVIFY Platform Operation Team handle events that occur on the Platform.

## Definition

Event Management process is the process that guide the resolution of platform service request, event or cybersecurity incident. We use JIRA Service Desk as the single source of truth to track event management process.

## Roles & Responsibilities

See [Roles & Responsibilities page](https://docs.platform.navify.com/guides/operations/roles-responsibilities)

## Process Flow

![Event Management Process Flow](https://s3.amazonaws.com/user-content.stoplight.io/19596/1570707138291)

## RACI Matrix

| Cadence | Process Step | Responsible | Accountable | Consulted | Informed |
| --- | --- | --- | --- | --- | --- |
| Once per Event | **Initial Communication and Event Logging** | On Call Engineer | On Call Engineer | | NAVIFY support team |
| Once per Event | **Determine Event Severity** | Event Commander | Event Commander | |  NAVIFY support team |
| Hourly during Event | **Communications - Medium / High Severity Events** | Communications Lead | Event Commander | NAVIFY support team | Product support teams & Legal team (if necessary) |
| Once per Event | **Identifying Cyber Security Incidents** | On Call Engineer (SecOps) | On Call Engineer (SecOps) | | NAVIFY support team |
| Once per Event | **Declaring Emergency Response Level** | Event Commander |NAVIFY Product Owner | NAVIFY support team | |
| Once per Event | **Generalized Resolution Process** | Operations Lead | Event Commander | NAVIFY support team | Product support teams & Legal team (if necessary) |

## Classification, Severity & Priority

Events are classified by the platform on-call support engineers (DevOps & SecOps) as follow:

- **General Outage** event (see Defect Management page to learn more)
- **CyberSecurity** event (see Cybersecurity Incident Response Management page to learn more)

Once classified, on-call engineers assess the severity following the definition above:

| Level | System Outage Issue | Cybersecurity Issue |
| --- | --- | --- |
| **High** | Major service outage, a defect that causes a major functionality problem on a system, or a security vulnerability detected with a high risk, high probability and high impact. Generally no support requests will have high severity. | Issue identified with high risk, impact, and probability OR any ongoing cyber attack |
| **Medium** | An issue causing a service outage, defect, cyber vulnerability, or progress-blocking support request for a product team. | Issue identified with medium risk, impact, and probability |
| **Low** | Minor service outage or defect with a workaround, or minimal impact on system. | Issue identified with low risk, impact, and probability |

Severity is used to classify incidents, but priority is used to queue them.  Severity is more quantitative in nature, while priority is more qualitative in nature.

| Event/Incident Priority | Description |
| --- | --- |
| **Urgent** (Priority 1) | Critical Fault (no workaround)– Entire Platform (a Product) has halted. The Product is unavailable or unusable. No further execution possible and there are no workarounds from Product Eng team. Security vulnerability detected with a high risk, high probability and high impact. |
| **High** (Priority 2) | Major Fault (with workaround) - A portion of the platform has halted, unavailable or unusable. The problem is isolated to a particular function or impacts a single product. Security vulnerability detected with a moderate risk, moderate probability and moderate impact. |
| **Normal** (Priority 3) | Minor Fault - Feature not working as documented – fault for which there is a reasonable workaround. The fault does not affect the Product in a hazardous way. |

## Event Response SLA's

See Service Level Agreement page to see the agreed SLA's.

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
