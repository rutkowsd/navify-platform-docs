# Roles & Responsibilities

In case of event, the following roles are assigned within the platform operation team:

![Event Management Process Roles](https://s3.amazonaws.com/user-content.stoplight.io/19596/1570696638361)

| Role | Description |
| --- | --- |
| On-Call Engineer (OCE) | This individual has the responsibility to handle potential events as they occur on the platform during that person’s assign “on-call” shift. By default, the OCE becomes the Event Commander until the role is delegated. |
| Event Commander (EC) | Has decision-making responsibility and ultimate accountability for outcome of event. They structure the event response task force and assign responsibilities according to need and priority. By default, the commander holds all positions that they have not delegated. |
| Operations Lead | Usually the first role to be delegated – this person leads the effort to investigate and fix the issue. |
| Communications Lead | Responsible for updating stakeholders (both users, external partners, and internal partners) and keeping the log of event activities. Responsible for organizing the post-mortem. |
| Logistics / Planning | Tasked with providing direct support to the teams that are activated. Gaining access to conference rooms, provisioning new system access for Ops team, tracking the bandwidth/exhaustion of team members and ensuring the IC is aware. |

For low-priority platform events, all the roles above may be filled by a single person. But as more resources are required, roles will be delegated.

For some processes additional roles may be describes such as for Cyber Security Incident Management.

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>