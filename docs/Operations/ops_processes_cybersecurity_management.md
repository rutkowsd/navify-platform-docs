# Cyber Security Incident Management

## Definition

Cyber Security Incident Response is a subset of general Event Response (see the Event Management Process). Note, while the term event is used to describe platform outages, support requests, and defect management, for all cybersecurity events, they are referred to as incidents.

## Roles

Cyber Security Incident Management introduces two new roles in addition to the ones already defined in the [Roles & Responsibilities page](https://docs.platform.navify.com/guides/operations/roles-responsibilities).

| Role | Description |
| --- | --- |
| Cyber Security Incident Response Team (CSIRT) | the wider team responding to the incident. While the team is largely composed of SecOps engineers who have intimate knowledge of the platforms' defense capabilities, the CSIRT team may also include specialists in compliance/liability law, communications, and other fields. |
| Legal Support | Contact in legal department to provide information around SLA infractions (service provider or customer) or data breach issues. |

## Process flow

![Cyber Security Incident Management Process Flow](https://s3.amazonaws.com/user-content.stoplight.io/19596/1571829022203)

## RACI Matrix

| Process Step | Operation Lead | Communication Lead | Event Commander | CSIRT | Platform Product Owner | Product Team | Legal |
| :--- | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| Analyze and Investigate | R | | A | C | C | I | I |
| Contain and Eradicate | R | | A | C | | C | |
| Communicate All Clear | | R | A  | I | I | I | I |
| Hold Post Incident Response Process |  | | AR | I |  | C | C |

## Additional resources

The Incident Response Consortium has developed detailed process breakdowns for various types of Compromise, including:

- [Malware](https://www.incidentresponse.com/playbooks/malware-outbreak)
- [Denial of Service](https://www.incidentresponse.com/playbooks/ddos)
- [Virus Outbreak](https://www.incidentresponse.com/playbooks/virus-outbreak)
- [Elevation of Privileges](https://www.incidentresponse.com/playbooks/elevation-of-privilege)
- [Compromised Root Access](https://www.incidentresponse.com/playbooks/root-access)
- [Data Theft](https://www.incidentresponse.com/playbooks/data-theft)

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
