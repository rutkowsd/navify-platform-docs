# Platform Maintenance

## Summary

Please find below a table summarising the upcoming planned maintenance. Detailed information about the planned maintenance can be found further in this document.

### Up-coming Maintenance

No maintenance planned.

### History

| Date | Maintenance |
| --- | --- |
| 12 Oct. 2019<br />13 Oct. 2019 | [jFrog Maintenance](#jFrog) |

In case of question please contact the [NAVIFY Platform Operation team](https://navifypoc.atlassian.net/servicedesk/).

## <a id="jFrog"></a>Detailed information - jFrog Maintenance

### When

- Saturday, October 12 at 00:00pm. - Sunday, October  13 at 1:00am. (SSF/PST)
- Saturday, October 13 at 9:00am. -  Sunday, October  13 at 10:00am. (Basel/CET)

While we expect the work to last about 60 mins, it is possible that we will finish our work early within the downtime, but we are being conservative with the downtime notification

### Description

jFrog Artifactory account migration to Enterprise to increase stability and support. We are migrating the current Artifactory (Cloud Pro X - Shared Server) to (Jfrog Enterprise Cloud X - Dedicated Server)

### What Action do product teams need to take

No action required.

### Impact

#### During Maintenance

AWS EC2 instances using NAVIFY Platform Hardened AMIs, while using a package manager (e.g. apt or yum), will experience intermittent repository access, and should be considered unavailable during the maintenance period (mentioned below).

#### After Maintenance

AWS EC2 instances using NAVIFY Platform Hardened AMIs will have repository access return available, as before.

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>