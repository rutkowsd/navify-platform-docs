# NAVIFY Platform IAM User Management Tool

NAVIFY Platform User Management is a web application to manage AWS IAM users for products leveraging the platform. It provides `Product Owners` (of the specific products leveraging the platform) the ability to create and assign users to their respective products on the NAVIFY Platform.

The goal of the tool is to give you all the speed and flexibility of managing your IAM users without being dependent from the NAVIFY Platform Support team.  

The tool was designed to address a common request from various product teams, we rolled out a first release that allows you to add users. More features such as list and delete users will come soon, stay tuned!

## Access & Login

Application is designed to serve `Product Owners/Managers` (identified during product account requests) to easily create and assign their team members to their platform AWS accounts. Hence, access to the tool is restricted to `Product Owners/Managers` (only).

- Start [NAVIFY Platform IAM User Management](https://iam.devops.platform.navify.com)

Please login with your Roche Google credentials.

> Please note that, for security reason, the tool can only be accessed from the Roche Corporate Network. If not connected to from a Roche location, please use Pulse Secure to have a VPN access to RCN.

![NP IAM User Management Login Screen Snapshot](https://s3.amazonaws.com/user-content.stoplight.io/19596/1579796614288)

## User creation

![INP IAM User Management Add User Snapshot](https://s3.amazonaws.com/user-content.stoplight.io/19596/1579796738888)

### Create new user

Start by providing the Roche email address for your new team member. Product list will give you a list of all the products that you own whereas Group will allow you to select specific roles for your new team member.

After filling in the form and hitting Add, new team member will receive an email with their access details of her/his AWS account including a link for a temporary, one-time password.

### Login with the newly created account

Once the email received, your new team member can sign-in to NAVIFY Org AWS IAM Account using below credentials:

- URL: [https://navify-org-iam.signin.aws.amazon.com/console](https://navify-org-iam.signin.aws.amazon.com/console)
- Username: Submitted email address for new member
- Password: One-Time Password retrieved by new member

Once logged in to NAVIFY Org AWS IAM Account, new member can switch to Product account with the account number and the role assigned while creating.

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>