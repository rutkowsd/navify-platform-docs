# Tool Change Management

## Definition

Tool change request can be triggered through the [NAVIFY Platform Support Center](https://navifypoc.atlassian.net/servicedesk/customer/portal/1/group/6/create/26) and will follow a strict process before any potential release.

## Roles

| Role | Description |
| --- | --- |
| NPDTC Product Owner | Primary Accountable contact for the operation of the NAVIFY toolchain. |
| NPDTC Platform Architect | Main designer / visionary on the operation of NAVIFY toolchain. Important to Consult when proposing tool changes. |
| Tool Lead | Main operational contact for a given tool (e.g. SumoLogic). Usually one Tool Lead each for Platform DevOps and SecOps teams. See the tool’s runbook for contact name. Responsible for main activity in releasing new changes. |
| DevSecOps Team | Should be Consulted about changes to a tool that could have ripple effects to other tools. Should be able to view history of configuration changes in a self-service manner when needed. |
| Product Owners | Main development contact for a given Product (e.g. TumorBoard). Must be Informed when configuration changes are made that have minimal impact on product performance and Consulted when those changes will have significant impact. |

## Process Flow

- Intake
- Assigning Tool Lead
- Design and Security Review
- Approval / Contracting
- Proof of Concept
- Communication Plan
- Documentation
- Implementation
- Requalification

## RACI Matrix

| Process Step | Details | Responsible | Accountable | Consulted | Informed |
| --- | --- | --- | --- | --- | --- |
| Create and log Request for Change | Complete Request Change Form with details about the new tool or version change | Requestor | Requester |  | Platform Owner |
| Name DevOps and SecOps "Tool Lead" | These contacts will have primary responsibility for tool change activities, and will be the primary point of contact for support questions regarding this tool. Hereafter these two resources are referred to collectively as Tool Leads. | Platform Owner | Platform Owner | | Platform Team |
| Review Change Request | Peer review of submitted change request | Tool Leads | Platform Owner | Requestor | |
| Conduct Proof of Concept | Limited test of integrating tool into toolchain. Output will include a set of required integrations, potential security concerns, and Tool Impact Classification | DevOps Tool Lead | Platform Owner | SecOps Tool Lead | Requester |
| Evaluate Proof of Concept | Evaluate PoC based on system impact & risk, data migration/archive requirements & plan. Outputs a set of How To instructions for implementation | Tool Leads | Platform Owner | | Requester |
| Update Tool Registry | Add tool / change version number to registry, update tool runbook as necessary | Tool Lead | Platform Owner | | |
| Implementation | Add / change tool; communicate to end users about new functionality or any service interruption | Tool Lead | Platform Owner | Platform DevOps SecOps Teams | Product Teams |
| Requalification | Rerun system performance tests across the NAVIFY platform to confirm that functionality is unimpacted. | Platform DevOps / SecOps team | Platform Owner |  | |
| Close Change Request | Log all the relevant documenation, close RFC ticket within JIRA. | Tool Leads | Platform Owner | | |

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>