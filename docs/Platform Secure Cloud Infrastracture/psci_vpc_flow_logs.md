# VPC Flow Logs

VPC Flow Logs is a feature enabled to capture information about the IP traffic going to and from network interfaces in your VPC. Since we don't want to repeat AWS documentation follow the link to get a better picture of what VPC Flow Logs are and the benefits they bring (see [AWS Flow Logs](https://docs.aws.amazon.com/vpc/latest/userguide/flow-logs.html)). 

## Why use VPC Flow Logs: 

* Troubleshoot why specific traffic is not reaching an instance, diagnose overly restrictive security group rules. 
* Monitor suspicious traffic 

## VPC flow logs in context of NAVIFY Platform 

* Each network interface in the VPC will be monitored (VPC Flow logs feature)
* VPC Flow logs will be captured in an S3 bucket managed by the Platform SecOps team. From there they will be made available to products via their Sumo Logic dashboards. 
* VPC Flow logs are enabled for VPC created by the platform team, hence we suggest that you use the platform provisioned VPCs (see [Platform provisioned accounts](https://docs.platform.navify.com/guides/platform-secure-cloud-infrastructure/platform_account/np-provisioned-account))

Note: The Platform feature pipeline includes a request to provide monitoring of newly created VPCs (created by different product teams) to enable VPC flow logs automatically for newly created VPCs.

## Scenarios

* What happens when new resources are provisioned?
  * If you launch more instances into your subnet after you've created a flow log for your subnet or VPC, then a new log stream (for CloudWatch Logs) or log file object (for Amazon S3) is created for each new network interface as soon as any network traffic is recorded for that network interface

## Log records

See AWS documentation for [Flow Log Records](https://docs.aws.amazon.com/vpc/latest/userguide/flow-logs.html#flow-log-records)

## Pricing

CloudWatch Logs charges apply when using flow logs, whether you send them to CloudWatch Logs or to Amazon S3. For more information, see Amazon CloudWatch Pricing.

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
