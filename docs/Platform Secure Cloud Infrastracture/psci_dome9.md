# Dome9

Dome9 is a SaaS tool that provides security and protection of cloud infrastructure and resources. This allows users to monitor and quickly identify security issues within the NAVIFY cloud infrastructure to limit vulnerabilities and potential threats.

Dome9 can check cloud services for a variety of security and compliance best practice features including the use of elevated root credentials, open subnets/resources, and improperly configured security settings allowing for the viewing of these concerns in a single dashboard.

\[More coming soon...\]

[Learn more about Dome9](https://www.dome9.com/)

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
