# How to request your platform account

## Request your account

![Diagram - How to request your platform account](https://s3.amazonaws.com/user-content.stoplight.io/18223/1562586138772 "Diagram - How to request your platform account")

The **Platform Genius** is your single point of contact for new product accounts.

To contact a **Platform Genius** and request a new Platform AWS account, your first port of call is the [NAVIFY Platform support portal](https://navifypoc.atlassian.net/servicedesk/customer/portal/1) where you select the **Get Started** form.  

> Platform Geniuses are responsible for approving the account request

Creating an account requires some specific information that we need to collect. The table lists the mandatory information for AWS Account Creation. Ensure that you have this information at hand when contacting the Platform Genius about account creation.

| Mandatory Information | Description |
| --- | --- |
| Product name | This is the name of your product. All your future AWS accounts are linked to this name. |
| Cost center | This is the cost center where your AWS costs will be billed against. It allows the NAVIFY Platform team to charge you back for the incurred costs.|
| Product Security Champion | The nominated single point of contact for your product security. The Security Champion will be the person whose credentials are also configured in the security tools the platform offers. |
| Product Security Champion email | The email of your Product Security Champion. |
| Product Security Champion Proxy | The nominated proxy to the Security Champion will be the person we contact if we cannot reach the Product Security Champion. |
| Product Security Champion Proxy Email | The email of your Product Security Champion Proxy. |
| Product Owner Name | This is the key stakeholder and main point of contact for a product. Necessary to be kept informed also for verifying approval of raised request. |
| Product Owner email address | The email of the product owner. |
| AWS IAM user name | This will be used to generate an IAM user name you will use to log in to the AWS console. The convention for the user name is [first name (dot) last name]. Once you log in you can switch to the provisioned IAM Role in your account.  Read more about IAM user from AWS ([LINK](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_users.html))|
| AWS IAM user email | The email of the user that gets access to the AWS console. |
| Environment type | Select one of the available environment types that you would like to request. Dev QA STG/VnV Prod|

## When You Receive Your Platform Account

When you request the account, the NAVIFY Team starts to provision and secure your account under the NAVIFY Platform organization. Once the provisioning process is complete, the Platform Genius contacts you to provide the account details.

You will receive:

- AWS Account Id: 12-digit number
- Your IAM user name which you will use to login to the AWS console
- Instructions on how to switch to the IAM role created for your requested account We provision the DevOps role on the requested account with administrator access (example role: roche/[product name]/[product_name]-DevOps)

> You can find more information in this AWS documentation:
>
> [Creating a Role to Delegate Permissions to an IAM User](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_create_for-user.html?icmpid=docs_iam_console)
>
> [Switching to a Role (Console)](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_use_switch-role-console.html)
>
> Provisioned roles
> The platform role provisioned for you in your account will be a **DevOps** role (e.g. roche/[product name]/[product_name]-DevOps) with administrative rights.

### DevOps Role Policy

``` json
{
    “Version”: “2012-10-17",
    “Statement”: [
        {
            “Action”: “*”,
            “Resource”: “*”,
            “Effect”: “Allow”
        }
    ]
}
```

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
