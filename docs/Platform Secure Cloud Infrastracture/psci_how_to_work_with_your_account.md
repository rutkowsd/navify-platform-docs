# How to work with your account

AWS offers you different ways to work with your account:

- Human access: using the AWS UI
- Programmatic access: using the AWS CLI and potential other tools like Terraform and so on.

## Human Access

### AWS Management Console

Find below the information on how to log in to your AWS account with the user credentials you received at onboarding process:

| | |
| --- | ---|
| URL | [https://navify-org-iam.signin.aws.amazon.com/console](https://navify-org-iam.signin.aws.amazon.com/console) |
| Username | Should be your roche e-mail address without @roche.com or @contractors.roche.com |
| Password | You should have received it securely from our team (note that the policy in place will require you to change it on a regular basis |

![AWS UI](https://s3.amazonaws.com/user-content.stoplight.io/19596/1571132425406)

### At first login

#### Password Reset

You have to change the password that was provided to you by default at onboarding.

#### First role to endorse

At first login, you have no permissions to perform any actions until you endore the `product-Devops` role.

- `Account:` Account ID just created
- `Role:` roche/<product_name>/<product_name>-Devops
- `Alias:` Anything desired for ease of access

### How to Swith role

To switch role, use the dropdown menu available while cliking on your username located at the top right corner.

![AWS UI Switch Role](https://s3.amazonaws.com/user-content.stoplight.io/19596/1571132632604)

> Note: You can use the following URL to bookmark quick access to given role: `https://signin.aws.amazon.com/switchrole?roleName=roche/productname/productname-Devops&account=yourawsaccountnumber`

> Important Note: role names are case sensitive

### Enabling MFA for accessing more secure environments

Password protected access may be sufficient enough for development environments but it is nore secured enough for QA or Prod environments. Therefore you should enabled MFA for your IAM users.

This process is documented by AWS: [https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_mfa_enable_virtual.html](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_mfa_enable_virtual.html)

## Programmatic Access

### AWS Access Key

A AWS Access Key is required to use the AWS CLI.

You can provision your AWS Access Key from the "My Security Credentials" menu.

### Configure AWS CLI

AWS offers a complete documentation about AWS CLI:

[https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html)

We do not recommend to set up a default profile as you may have the needs to switch between accounts and it can be easy to accidently run a command against the default environment. Instead we recommend you to configure profiles as explained in the AWS documentation:

[https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-role.html](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-role.html)

### Advanced Setup

Eventually, you’re going to be STS-ing across cross-account roles. You will likely have one IAM/SSO account your user and keys live in, and then you with AssumeRole to other accounts. [Assuming an IAM Role in the AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-role.html) will help you understand the process of setting up roles in the `~/.aws/config` file.

### Configure Terraform

One option would be to rely on your AWS profiles but the AWS provider would also allow you to define all possible options to leverage your access key and define the role ARN when defining your AWS provider with the assume_role property.

```json
provider "aws" {
  profile = "smarttesting-appdev"
  version = "~> 2.0"
  region  = "us-west-2"
}
```

## AWS Access Key and MFA

By default AWS do not enforce your MFA when using Access Key. If required, e.g for roles that are processing sensitive data and so on, you can enforce it by updating your IAM Role Trust Relationship:

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::457095867005:root"
      },
      "Action": "sts:AssumeRole",
      "Condition": { "Bool": { "aws:multifactorAuthPresent": true } }
    }
  ]
}
```

![AWS IAM Role Trust Relationship](https://s3.amazonaws.com/user-content.stoplight.io/19596/1571138812351)

At usage you need to update your `~/.aws/profile` to add the ARN provided by your MFA provider.

```bash
[profile example]
role_arn=arn:aws:iam::973177758033:role/roche/xyz/xyz-Devops
source_profile=default
region=us-west-2
mfa_serial=arn:aws:iam::457095867005:mfa/firstname.lastname
```

Once updatd the AWS CLI checks if a session token exists and ask for the MFA code.

### Terraform and MFA

Terraform does not ask you for your MFA code interactively. You ned to first provision temporary credential and pass them to the Terraform provider at each script invoke through variables.

```bash
aws sts assume-role --role-arn arn:aws:iam::973177758033:role/roche/xyz/xyz-Devops --role-session-name terraform-nasp --profile xyz {
    "AssumedRoleUser": {
        "AssumedRoleId": "AROA6FFPKEFIWPDLFBCAZ:terraform-xyz",
        "Arn": "arn:aws:sts::973177758033:assumed-role/xyz-Devops/terraform-xyz"
    },
    "Credentials": {
        "SecretAccessKey": "XNF00HmzNuWh0WT/toalYRRkTuH5jPRkqMOrSaWT", 
        "SessionToken": "FQoGZXIvYXdzEOf//////////wEaDODPAGeUh1F5W6LaJSLyAarxd4GAIO66gI5Kj2eQdgSFhgFXXysGwgan1Y+1NlST11BoWnuteV9+7r1bmvpT8Shcb+UMlRztm1PYdJKhIHaJJLrHypIggSh5ffS3Nofni/AXLVWWKZ3shhjRjBsEeFao5LlVAsyUyWM+6Lt0KlNzNigKqm2LJs4KVwP01nmxAMVElwVQM4IBCcCJd5ctL1uDqWYQMn6Wlj+tUfhL+yon4n1L9kY+T0JxJR9pqV3I2Wljnw2N8hthcIE4wtygXIpSoHaaWwQT80tlMhyG3Z6QTxZm15SKpH+86aHhT3KUafUNgL7DWXzoYGWu57iPYresKMbi/OwF",
        "Expiration": "2019-10-10T14:25:26Z",
        "AccessKeyId": "ASIA6FFPKEFIQOJ5NOIF"
    }
}
```

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>