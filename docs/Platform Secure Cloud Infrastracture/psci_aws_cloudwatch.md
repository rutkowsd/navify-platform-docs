# AWS CloudWatch

## What is CloudWatch

CloudWatch is Amazon Web Services native monitoring service for AWS cloud resources and applications. AWS CloudWatch is used primarily to collect and track metrics, collect and monitor log files, and set alarms at user-defined thresholds.

As the NAVIFY platform infrastructure is built on AWS, Cloudwatch provides detailed monitoring capabilities for the various resources which NAVIFY products use on a day-to-day basis.

\[More coming soon...\]

[Learn more about AWS CloudWatch](https://aws.amazon.com/cloudwatch/)

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
