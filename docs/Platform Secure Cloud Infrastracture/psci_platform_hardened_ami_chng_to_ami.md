# Changes to Platform Hardening AMIs

The NAVIFY Platform team is responsible for the Hardened AMI pipeline. This means any changes to the baseline security measures within the shared Hardened AMI have to be managed by the Platform SecOps team. 
In cases where a product team requires to make a change to the baseline security measures they must raise a change request to the NAVIFY Platform SecOps team via [**JIRA Service Desk**](https://navifypoc.atlassian.net/servicedesk/).

> <p style="color:#F78D2A; font-weight:bold">IMPORTANT!</p><p style="color:#F78D2A;">Any changes introduced to the Hardened AMIs baseline security measures by the Product team, without going through the standard change process will be detected as rogue AMI-IDs.</p>

## Disclosure

If the NAVIFY Platform (continuous) cyber defenses, including, but not limited to, security measures, controls, monitoring, detection, alerting, or general toolchain is found to be modified, bypassed, avoided, ignored, etc. the availability and integrity of your account(s) may become negatively impacted.
It is your responsibility to ensure that all engineers within your org that use the NAVIFY Platform, a shared platform, are also aware of their shared responsibility and required diligence to uphold and adhere to the integrity of our cyber defenses in totality. This also includes ensuring they’re aware of the potential impacts to not only your account(s), but also the potential account(s) and data of others within our shared platform if they choose to neglect our cyber defenses.

## Change Request Process

![Change Request Process](https://s3.amazonaws.com/user-content.stoplight.io/18223/1571752182115)

It is your responsibility (Product Team’s) to clearly communicate the detailed reasoning and justification to the NAVIFY Platform SecOps team if an edge case or corner case arises which may require exception(s) to be made by way of altering our cyber defenses and/or toolchains. An evaluation must occur prior to any alterations or modifications being made for exception(s).  

Prior to a final decision being made, dependent on potential risks and risk impact, as well as time involved to measure risk probability, additional dialog may occur requesting supporting information. (Continuous) attempts may also occur to help find alternative solutions prior to making any exception(s).  

When exception(s) are determined to directly or indirectly create new risks and/or elevate risk impacts for the platform as a whole, or in-part by affecting other tenants, the decision may ultimately be made not to accommodate the exception(s).  

If this is the outcome, and further discussion is needed, we advise looping in the security contact(s) within your orgs leadership team to discuss security concerns in further detail.  

**In summation**, for all engineers -- what may seem like small, localized, or short-lived neglect, may not always be the case for our platform as a whole, especially at scale. We strongly encourage a security conscious and security-first mindset, and to always remember that we do have neighbors present on the platform that put trust, even blind trust for most, in your diligence to uphold the security-posture and integrity of not only your platform space, but their platform home as well.

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>