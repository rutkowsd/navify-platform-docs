# AWS CloudTrail

## What Is AWS CloudTrail?

AWS CloudTrail is a security agent that logs, monitors and stores information on your account history across the AWS infrastructure helping with governance, compliance and auditing of the AWS account.

AWS CloudTrail runs on EC2 and ECS instances.

CloudTrail works together with AWS Inspector. When a trail is created, CloudTrail sends all the events recorded in the Inspector logs to the S3 bucket.
AWS Cloudtrail captures and records the activity -for example AWS Inspector activity-as a Cloudtrail event.

For more information on ClodTrail, check:

 [ AWS CloudTrail](https://aws.amazon.com/cloudtrail/) and [ AWS CloudTrail log files](https://docs.aws.amazon.com/awscloudtrail/latest/userguide/cloudtrail-log-file-examples.html)

## What Is AWS CloudTrail Added Value for Your Product?

AWS CloudTrail simplifies validation of compliance and standards with internal policies by providing a history of activity in the AWS account.  
CloudTrail records AWS API calls for your account and delivers log files to you.
With CloudTrail, you can:

* Get a history of AWS API calls for your account.
* View into user and resource activity.
* Do security analysis, resource change tracking, and compliance auditing through the AWS API call   history produced by CloudTrail. 
* View events that CloudTrail delivers and are stored in the S3 bucket.

The recorded information includes:

* The identity of the API caller.
* The time of the API call.
* The source IP address of the API caller.
* The request parameters. 
* The response elements that the AWS service returns. 

## CloudTrail Logs

## Roles and Responsibilities

## Prerequisites

You need to request and obtain an AWS account.

[Learn more about AWS CloudTrail](https://aws.amazon.com/cloudtrail/)

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
