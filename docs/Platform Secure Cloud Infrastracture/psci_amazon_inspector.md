# Amazon Inspector

## What Is AWS Inspector?

Inspector is a security service dedicated to improve security and compliance of newly developed and deployed applications running on Amazon EC2, ECS-EC2, EKS-EC2 instances only.  
The service allows you to configure a vulnerability scanner that scans your resources and assesses the vulnerability level.  
Inspector is an agent-based service that is deployed on your EC2 instances.  
**NOTE**  
Inspector does not run on ECS containers.  

When SecOps creates an AMI with Inspector built-in, the AMI is shared with the Product.  
When you launch an EC2 instance in your account, you are able to monitor the selected instance, track potential issues and security vulnerabilities deriving from the application.  
For SecOps to run the Inspector agent, you must tag the instance with the key **Inspector_Scan** and the value set to **True**.
  
When Inspector finds vulnerabilities and deviations from standard behaviour in your applications, it produces a list of security hazards findings.  

Inspector is part of the AWS cybersecurity suite and is integrated with AWS CloudTrail, which captures API calls from Inspector.

[Learn more about AWS Inspector ](https://aws.amazon.com/inspector/) and [CloudTrail](https://aws.amazon.com/cloudtrail/)

## What Is AWS Inspector Added Value for Your Product?

Amazon Inspector supports HIPAA and provides your product with an extra layer of security by:
* Integrating security with DevOps.
* Reducing the risk of introducing vulnerabilities with a new application.


Inspector identifies vulnerabilities in your system and/or applications through the use of rules packages applied on the target systems.  
Inspector rules packages are:
* Common Vulnerabilities and Exposures (CVE Database).
* Center for Internet Security (CIS).
* Security Best Practices.
* Runtime Behavior Analysis  
  This analysis based on the services deployed on the target EC2 instance includes - but it is not limited to- ports, services, unused TCP ports, and client protocols.

Based on the Shared Responsability mode, AWS Inspector findings depend on various factors such as your choice of:
* Rules packages included in each assessment template.
* Presence of non-AWS components in your system.
To learn more about Rules packages, check:

[Inspector Rules Packages](https://docs.aws.amazon.com/inspector/latest/userguide/inspector_rule-packages.html)

  
AWS doesn't guarantee that following the provided recommendations will resolve every potential security issue.  You are responsible for the security of applications, processes, and tools that run on AWS services. For more information, see the AWS Shared Responsibility Model for security.


## Use Case and Scenarios
You build and deploy an application on AWS and need to ensure that your application is compliant and any security vulnerability is spotted and resolved timely.

 Reviews findings to remediate.
* Helps you to enforce security standards.
* Allows you to define your security standards.
* Uses rules bundles that are mapped to the rules and policy Knowledge base.

## Inspector Logs
Inspector logs record avery action/event that happens in your EC2 instance, whilst CloudTrail records all events that occurred and all the actions that have occurred as a result of a given event.
Inspector performs runtime behaviour analysis, checks for CPU, memory, RAM and more. 
Check the Inspector log sample to view the items that are analysed.


## What Are the Inspector Flow Logs?
Inspector is integrated with CloudTrail that captures Inspector activity. All of the Inspector events are recorded in a CloudTrail event. 
You can access Inspector findings in two ways:
* Through Event history  
You download recent events from your AWS account and view them in Event history.
* S3 bucket  
You must create a trail if you want CloudTrail to continuously deliver log files to the S3 bucket.  
*NOTE*  
Trail creation is an automated process. A trail exists between 1 hour and 1.5 hours to generate and export the Inspector scan results to PDF and place them in the S3 bucket.
After the export, trails are automatically purged.
From a trail, tlife is between one hour to 1.5 hours

To learn how to create a trail, check:  
[How to Create a Trail for Your AWS Account](https://docs.aws.amazon.com/awscloudtrail/latest/userguide/cloudtrail-create-and-update-a-trail.html)

## Roles and Responsibilities

In term of service accountability, the SecOps NAVIFY Platform is responsible for:

* Configuring the service for every new account.
* Maintaining the service.

## Prerequisites

You need to request and obtain an AWS account.
Once you receive your AWS account, you can install the Inspector Agent on your AMI.

To install the Inspector agent, check the AWS documentation:
[Installing AWS Inspector Agent](https://docs.aws.amazon.com/en_pv/inspector/latest/userguide/inspector_installing-uninstalling-agents.html).

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
