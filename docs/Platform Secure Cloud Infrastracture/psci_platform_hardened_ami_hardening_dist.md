# Platform Hardening and AMIs Distribution Process

The NAVIFY Platform security team offers **Hardened Amazon Machine Images (AMIs)** or a so called **golden AMI**. A golden AMI is an AMI that is standardized through configuration, consistent security patching, and hardening. It also contains agents approved for logging, security, performance monitoring, etc.

The SecOps team then takes on the responsibility for:

1. Distributing the golden AMI(s) to product accounts.
2. Continuously assessing the security posture of all active golden AMIs.
3. Decommissioning golden AMIs once obsolete.

## Hardening an AMI

The Platform AMI hardening and distribution process is broken down into 3 phases.

* Build
* Validate & Approve
* Distribute

Most notable agents and services that are baked in:

* fireeye HX - (agent: xagt)
* Osquery (agent: osqueryd)
* AWS Inspector agent (agent: awsagent)
* Sumo Logic collector (service: sumocollector)
* ClamAV
* Audit daemon (auditd)
* checksec

## Build

![Build process](https://s3.amazonaws.com/user-content.stoplight.io/18223/1571752325138)

| Step | Description |
| --- | --- |
| **1 - Create Instance** | An instance is created from the latest available base AMI. This is an Amazon AMI with a specific Linux or Windows OS. |
| **2 - Run OS Hardening** | When the instance is up and running, packages and scripts are securely downloaded from our secure jFrog Artifactory and executed. This could include operating system updates, operating system hardening scripts, and the installation of new software packages and configuration changes. |
| **3 - Build Gold AMI** | After the instance has been updated, a new hardened AMI is created. |

## Validate and Approve

![Validate and Approve](https://s3.amazonaws.com/user-content.stoplight.io/18223/1571752214300)

**Validation Phase:** We use Amazon Inspector to verify that our instances meet our security requirements.

| Step | Description |
| --- | --- |
| **4 - Create EC2 Instance** | A new instance is created from the golden AMI |
| **5 - Run Validation** | When the instance is up and running, we run Amazon Inspector to validate the golden AMI. Amazon Inspector produces a report that is assessed and reviewed before the golden AMI is made available for distribution.|
| **6 - Approval** | After the scanning is complete, we inspect the generated reports before approving the new golden AMI, giving it a `rc` number, and distributing it for consumption. |
| **7 - New Golden AMI** | The new golden AMI ID is stored in a data store (SSM Parameter Store) for product consumption. |

## Distribute

The NAVIFY Platform SecOps team will distribute the Hardened AMIs to all Product Account ID(s) during onboarding. All approved Hardened AMIs are shared from the Platform SecOps AWS account number `186572554010`.

> <p style="color:#0066cc; font-weight:bold">NOTE:</p>
> <p style="color:#0066cc;">The Hardened AMIs will be made available within the <b>Amazon Region(s)</b> specified when an account was requested.</p>

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
