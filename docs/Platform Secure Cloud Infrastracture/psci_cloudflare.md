# Cloudflare

## What is Cloudflare

Cloudflare is a cloud delivery network that provides server and application data quickly and securely. Cloudflare provides a security layer for any product calling the NAVIFY platform against DDos attacks, malware, and other security threats.

Aside from basic security features, Cloudflare provides a way for securely registering Roche domains and SSL certificates in a centralized SSL cert repo while also providing a centralized location for viewing domain security and network configuration statistics for the NAVIFY platform. Cloudflare monitors configured cloud metrics and sends alerts when thresholds are exceeded.

[Learn more about Cloudflare](https://www.cloudflare.com/)

## How to setup Cloudflare for your product

To enable Cloudflare you need to follow the steps below:

> We use example.dev.product.navify.com as an example for the next steps.

### 1. Create a CNAME entry on your account

Create example.dev.product.navify.com with a CNAME of example.dev.product.navify.com.cdn.cloudflare.net (the host will always be appended with ".cdn.cloudflare.net")

### 2. Send the origin domain and edge domain to your Genius

e.g. example.lb-12345678.us-west-2.elb.amazonaws.com and example.dev.product.navify.com

Within Cloudflare, the platform team will create example.dev.product.navify.com with a CNAME value of example.lb-12345678.us-west-2.elb.amazonaws.com.

As long as you have a valid, non-expired (ACM/letsencrypt/etc.) TLS certificate on the origin host/LB, the traffic will start flowing through. The origin certificate does NOT need to contain the domain used on the edge.

This will effectively shrink your attack surface, since the origin host will no longer be know to the outside, and will be masked as example.dev.product.navify.com.cdn.cloudflare.net

### Traffic permissions

Once you're setup and have verified success behind Cloudflare, we can go a step further and give you a list of Cloudflare CIDR's that you can place within a SG. You'd then be able to block * and only permit the Cloudflare ranges for ingress.

This would effectively block people that find you by scanning subnet ranges, or if they find your original origin in a cached page or something alike.

### Live Examples

A few live examples currently behind Cloudflare are:

1. www.qa.nasp.navify.com :: has a CNAME alias of d3drhs22clskvm.cloudfront.net
2. api.qa.nasp.navify.com :: has a CNAME alias of qa-nasp-lb-1004866496.us-west-2.elb.amazonaws.com
3. us.api.platform.navify.com has a CNAME alias of us-west-2.api.platform.navify.com

(1) and (2) are blocking * and only allowing Cloudflare CIDR's – you can check the origin and edge both directly, and should notice only the edge permits traffic.

All three examples have their subdomain within r53 and control their own records. All we need is the origin CNAME.

### Miscelaneous

This will also be effective immediately once setup:

- Only permit TLS 1.2 and 1.3, with safe ciphers and keep you at an A grade with tls tests, like qualys: [https://www.ssllabs.com/ssltest/analyze.html?d=us.api.platform.navify.com&amp;latest](https://www.ssllabs.com/ssltest/analyze.html?d=us.api.platform.navify.com&amp;latest)
- Permit IPv6 traffic by way of their IPv6->IPv4 automatic gateway: [https://www.cloudflare.com/ipv6/](https://www.cloudflare.com/ipv6/)

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
