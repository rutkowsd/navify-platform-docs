# What do you get with your platform account

## What is in a provisioned account

The **NAVIFY Platform provisioned AWS account** for products comes with preconfigured objects concerning security and infrastructure configuration.

The preconfigured elements consist of:

- **Roles**
- **Enabled security features** ([Secure Cloud Infrastructure](https://docs.platform.navify.com/guides/platform-secure-cloud-infrastructure/cybersecurity/security-introduction))
- **VPCs created in the product account**
  - The standard **"default"** VPC created when you create an AWS account
  - NP **"BASE"** VPC (see table for details)
  - NP **"MGMT"** VPC*
- **Bastion host deployed in VPC**

## Details for the specific elements

### Roles

The AWS account will have the following roles available

| Role | Description |
| --- | --- |
| `roche/[product_name]/[product_name]-Devops` | This IAM role is used by product team members to work in the provisioned account  |
| `NavifySecOps` | this IAM role is used by platform security team to provision the necessary security objects and enable logging and monitoring tools |
| `navify-Devops` | this IAM role is for Devops with super admin permissions |
|`Dome9-Connect`| this IAM role is for applying the Dome9 policies |
| `EC2-SumoCollectorRole` </br> or `SumoCollector` </br> or `EC2-SumoCollector` | this IAM role is for EC2 instances to register with Sumologic. </br></br>**Note:** SumoCollectore role is the most current standardized role name. Other roles may still be visible in Platform accounts created earlier |

### Enabled security features

See [Secure Cloud Infrastructure](https://docs.platform.navify.com/guides/platform-secure-cloud-infrastructure/cybersecurity/security-introduction)

### VPCs provisioned in your product account

The NAVIFY Platform team provisions two additional VPCs in your account in the region(s) the account was requested to be provisioned in. This is on top of the [Amazon default VPC](https://docs.aws.amazon.com/vpc/latest/userguide/default-vpc.html) created by AWS when an AWS account is created.

> **Read more on AWS VPCs**
>
> - [Amazon VPC](https://aws.amazon.com/vpc/)
> - [Amazon default VPC](https://docs.aws.amazon.com/vpc/latest/userguide/default-vpc.html)
> - [Amazon VPC pricing](https://aws.amazon.com/vpc/pricing/)
>

| | **np-base-VPC** | **np-mgmt-VPC** |
| --- | --- | --- |
|**General Information** | This should be the VPC of choice for standard use, think of **“BASE”** as synonymous with default.  If your stack requires custom VPCs, e.g. by way of CFN or Terraform, that is **OK**.  This is simply provided as a means to avoid using the default VPC created by AWS.  **Following best practice, do not use the “default” VPC.** | This VPC is for hosting your hardened bastion.  It will be peered with the NP **\"BASE\"** VPC. Details are below.|
| **Deployment Region** | Default: Us-west-2 </br></br>Deployed per operation region | Default: Us-west-2  </br></br>Deployed per operation region |
| **VPC Name** | np-base-VPC | np-mgmt-VPC |
| **CIDR** (Classless Inter-Domain Routing) | Default: `172.16.0.0/16` </br></br> The Private and Public Subnets will be assigned to availability zones (AZ) consecutively within a single region. The script creates the `PublicSubnet01Block` and `PrivateSubnet01Block` in AZ1, then continues to create `PublicSubnet02Block` and `PrivateSubnet02Block` in AZ2, then if AZ3 is available creates Block 03.| Default: `192.168.128.0/18` </br></br>The Private and Public Subnets will be assigned to availability zones (AZ) consecutively within a single region. The script creates the `PublicSubnet01Block` and `PrivateSubnet01Block` in AZ1, then continues to create `PublicSubnet02Block` and `PrivateSubnet02Block` in AZ2 |
| **Public Subnets**| `PublicSubnet01Block`:</br> Default: `172.16.192.0/20` </br></br> `PublicSubnet02Block`:</br> Default: `172.16.208.0/20`</br></br> `PublicSubnet03Block`: Default: `172.16.224.0/20` </br></br> Unused block in defaults is `172.16.240.0/20` | `PublicSubnet01Block`:</br> Default: `192.168.160.0/20` </br></br>`PublicSubnet02Block`: </br>Default: `192.168.176.0/20`|
| **Private Subnets** | `PrivateSubnet01Block`</br>Default: `172.16.0.0/18`</br></br>`PrivateSubnet02Block`</br> Default: `172.16.64.0/18`</br></br>`PrivateSubnet03Block`</br> Default: `172.16.128.0/18` | `PrivateSubnet01Block`: </br>Default: `192.168.128.0/20`  </br></br>`PrivateSubnet02Block`: </br>Default: `192.168.144.0/20` |
| **Internet Gateway** | Yes Attached to the VPC [VPC Internet Gateway](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Internet_Gateway.html)| Yes Attached to the VPC [VPC Internet Gateway](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Internet_Gateway.html)|
| **NAT Gateway** | Created in PublicSubnet01 Also an elastic IP associated with the NAT Gateway [EIP link](https://aws.amazon.com/ec2/pricing/on-demand/#Elastic_IP_Addresses) costs generated per hour and per data processed [VPC NAT Gateway](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-nat-gateway.html)| Created in PublicSubnet01 Also an elastic IP associated with the NAT Gateway [EIP link](https://aws.amazon.com/ec2/pricing/on-demand/#Elastic_IP_Addresses) costs generated per hour and per data processed [VPC NAT Gateway](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-nat-gateway.html)|
| **Public and Private Routing** | The routing tables created for the public and private subnets based on the Public and Private CIDR Blocks| The routing tables created for the public and private subnets based on the Public and Private CIDR Blocks |
| **S3 Endpoints** | Creating S3 endpoints that allow your VPC to access the S3 buckets created in the region this VPC is deployed in. You can then configure your S3 buckets to limit access to it only from the defined range of addresses.| Creating S3 endpoints that allow your VPC to access the S3 buckets created in the region this VPC is deployed in. You can then configure your S3 buckets to limit access to it only from the defined range of addresses. |
| **Bastion Host** | No Bastion deployed to this VPC | Bastion Instance Type: **'m4.large'** (default) </br></br>[Amazon instance types](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/instance-types.html)</br></br> [Amazon instance types pricing](https://aws.amazon.com/ec2/pricing/on-demand/)</br></br> Bastion is deployed in `Public Subnet 01` with access from the following IP ranges: </br></br>SecurityGroupIngress:</br> * Description: \"Ssh from RCN US Bay Area\" </br>IP Protocol: \"tcp\" From/To Port: '22'</br>CidrIp: 7`2.34.128.0/19` </br></br> * Description: \"Ssh from RCN\"</br> Ip Protocol: \"tcp\", From/To Port: '22'</br> CidrIp: `194.120.80.9/32` </br></br>* Description: \"Ssh from RCN\" </br> Ip Protocol: \"tcp\", From/To Port: '22' </br>         CidrIp: `196.3.48.0/21` </br></br>* Description: \"Ssh from RCN\"</br>        Ip Protocol: \"tcp\", From/To Port: '22' </br>CidrIp: `198.21.16.0/20`</br></br> * Description: \"Ssh from RCN\"</br>Ip Protocol: \"tcp\", From/To Port: '22'</br>CidrIp: `206.53.226.0/23` </br></br>* Description: \"Ssh from RCN\"</br> Ip Protocol: \"tcp\", From/To Port: '22'</br> CidrIp: `210.172.233.7/32`</br></br></br>An Elastic IP assigned to your Bastion|

## Do's and Don'ts"

- **When creating new instances**: Always attach the SumoCollector role to any instance profile before you launch it. This is a shared responsibility and ensures we have logs in the case of a security incident.

- **What to do if you delete the VPC and instances**: Remember to unreserve the elastic IPs that were registered with the instances NAT and Bastion. The reason is that Amazon will start billing you for those IPs if they are not attached to an instance
  - [EIP charges doc](https://aws.amazon.com/premiumsupport/knowledge-center/elastic-ip-charges/)
  - [Elastic ip pricing:](https://aws.amazon.com/ec2/pricing/on-demand/#Elastic_IP_Addresses)
- **Don't remove the roles provisioned in the account**. They are there for security purposes and to allow you to connect to the account.
- **There is a Platform IAM group** that users have to be added to, to access the Bastion host, among other steps outlined below.  
- **Platform Bastion 1.0 users must:**
  1. On any EC2 host where you want the IAM user created, you must have the `SecOps Bastion` IAM Instance Profile attached or the policies `SecOps Bastion` and `SecOps Bastion Assume Role` must be added to whatever IAM instance profile is attached to the EC2 host.
  2. Add the users SSH Public Key to their IAM User
  ![Diagram - How to request your platform account](https://s3.amazonaws.com/user-content.stoplight.io/18223/1565622433753 "Diagram - How to request your platform account")
  3. Create an IAM group named `bastion`
  4. Add users to the IAM group named `bastion`
  5. Wait 15 minutes for the scripts to run and update users locally

## Limitations

### Platform Bastion limitation

The Platform bastion workflow has a few limitations that are critical to understand:

- Usernames are based off IAM username and will be truncated to less than 32 characters after the removal of an email address (not an IAM limitation)
- Usernames are formatted for Unix safe values, so the username on the host may not match the IAM username exactly e.g. an email address: ryan.fackett@contractors.roche.com becomes ryan.fackett.at.contractors.roche.com

### AWS Limitations

Read dedicated chapter in this documentation about AWS limitations.

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
