# Platform Hardened AMI Security Policies

The list captures the main Hardened AMI Security Policies. Each product must adhere to these policies to continue with a Platform Secure account.

1. The NAVIFY Platform does not and cannot capture or store **PHI** or **PII** data in any of our systems logs. Therefore we do not and can not capture any **application logs**. Please do not modify your solutions to capture application logs along with the system logs.  
The Platform SecOps logging and monitoring solution is built to log, aggregate, monitor, and analyze **system logs** `ONLY`. This is because the platform chooses not to store any **PII** and/or **PHI** data. Therefore it is **important for us not to capture any application logs**.  

    > <p style="color:#F78D2A; font-weight:bold">IMPORTANT!</p>
    > <p style="color:#F78D2A; font-weight:regular">Please do not modify your logging to capture application logs to be sent to our logging solution.</p>

2. Sumo Logic roles: Use the Sumo logic IAM role provided with the account `SumoCollector` (latest and standardized), `EC2-SumoCollector`, or `EC2-SumoCollectorRole` when launching an ec2 instance. It is a must to assign the role prior to launching the ec2 instance.  

    Read more... [AWS Docs:  AWS CLI > Launch, List, and Terminate Amazon EC2 Instances](https://docs.aws.amazon.com/en_pv/cli/latest/userguide/cli-services-ec2-instances.html)

3. If you need to launch an ec2 instance with another role. You must ensure that at least the policy SumoLogic is assigned to the role that will be used. It is a must to have the `SumoLogic` policy assign prior to launching the ec2 instance. This policy is a strict requirement.  

    Read more... [AWS Docs: Amazon EC2 > IAM Roles](https://docs.aws.amazon.com/en_pv/AWSEC2/latest/UserGuide/iam-roles-for-amazon-ec2.html)

4. Any non-hardened platform provided AMI’s detected in use (depending on the environment type), will either be terminated immediately or a notice will be sent detailing the date and time in which the instance will be terminated if no action is taken by the product.

5. In a DEV environment, product teams are expected to migrate to platform-approved hardened AMIs, therefore there will be a window of allowed usage of unauthorized AMIs, strictly for the purpose of testing and migrating to platform-approved hardened AMIs, which must be used, and can be seen shared from the SecOps account ID (`186572554010`)

6. In a Stage, UA, Production environment, product teams are expected not to use any other Hardened AMIs except for the NAVIFY Platform shared Hardened AMIs.

7. As a user of the Hardened AMIs you must follow the rules described in the Hardened AMI Configuration document ([See Hardened AMI Configuration Guiding Rules](https://docs.platform.navify.com/guides/platform-secure-cloud-infrastructure/platform-hardened-ami/new-subpage))

## Training

**Cornerstone training** on platform security and shared responsibility will be assigned to all product team security champion, security proxys, and all users building on the platform. It is a must for the product security champion fto take the cornerstone training on platform security.

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
