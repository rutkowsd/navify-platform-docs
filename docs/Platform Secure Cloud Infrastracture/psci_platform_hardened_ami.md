# Platform Hardened AMIs Introduction

The NAVIFY Platform takes on the responsibility for providing secure cloud infrastructure, one of the elements of this infrastructure is creating and sharing Hardened AMIs with product teams.

The NAVIFY Platform security team offers Hardened Amazon Machine Images (AMIs) (or so called golden AMIs). A golden AMI is an AMI that is standardized through configuration, consistent security patching, and hardening. It contains agents approved for logging, security, performance monitoring, and more. We ensure that the images continue to meet Roches InfoSec requirements.

## NAVIFY Platform Hardened AMI Offering

The NAVIFY Platform currently provides the following instance types of Hardened AMIs

* [EC2](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/concepts.html)
* [ECS](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ECS_instances.html)
* EKS (we are currently working on providing this option, however it is not something we provide for product consumption)

### The Base AMIs OS versions

* (Hardened) Ubuntu 18.04.2 LTS (Platform Default)
* (Hardened) Ubuntu 16.04.6 LTS
* (Hardened) [Amazon Linux 1 (2018.03)](https://aws.amazon.com/amazon-linux-ami/)
* (Hardened) [Amazon Linux 2 (2017.12) LTS](https://aws.amazon.com/amazon-linux-2/)
* (Hardened) Windows 2016
* (Hardened) Windows 2019

### Bastion AMI

* (Hardened) Ubuntu 18.04.2 LTS (Platform Default)
* *[TODO - Link to documentation about Bastion Host AMI]*

### ECS-EC2 AMI

* (Hardened) Ubuntu 18.04.2 LTS ECS-optimized (Platform Default)

### EKS-EC2 AMI

* (Hardened) Ubuntu 18.04.2 LTS EKS-optimized (Platform Default)

## Current AMI Distribution

[See the Hardened AMI Distribution Versions](https://docs.platform.navify.com/guides/platform-secure-cloud-infrastructure/platform-hardened-ami/distribution-versioning)

## Bare-minimum instance type suggested for running our hardened AMI

>Our base AMIs have security-specific software installed and tuned, and although we put efforts in systematically capping resource consumption, we do require resources.  This is why we suggest these instance types at the very minimum.

### Dev environments

EC2 instance types with at least a vCPU count of 2, and 4 GiB of Mem. e.g. `t2.medium` (and above), while using product specific software that normally consumes little CPU/Mem.

### QA, Stage, & Production environments

EC2 instance types with at least a vCPU count of 2, and 4 GiB of Mem. e.g. `t3.medium` (and above).  We request teams use good judgement on instance types based on projected and/or profiled resource consumption and requirements, with regard to their product-specific software that will run on our hardened base.

## Compute costs for amazon ec2

> Please consider the compute costs for launching an ec2 instance in specific regions.

For current pricing of Amazon ec2 instance visit [https://aws.amazon.com/ec2/pricing/on-demand/](https://aws.amazon.com/ec2/pricing/on-demand/)

### Example pricing for general purpose usage

#### Region us-west-2 (Oregon)

| Type | vCPU | ECU | Memory (GiB) | Instance Storage (GB) | Linux/UNIX Usage (hour) | Linux/UNIX Usage (day) |
| --- | --- | --- | --- | --- | --- | --- |
| **t2.medium** | 2 | Variable | 4 GiB | EBS Only | $0.0464 per Hour | $1.1136 per Day|
| **t3.medium** | 2 | Variable | 4 GiB | EBS Only | $0.0416 per Hour | $0.9984 per Day|
| **m4.large** | 2 | 6.5 | 8 GiB | EBS Only | $0.10 per Hour | $2.4 per Day|
| **m4.xlarge** | 4 | 13 | 16 GiB | EBS Only | $0.20 per Hour | $4.8 per Day |

For current pricing of Amazon ec2 instance visit [https://aws.amazon.com/ec2/pricing/on-demand/](https://aws.amazon.com/ec2/pricing/on-demand/)

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
