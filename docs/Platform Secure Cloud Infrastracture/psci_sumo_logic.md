# Sumo Logic

Sumo Logic collects, analyzes, and manages log data. For the systems it monitors, it tracks activity by users and outreach from other programs. This could mean the number of access requests (and failures) on an AWS bucket, or a login by a registered user. Sumo Logic is a Software as a Service, and the logs are collected and stored in Sumo Logic’s cloud.

## Environment details (Dev/Prod)

Sumo Logic is not separated into development or production environments. It has been set up as a single instance that is connected to an AWS S3.

The SecOps tools store log data to the S3 bucket. There is a single Sumo Logic instance which then polls the S3 bucket and aggregates log data into dashboards and queries.

\[More coming soon...\]

[Learn more about Sumo Logic](https://www.https://www.sumologic.com/)

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
