# Security

## What is the NAVIFY Platform Secure Cloud Infrastructure?

The NAVIFY Platform secure cloud infrastructure is a security umbrella deployed over your AWS account. It consists of services, tools, hardened AMIs, and security processes assembled and configured on industry best practices.

Our goal is to protect your account from external and internal security threats, however **it is in your responsibility** to make sure that the product you are building is secure.

When you receive an account on the NAVIFY Platform the Secure Cloud Infrastructure is already configured so you can concentrate on working on your product.

Each security service is dedicated to monitor a specific area of your cloud instance and each service creates a log listing all the threats and abnormal behaviours that are detected.

## What Are the Security Services Under the NAVIFY Umbrella?

The Platform security team enables this set of security services, analytics tools, and hardened AMIs.

- Cloudflare
- AWS CloudTrail
- Amazon Inspector
- Amazon GuardDuty
- VPC Flow Logs
- Dome9 (Continues Compliance Dashboard)
- Sumo Logic (Dashboards and Alerts)
- Splunk (Roche MIR team dashboard)
- NAVIFY Platform Hardened AMIs (which use multiple tools and agents to secure your OS instance)

Table: The Security Layers

| Where | Description |
| --- | --- |
| World Wide Web | Cloudflare services sit between the visitor and the Cloudflare user's hosting provider and check for external threats.|
| AWS Account | CloudTrail monitors and tracks account activity related to actions across your AWS infrastructure. |
| VPC | GuardDuty provides continuous monitoring of the VPC. Intelligent threat detection to protect your AWS accounts and workloads |
| Resources | CloudWatch monitors and collects operational data in the form of logs of your applications and resources. |
| EC2 | Inspector checks for unintended network accessibility and vulnerabilities of your EC2 instances.|

The diagram below shows a more detailed overview of data sources and their flow.

![Diagram - Security Data Sources](https://s3.amazonaws.com/user-content.stoplight.io/18223/1565102659867 "Diagram - Security Data Sources")

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
