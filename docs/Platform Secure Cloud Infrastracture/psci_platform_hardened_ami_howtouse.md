# How to Use Platform Hardened AMIs

## How to Find the Platform Approved Hardened AMIs

### Amazon Console

You can view your AMI list in the EC2 dashboard under EC2 > Images > AMIs > Private images

![AMIs in Console](https://s3.amazonaws.com/user-content.stoplight.io/18223/1571752532210)

### Amazon CLI

> Prerequisites: you must have AWS CLI installed on your computer (see: [add link: how-to-work-with-your-account](https://dasdasd))

```bash
$aws ec2 describe-images  --owners 186572554010
```

`186572554010` = This is the (SecOps) account ID where Platform authorized Hardened AMIs are shared from.

See also: [AWS Docs > Finding Shared AMIs](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/usingsharedamis-finding.html)

## Launching the Hardened AMI

> NOTE: Use the Sumo logic IAM role provided with the account `SumoCollector` (latest and standardized), `EC2-SumoCollector`, or `EC2-SumoCollectorRole` when launching an ec2 instance. It is a must to assign the role prior to launching the ec2 instance.

It is important that you launch the Platform Hardened AMI with our SumoCollector role. Here is a document from AWS on how you can do this [AWS Docs: IAM Roles for Amazon ec2 section: Launching an Instance with an IAM Role](
https://docs.aws.amazon.com/en_pv/AWSEC2/latest/UserGuide/iam-roles-for-amazon-ec2.html)

## Accessing your ec2 instance

General prerequisites to using your ec2 instance [AWS Docs > ec2 Connection prerequisites](https://docs.aws.amazon.com/en_pv/AWSEC2/latest/UserGuide/connection-prereqs.html)

Accessing your ec2 instance [AWS Docs > ec2 Accessing Instance](https://docs.aws.amazon.com/en_pv/AWSEC2/latest/UserGuide/AccessingInstancesLinux.html)

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
