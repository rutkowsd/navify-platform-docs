# Learn more about AWS

## AWS Documentation

For your information, the table below gives you a list of the main AWS services used by the platform as well as a link to the AWS documentation.

We believe that you should have a good understanding of these services when building with the NAVIFY Platform.

| AWS Service | Link and explaination |
| --- | --- |
| **EC2** | Learn about EC2 and how to run virtual machines in AWS.<br />[EC2 Documentation](https://docs.aws.amazon.com/ec2/index.html)|
| **VPC** | Learn about networking in AWS.<br />[VPC Documentation](https://docs.aws.amazon.com/vpc/index.html)|
| **ECS** | If you've got a bunch of docker containers to run your application, we encourage you to run your applications in ECS!<br />[ECS Documentation](https://docs.aws.amazon.com/ecs/index.html)|
| **API Gateway** | API Gateway is the main interface between your product and the platform.<br />[API Gateway Documentation](https://docs.aws.amazon.com/apigateway/index.html)|
| **AWS Glossary of Terms** |Look up for AWS terms. It's always good to speak the same language.<br />[AWS Glossary](https://docs.aws.amazon.com/general/latest/gr/glos-chap.html)|

## AWS Whitepapers & Guides

Expand your knowledge of the cloud with AWS technical content authored by AWS and the AWS community, including technical whitepapers, technical guides, reference material, and reference architecture diagrams.

* [AWS Whitepapers & Guides](https://aws.amazon.com/whitepapers)

## Additional Roche offerings

Roche employees have access to multiple platforms where you can learn and practice AWS:

| Platform | Link |
| --- |--- |
| Linkedin Learning | [https://www.linkedin.com/learning](https://www.linkedin.com/learning)|
| QWIKLABS | [https://www.qwiklabs.com/](https://www.qwiklabs.com/)<br />Sign up with your Google Account to receive Credits.|

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
