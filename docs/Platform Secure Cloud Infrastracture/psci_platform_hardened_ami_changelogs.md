# Platform Hardened AMIs Changelogs

The current release cycle **`rc01`** is the first version of the Hardened AMI release to product teams. Hence there is no changelog presented here.  
Once a new distribution version is available this changelog will capture changes from categories:

* Packages
  * Version changes
* Bug fixes (security)
* Bug fixes (non-security)
* Improvements/Enhancements

We will also make sure that anything that is CPU or MEM intense is documented and the change log description.

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
