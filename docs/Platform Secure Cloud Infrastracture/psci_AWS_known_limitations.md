# AWS Known Service Limitations

Yes, AWS has limits and they are pretty well known, You can find them in the AWS Document - [AWS Service Limits](https://docs.aws.amazon.com/en_pv/general/latest/gr/aws_service_limits.html). The article also leads you to information on potentially extending those limits.

Some most notable limits you should be aware of are listed below for direct access.

## API Gateway

Be aware of some limits and important notes about Amazon API Gateway
[https://docs.aws.amazon.com/apigateway/latest/developerguide/limits.html](https://docs.aws.amazon.com/apigateway/latest/developerguide/limits.html)

## SSH

If you are experiencing issues SSH’ing into your instances or bastion host, which may seem like you are denied or blocked, try clearing the identities from your authentication agent.
Type in bash:

> ssh-add -D

When this is the case, even specifying your key directly ("ssh -i <key.pem>") will not suffice and will still fail:

Troubleshooting instance connections link:
[https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/TroubleshootingInstancesConnecting.html](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/TroubleshootingInstancesConnecting.html)

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
