# Sumo Logic Alerts and Remediation Tactics

This document helps product owners to understand the meaning of the security alerts in Sumo Logic. It also provides guidelines on how to remediate them. Please consult with SecOps ([global.navify_platform_secops@roche.com](mailto:global.navify_platform_secops@roche.com) for more information or to help with documentation on additional alerts.

# Authentication

## Alert_SecOps_All_ROOT_running_suspicious_commands_\<PRODUCT_NAME>_\<CRITICALITY>

This alert triggers when certain commands run on a particular EC2 instance. These commands are security related and include `nmap`, `netcat`, `scapy`.

An example is listed below from running an `awk` command.
(note: `awk` will not be part of the commands read, is just an example of what the output looks like)

![pic1](https://s3.amazonaws.com/user-content.stoplight.io/18223/1579263466806)

As you may see, the column command will show the command run.  On the  _raw column, you can see more details.

The **suggestion** for this alert is to first see if that command has been run legitimately by anyone on the team. If not, please see the `_sourcehost` column and login to the system and see the command history of user root to check for command execution. On the `_raw` column you can see the `UID=0` which means “root or privileged user”. You can always ask **[SecOps team](mailto:global.navify_platform_secops@roche.com)** for help if there is no clue of what is going on.

## Alert_SecOps_SSH_failures_<PRODUCT_NAME>_last_24h_<PRODUCT_NAME>_<CRITICALITY\>

This alert could also be just information on failures on the account. It just looks like the below example:

![pic2](https://s3.amazonaws.com/user-content.stoplight.io/18223/1579263477818)

Here you can check with the `src_user` column, the `dest_hostname` and the `message`. You can ask your colleagues if those failures logins are legitimate.  If you see many failed attempts, it means it could be a possible brute force attempt. If no one in your team has an idea, you can always ask **[SecOps team](mailto:global.navify_platform_secops@roche.com)** for help.

## Alert_SecOps_Auth_Existing Users Added to Privileged Group_\<PRODUCT NAME>_\<CRITICALITY>

This alert shows if users have been added to privileged groups. This could be legitimate, but could also be a potential privilege escalation. Please investigate if the activity is from  normal user behaviors.

An example of this alert is shown below:

![pic3](https://s3.amazonaws.com/user-content.stoplight.io/18223/1579263487951)

You can see the process name (it could be add, remove, etc.) the `dest_group` and `dest_user` are added or removed. If you find something suspicious here, let **[SecOps team](mailto:global.navify_platform_secops@roche.com)** know and we will try to help.

# Antivirus

## Alert_SecOps_ClamAV_FOUND_infected_\<PRODUCT NAME>_\<CRITICALITY>

This alert is quite descriptive. ClamAv is the antivirus installed on the instances. If a virus is found, it will trigger that alert. You need to check if it is really suspicious,  there is a chance it could be legitimate.

Check the hash of the file identified as virus and cross-verify it with [https://www.virustotal.com](https://www.virustotal.com)

1. Login into the system
2. Check where the file is located and run this command: md5sum \<file>

![pic4](https://s3.amazonaws.com/user-content.stoplight.io/18223/1579263497895)

3. With the hash returned, copy it and paste in virustotal url:

![pic5](https://s3.amazonaws.com/user-content.stoplight.io/18223/1579263513963)

When clicking enter, it will search on many AVs to check if the file is malicious. If so, you can delete it.

# CloudTrail

## Alert_SecOps_Cloudtrail_AWS Console Logins_\<PRODUCT NAME>_\<CRITICALITY>

This alert is focused on information about logins that happened in the AWS console. It will list if there are failures and if MFA is enabled or not (MFA = multi-factor authentication). With this information, you can instruct your colleagues to **enable MFA** on the account. You can track if the logins are legitimate or not.

_Example of alert (note: username has been hidden in this picture)_

![insert pic 6 here](https://s3.amazonaws.com/user-content.stoplight.io/18223/1579263527684)

## Alert_SecOps_CloudTrail_malicious_Actors_\<PRODUCT NAME>_\<CRITICALITY>

This alert is correlating SourceIP's coming from malicious sources. If you see this, please check which instances are affected that could be communicating to these malicious IPs. As a recommendation, you could then modify its security group to be restrictive, and only allow IPs that you know (e.g **Roche IPs**, your home IP, etc). Instead of having a security group allowing `0.0.0.0/0`, let's be restrictive to avoid possible compromises. For Roche IPs list, follow link [here](https://docs.platform.navify.com/guides/miscellaneous/roche-gw-ips) or contact the [SecOps team](mailto:global.navify_platform_secops@roche.com).

## Alert_SecOps_CloudTRail_Users_Logins from Multiple IP_\<PRODUCT NAME>_\<CRITICALITY>

We could consider this as an informational alert, but just in case, we need to verify with the user. It will trigger an alert if in last 24 hours, a user has logged in using more than 2 distinct IP addresses. Of course this could be a normal behavior as we have different accounts, VPNs, etc. As said, please check with the affected user.

## Alert_SecOps_CloudTrail_SG_Authorizations_or_revocations \<PRODUCT NAME>_\<CRITICALITY>

This alert can treated as information, but needs to be investigated further. Someone has authorized a Security Group or Revoked it using the AWS console. The following is an example of one alert, where you can see the product name, sourceIP, user, Security group information and so on. Please pay attention to those **SG_FromRange** that if `0.0.0.0/0` it is too wide open. This should be restricted to Roche IPs or your home IP, etc, but not to everyone. etc. Please check that this one is legitimate. Please also check that the user has **MFA enabled**, if not, please enable it.

![insert pic7 here](https://s3.amazonaws.com/user-content.stoplight.io/18223/1579263536470)

## Alert_SecOps_CloudTrail_NetworkACL_Creation_Too_wide_open_\<PRODUCT NAME>_\<CRITICALITY>

This alert will trigger when an ACL has been created and opened to 0.0.0.0/2 ingress, meaning that is too wide open. Please configure by restricting ingress (inbound).

## Alert_SecOps_CloudTrail_SG_Authorizations_wide_open_\<PRODUCT NAME>_\<CRITICALITY>

Security groups should be careful configured. Sometimes for small amount if time, there is a need to open it to `0.0.0.0/0` for testing purposes. If this is not the case and it has been misconfigured, or is a permanent change, please restrict is as much as possible.

## Alert_SecOps_CloudTrail_potential malicious change of policies__HIGH  --> `only for SecOps`

Getting an error or failure response to legitimate control plane operation or even worse to illegitimate one is a red flag. In either case security organization will want to be notified about it, as it represents a potential malicious change of policies

## Alert_SecOps_CloudTrail_User added to Admin group_\<PRODUCT NAME>_\<CRITICALITY>

This alert triggers when a user is added to an Admin group. Please check that the activity is legitime. Contact the source User from the alert for more information.

## Alert_SecOps_CloudTrail_Console_Login_5+_Failures_in one hour timeframe_MEDIUM --> `only for SecOps`

Notify users with more than 5 console failures on 1 hours time slices.

## Alert_SecOps_CloudTrail_AttachUserPolicy_with_AdministratorAccess_\<PRODUCT NAME>_\<CRITICALITY>

In this example, the organization will be alerted when admin privileges are granted. We need to make sure that it is a legitimate operation. Will capture logs where user policy is changed and the request field indicates that admin access was granted. Please check the username involved and make sure there is a business reason to make those admin changes.

# Osquery

## Alert_SecOps_Osquery_SUID_ON_DANGEROUS_COMMAND_\<PRODUCT NAME>_\<CRITICALITY>

**SUID**: Set User ID is a type of permission that allows users to execute a file with the permissions of a specified user. Those files which have `suid` permissions run with higher privileges. There are some commands that, if a root user configured SUID on it, an attacker can escalate privileges and become root on that system. One example of that command is `cp`, but there are many others. This alert will trigger if any of those “dangerous commands” are setup with `SUID`.

If you receive this alert, the suggestion is to edit the file: `ls -la /bin/cp`

![insert pic8 here](https://s3.amazonaws.com/user-content.stoplight.io/18223/1579263548665)

You can see the command highlighted in red and in the left side, you can see the permissions `-rwsr-xr-x` the `s` is the `SUID` bit on it. In order to remove that privilege, first you need to ask your colleague to find out if someone did it on purpose. If not, please remove the `SUID` by running as `root`: `chmod u-s /bin/cp`

![insert pic9 here](https://s3.amazonaws.com/user-content.stoplight.io/18223/1579263556619)

## Alert_SecOps_Osquery_Auth_suspicious_commands_executions_\<PRODUCT NAME>_\<CRITICALITY>

This Alert is triggered when some "dangerous commands” are run on any EC2 instance. Commands like `nmap`, `netcat`, `nc`, `msfconsole`, etc. Please investigate if the command run was on purpose and if it legitimate. If not, you should check with [**SecOps team**](mailto:global.navify_platform_secops@roche.com) for help.

## Alert_SecOps_Osquery_DNS_resolvers_non_standard_\<PRODUCT NAME>_\<CRITICALITY>

Amazon EC2 default DNS resolvers are setup by AWS and they usually assign the address of `x.x.0.2` IP address or `127.0.0.53`, which is Route53. If for some reason, the dns resolver is not those listed, an alert will trigger. Please check if the change is legitimate or someone else from your team probably has done it for a reason. If not, you can contact [**SecOps team**](mailto:global.navify_platform_secops@roche.com) for advise.

## Alert_SecOps_Osquery_etc_hosts_changes_\<PRODUCT NAME>_\<CRITICALITY>

`/etc/hosts` file is where local resolution takes place. This normally overwrites the DNS resolution order, which means that someone can manipulate this file to bypass the DNS resolution. E.g: `navify.com` points to IP` 35.197.29.93`. If you configure `/etc/hosts` like `64.233.160.5` **(google ip address) ->  navify.com**

![insert pic9 here](https://s3.amazonaws.com/user-content.stoplight.io/18223/1579263564100)

If I ping [navify.com](navify.com), it will resolve and point me to [google.com](google.com) instead of [navify.com](navify.com)

```bash
PING navify.com (64.233.160.5): 56 data bytes
```

**note**: this is Google IP, not Navify IP

When this alert is triggered, pay attention on the hostnames and address columns on the alert. It will list the address been configured. If you suspect something, you can ask your colleagues for that and [**SecOps team**](mailto:global.navify_platform_secops@roche.com) for advise.

## Alert_SecOps_Osquery_\<PRODUCT NAME>_\<CRITICALITY>

This alert is informational, to show you the list of users creating logins to your instances, by country, product name, sourcehost and username. Please review this information to see if there is something that is weird, e.g country or city, etc. The threat column should indicate if any source IP it could be associated with malicious IPs. In that case, please contact [**SecOps team**](mailto:global.navify_platform_secops@roche.com) for advise.

![insert pic9 here](https://s3.amazonaws.com/user-content.stoplight.io/18223/1579263571730)

## Alert_SecOps_Osquery_UID_greater_than_2147483646_\<PRODUCT NAME>_\<CRITICALITY>

This alert is very descriptive. There is known vulnerability with a software, that, when installed and a user is created with a `UID` larger than **2147483646**, it can be used to escalate privileges. You can check `/etc/passwd` file for that `UID` in particular and notify [**SecOps team**](mailto:global.navify_platform_secops@roche.com).

## Alert_SecOps_Osquery_UID_OR_GID_0_\<PRODUCT NAME>_\<CRITICALITY>

If a user or group is configured with `UID` or `GID 0`, that means, `root` privileges. This should be investigated. It could be on purpose. Check `/etc/passwd` or `/etc/groups` for hints on those `users/groups`.

## Alert_SecOps_Osquery_CRONTAB_COMMANDS_\<PRODUCT NAME>_\<CRITICALITY>

This alert will look on crontab jobs a configuration indicating malicious command executions. If you receive an alert, please check the crontab configuration for those commands triggered. 

`crontab -e` will edit the current configuration.

# GuardDuty

## Alert_SecOps_GuardDuty_MaliciousIPCaller_\<PRODUCT NAME>_\<CRITICALITY> → will run every 6 hours (6 hours time frame)

This alert triggers because one or more EC2 instances is communicating with malicious IPs. This is something normal, as every system exposed to Internet, malicious actors are trying to compromise. To remediate this, please find the EC2 on the column `Description` and the public IP on the `AWS_IP` column. Go to its security group and do not leave it too open, like `0.0.0.0/0`, and try to restrict it with a Roche IP, your own home IP, etc. This way, the malicious actors will not have access to scan the machine and try to compromise it. Please ask [**SecOps team**](mailto:global.navify_platform_secops@roche.com) for advise if you are unsure.

Below you can see a sample of one alert:

![insert pic11 here](https://s3.amazonaws.com/user-content.stoplight.io/18223/1579263580527)

## Alert_SecOps_GuardDuty_\<HIGH> .  ---> `only for SecOps`

This alert is considered HIGH severity and [**SecOps team**](mailto:global.navify_platform_secops@roche.com) will receive it via VictorOps or email. product owners will be notified and contacted for help.

## Alert_SecOps_GuardDuty_\<PRODUCT NAME>_\<MEDIUM> → will run every 6 hours (6 hours time frame)

GuardDuty Medium alerts may include many different types of alerts. They are listed here:

[https://docs.aws.amazon.com/guardduty/latest/ug/guardduty_finding-types-active.html](https://docs.aws.amazon.com/guardduty/latest/ug/guardduty_finding-types-active.html)

It should be descriptive, but you may always ask [**SecOps team**](mailto:global.navify_platform_secops@roche.com) for help if is not clear enough. 

One example:

![insert pic12 here](https://s3.amazonaws.com/user-content.stoplight.io/18223/1579263591910)

## Alert_SecOps_GuardDuty_RDP_BruteForce_\<PRODUCT NAME>_\<CRITICALITY>

This alert triggers when there are some instances exposed to internet, in this case, port **3389 (RDP)**. Many malicious actors are trying to brute force using different passwords. In order to fix this, please configure its security groups to be more restrictive. You may configure Roche IPs or your own home IP to that system. That way, no one else would be able to access and try to compromise the instance.

## Alert_SecOps_GuardDuty_SSH_BruteForce_\<PRODUCT NAME>_\<CRITICALITY>

This alert triggers when there are some instances exposed to internet, in this case, port **22 (SSH)**. Many malicious actors are trying to brute force using different passwords. In order to fix this, please configure its security groups to be more restrictive. You may configure Roche IPs or your own home IP to that system. That way, no one else would be able to access and try to compromise the instance.

## Alert_SecOps_GuardDuty_PortProbe_\<PRODUCT NAME>_\<CRITICALITY>

We will get this kind of alert when any port is exposed to internet with no restrictions. Malicious actors will try to scan the port and try to compromise it. In order to fix this, please configure its security groups to be more restrictive. You may configure Roche IPs or your own home IP to that system. That way, no one else would be able to access and try to compromise the instance.

Example:

![insert pic13 here](https://s3.amazonaws.com/user-content.stoplight.io/18223/1579263607460)

## Alert_SecOps_GuardDuty_Console_logins_\<PRODUCT NAME>_\<CRITICALITY>

This alert triggers when is detected that users has login from  a different place than usual. It may be a sympton of a compromise. Please cross check with the user if he was login from that country/city.

![insert pic14 here](https://s3.amazonaws.com/user-content.stoplight.io/18223/1579263618714)

### Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>