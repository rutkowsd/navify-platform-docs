# Hardened AMI Configuration Guiding Rules

## Modifying Files and Directories

**Do not update, modify, delete anything under the following directories or any of the files specified here:**

* `/opt/SumoCollector/`
* `/opt/fireeye/`
* `/etc/clamav/`
* `/opt/sumo-register.sh`
* `/etc/osquery/`
* `/etc/profile.d/Z99-shell-logging.sh`
* `/etc/audit/auditd.conf`
* `/etc/audit/rules.d/secops.rules`
* `/etc/login.defs`
* `/etc/pam.d/system-auth-ac`
* `/etc/libuser.conf`
* `/etc/modprobe.d/dev-sec.conf`
* `/etc/audit/rules.d/secops.rules`
* `/etc/osquery/osquery.conf`
* `/etc/init.d/sumologic-register` (file not to delete)
* `/etc/rsyslog.d/00-secops.conf`
* `/var/log/shhistory/`
* `/etc/rsyslog.d/50-default.conf`
* `/etc/systemd/journald.conf`
* `/etc/logrotate.*`
* [TO DO: stay tuned more will be added]

## Init/Unit Files and Agents

**Do not edit or remove the init/unit files, and/or kill any of the agents mentioned for each of the Hardened AMI types.**

### Ubuntu-18

| Agents | Init/Unit files |
|---|---|
| /opt/fireeye/bin/xagt aka xagt | unit file: /usr/lib/systemd/system/xagt.service |
| /opt/<TODO: DETERMINE> | init file: /etc/init.d/osqueryd |
| /opt/aws/awsagent/bin/awsagent, aka awsagent | init file: /etc/init.d/awsagent |
| /usr/sbin/chronyd aka chronyd | init file: /etc/init.d/chrony |
| /usr/bin/fail2ban-server, aka fail2ban-server | init file: /etc/init.d/fail2ban |
| /usr/sbin/rsyslogd, aka rsyslogd | init file: /etc/init.d/rsyslog |
| /usr/sbin/haveged, aka haveged | init file: /etc/init.d/haveged |
| /lib/systemd/systemd-journald, aka systemd-journald | unit file: /lib/systemd/system/systemd-journald.service |
| /usr/bin/freshclam, aka freshclam | init file: /etc/init.d/clamav-freshclam
init file: /etc/init.d/clamav-daemon |
| /usr/sbin/cron, aka cron | init file: /etc/init.d/cron |
| /sbin/auditd, aka auditd | init file: /etc/init.d/auditd |
| Short lived process during init | init file: /etc/init.d/kmod |

### AMAZON Linux2

| Agents | Init/Unit files |
|---|---|
| /opt/fireeye/bin/xagt aka xagt | unit file: /usr/lib/systemd/system/xagt.service |
| <TODO: DETERMINE> | init file: /etc/init.d/osqueryd |
| /opt/aws/awsagent/bin/awsagent, aka awsagent | init file: /etc/init.d/awsagent |
| /usr/sbin/chronyd aka chronyd | init file: /etc/init.d/chrony |
| /usr/bin/fail2ban-server, aka fail2ban-server | init file: /etc/init.d/fail2ban |
| /usr/sbin/rsyslogd, aka rsyslogd | init file: /etc/init.d/rsyslog |
| /usr/sbin/haveged, aka haveged | init file: /etc/init.d/haveged |
| /usr/lib/systemd/systemd-journald, aka systemd-journald | unit file: /lib/systemd/system/systemd-journald.service |
| /usr/bin/freshclam, aka freshclam | init file: /etc/init.d/clamav-freshclam
init file: /etc/init.d/clamav-daemon |
| /usr/bin/freshclam-sleep |  |
| /usr/sbin/crond, aka crond | init file: /etc/init.d/crond |
| /sbin/auditd, aka auditd | init file: /etc/init.d/auditd |

### AMAZON Linux1

| Agents | Init/Unit files |
|---|---|
| /opt/fireeye/bin/xagt aka xagt | init file: /etc/rc.d/*/*xagt
init file: /etc/rc.d/init.d/xagt |
| /usr/bin/osqueryd, aka osqueryd | init file: /etc/init.d/osqueryd |
| /opt/aws/awsagent/bin/awsagent, aka awsagent | init file: /etc/init.d/awsagent |
| /usr/sbin/chronyd aka chronyd | init file: /etc/init.d/chronyd |
| /usr/bin/fail2ban-server, aka fail2ban-server | init file: /etc/init.d/fail2ban |
| /usr/sbin/rsyslogd, aka rsyslogd | init file: /etc/init.d/rsyslog |
| /usr/sbin/haveged, aka haveged | init file: /etc/init.d/haveged |
| /usr/bin/freshclam, aka freshclam | init file: /etc/init.d/clamd.scan |
| /usr/sbin/crond, aka crond | init file: /etc/init.d/crond |
| /sbin/auditd, aka auditd | init file: /etc/init.d/auditd |

### ECS - Ubuntu-18

| Agents | Init/Unit files |
|---|---|
| /opt/fireeye/bin/xagt aka xagt | unit file: /usr/lib/systemd/system/xagt.service |
| /opt/<TODO: DETERMINE> | init file: /etc/init.d/osqueryd |
| /opt/aws/awsagent/bin/awsagent, aka awsagent | init file: /etc/init.d/awsagent |
| /usr/sbin/chronyd aka chronyd | init file: /etc/init.d/chrony |
| /usr/bin/fail2ban-server, aka fail2ban-server | init file: /etc/init.d/fail2ban |
| /usr/sbin/rsyslogd, aka rsyslogd | init file: /etc/init.d/rsyslog |
| /usr/sbin/haveged, aka haveged | init file: /etc/init.d/haveged |
| /lib/systemd/systemd-journald, aka systemd-journald | unit file: /lib/systemd/system/systemd-journald.service |
| /usr/bin/freshclam, aka freshclam | init file: /etc/init.d/clamav-freshclam
init file: /etc/init.d/clamav-daemon |
| /usr/sbin/cron, aka cron | init file: /etc/init.d/cron |
| /sbin/auditd, aka auditd | init file: /etc/init.d/auditd |
| Short lived process during init | init file: /etc/init.d/kmod |

### Windows

[TO BE ADDED]

## Custom Artifactory Repositories

The Hardened AMIs are setup to use the NAVIFY Platform repository, therefore  your package management solutions will pull first from our controlled environment.

In this case we ask you not to modify the specific files or the content of the folders specified here.

### Ubuntu 16

* Do not modify the navifyplatform.jfrog.io content within: /etc/apt/sources.list
* Do not modify anything under this path: /etc/apt/preferences.d/*
* Do not modify this file: /etc/apt/auth.conf

### Ubuntu 18

* Do not modify the navifyplatform.jfrog.io content within: /etc/apt/sources.list
* Do not modify anything under this path: /etc/apt/preferences.d/*
* Do not modify this file: /etc/apt/auth.conf

### Amazon Linux1

[TO BE ADDED]

### Amazon Linux2

[TO BE ADDED]

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
