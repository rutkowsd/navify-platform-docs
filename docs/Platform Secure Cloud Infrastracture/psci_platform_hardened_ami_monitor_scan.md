# Monitoring and Scanning of Hardened AMIs

This document describes the rules around monitoring our SecOps team follows with regards to the provided Platform Hardened AMIs and how the SecOps team will enforce these rules. This also includes the Bastion Host deployed in your account during creation.

## Monitoring

The SecOps team continuously monitors any launched ec2 instance. If an ec2 instance is detected that does not follow our guiding rules, your **security champion** will be notified and your instance will be terminated if necessary.

Our team will monitor and detect:

* **Rogue (unhardened, non-platform, unknown) AMIs**

  > <p style="color:#F78D2A; font-weight:bold">IMPORTANT!</p><p style="color:#F78D2A;">Any non-hardened platform provided AMI’s detected in use (depending on the environment type), will either be terminated immediately or a notice will be sent detailing the date and time in which the instance will be terminated if no action is taken by the product.</p>

* **Hardened AMI cross-account outbound sharing**

  * Sharing AMIs across different accounts is also not permitted and will be detected and the hardened AMI will be removed/terminated.

* **Rogue EC2 instances**

* **EC2 instances completely lacking an (IAM role) instance profile**

  * **Sumo Logic Role** - EC2 instance profiles will be continuously monitored and you will receive a notification by the SecOps team to correct and relaunch your EC2 instance(s) which are missing the SumoLogic Role. This policy is a strict requirement.

* **EC2 instances with unacceptable (IAM role) instance profiles**

  * **Sumo Logic Policy** - EC2 instance profiles will be continuously monitored and you will receive a notification by the SecOps team to correct and relaunch your EC2 instance(s) which are missing the SumoLogic policy. This policy is a strict requirement.

* **Explicit uses of Deny, potentially being used to create a false positive**

## OS Scanning

> <p style="color:#0066cc; font-weight:bold">NOTE:</p>
> <p style="color:#0066cc;">The Platform will check the provisioned AMI on a regular frequency</p>

The NAVIFY Platform maintains a list of product accounts and regions updated on a regular interval. Based on this list we check all Platform hardened AMIs throughout all product accounts by running AWS Inspector and Checksec scans. The reports of these scans are currently sent to the NAVIFY Platform security team

### Checksec-scan

**Steps:**

* Scan all binaries on the system
* Scan all processes running on the system
* Scan the system kernel
* Upload scan output files to SecOps S3 bucket

The output can be used to identify which binaries lack sufficient hardening levels. Binaries may need to be recompiled or another package may need to be tested and used if insufficient hardening levels are detected.

### AWS Inspector

AWS inspector applies all available [Amazon Inspector Rules Packages and Rules](https://docs.aws.amazon.com/inspector/latest/userguide/inspector_rule-packages.html)

* [Network Reachability](https://docs.aws.amazon.com/inspector/latest/userguide/inspector_network-reachability.html) - The rules in the Network Reachability package analyze your network configurations to find security vulnerabilities of your EC2 instances. The findings that Amazon Inspector generates also provide guidance about restricting access that is not secure.
* [Common Vulnerabilities and Exposure](https://docs.aws.amazon.com/inspector/latest/userguide/inspector_cves.html) - The rules in this package help verify whether the EC2 instances in your assessment targets are exposed to common vulnerabilities and exposures (CVEs).
* [Center for Internet Security (CIS) Benchmarks](https://docs.aws.amazon.com/inspector/latest/userguide/inspector_cis.html) - The CIS Security Benchmarks program provides well-defined, unbiased, consensus-based industry best practices to help organizations assess and improve their security.
* [Security Best Practices for Amazon Inspector](https://docs.aws.amazon.com/inspector/latest/userguide/inspector_security-best-practices.html) - Use Amazon Inspector rules to help determine whether your systems are configured securely.
* [Runtime Behavior Analysis](https://docs.aws.amazon.com/inspector/latest/userguide/inspector_runtime-behavior-analysis.html) - The rules in the Runtime Behavior Analysis rules package analyze the behavior of your instances during an assessment run. They also provide guidance about how to make your EC2 instances more secure.

### Antivirus scanning ClamAV

The antivirus solution currently used in the Base AMI is ClamAV. The following settings are used for scanning for viruses on the system.

* Freshclam updates the vulnerability database every hour.
* A full scan of the system is done nightly at 2AM.
* On Access Scanning is enabled on the clamav daemon in order scan files in real-time when accessed, saved, or ran.

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
