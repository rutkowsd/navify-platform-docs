# Amazon GuardDuty

## What is Amazon GuardDuty?

AWS GuardDuty is an Amazon threat detection service and part of the NAVIFY Secure Cloud infrastrucutre approach.

To learn more about the benefits that Amazon GuardDuty offers, refer to the AWS documentation.

[Learn more about Amazon GuardDuty](https://aws.amazon.com/guardduty/)

## How Does Your Product Benefit from GuardDuty?

Amazon GuardDuty provides an additional layer of security to your AWS account. Your account is continuously monitored and each time GuardDuty detects a potential threat, an alert is generated.

It looks for malicious activity and can identify threats such as unusual API calls and unauthorized users to protect your AWS Accounts and workload.

The NAVIFY Platform manages one master GuardDuty account and adds your product account as a member. It allows us to manage all its findings for all the regions where GuardDuty is enabled.

## Use Case and Scenarios

GuardDuty allows us to monitor the AWS account access behaviour for signs of compromise such as:

- Unusual APIs calls, like for example a password policy change to reduce password strength.
- Instances that are deployed in a region that is not in use.

For this reason, we enable GuardDuty in all your AWS regions.

## GuardDuty Logs

You can view the logs that GuardDuty generate in the Sumo Logic dashboard.

The logs use the JSON format

``` json
GuardDuty log example
{
  "schemaVersion":"2.0",
  "accountId":"123456789012",
  "region":"us-west-1",
  "partition":"aws",
  "id":"74b42eefac0477551c574e8520bed2dc",
  "arn":"arn:aws:guardduty:us-west-1:636658394677:detector/62b429c449a4b7cbb3649d2a84122efd/finding/74b42eefac0477551c574e8520bed2dc",
  "type":"Recon:EC2/PortProbeUnprotectedPort",
  "resource":{ … },
  "service": { … },
  "severity":2,
  "createdAt":"2019-01-17T22:12:24.712Z",
  "updatedAt":"2019-07-29T12:05:10.695Z",
  "title":"Unprotected port on EC2 instance i-0e551c9331a6b7f95 is being probed.",
  "description":"EC2 instance has an unprotected port which is being probed by a known malicious host."
}
```

## What Is the GuardDuty Logs Flow?

GuardDuty logs are sent to Sumo Logic for log analysis after setting up a hosted collector and configuring http source urls for the different regions we support.

The GuardDuty Workflow diagram shows the sequence and interaction between GuardDuty and the elements it interacts with:

1. GuardDuty scans your AWS account.
2. GuardDuty scans VPC Flow Logs
3. Logs with potential threats are stored into S3 bucket.
4. S3 bucket sends the logs to Sumo Logic for analytics and insights.

![Diagram - GuardDuty Logs Flow](https://s3.amazonaws.com/user-content.stoplight.io/18223/1565101954324 "Diagram - GuardDuty Logs Flow")

## Roles and responsibilities

In term of service accountability, the NAVIFY Platform is responsible for:

- Enabling and configuring the service for every account that you create
- Maintaining the service

## Prerequisites

- You need to request and obtain an AWS account.
- Once you receive your AWS account, you can view the logs that GuardDuty generates in your Sumo Logic dashboards.

## Comments

<div id="disqus_thread"></div>
<script >
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://docs-platform.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})(); </script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
