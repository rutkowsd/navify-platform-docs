# NAVIFY Platform Documentation

## 1. Introduction

The **NAVIFY Platform Docs** is powered by StopLight.io and is entirely built from a git repo. The documentation consists of a set of MD files splitted by documentation chapters.

Below you will find how to contribute, best practices...

Let's build an awesome documentation together!

The DevX team.

## 2. How to contribute

### 2.1 Request your access to Stoplight

Request your access by contacting the DevX team.

> GLONAVIFY-Platform-DevX_Team <glonavify-platform-devx_team@msxdl.roche.com>

### 2.2 Clone the repo

Tuto

> [https://docs.stoplight.io/platform/projects/git-repo](https://docs.stoplight.io/platform/projects/git-repo)

Quick reference (git command)

> `git clone https://navify-platform:$STOPLIGHT_TOKEN@git.stoplight.io/navify-platform/navify-platform-dev-hub.git`

### 2.3 Repo content

The repo consists of:

#### Files that you can update

- **MD files** that contains documentation content. **These files are the ones where you can express your talent**
- **DOCUMENTATION_STRUCTURE.md** represents the structure as shown in the portal. If you delete pages, create new ones... please update this file too.

#### Files you should not update (please don't edit)

- **Configuration files** that are used to render the portal (yaml, css...).

### 2.4 Master branch and working branch

- Our "Master" branch is the branch named "version/1.6".
- Create your own branch from our "master" branch (version/1.6) and named it with the JIRA ticket number related to your update (e.g. NP-1234).
- You can pull your branch to origin
- Send a request to the DevX team for review and merge.

**Why don't we use the master branch rather than version/x.y?**

Stoplight UI does not allow to render the master branch. Using it wouldn't allow us to preview the documentation online.

## 3. Image Management

- Insert a placeholder in your md file. i.e. \[Ref or Name of the picture\]
- Send the picture to the DevX Team for polishing, brand compliance check and upload.

## 4. Good practices

### 4.1 Title Capitalization

Use the following guidelines for capitalizing titles

- Nouns
- Verbs
- Adjectives
- Pronouns
- Adverbs
- Prepositions with five or more characters eg Between
- Subordinating conjunctions eg That and Whether

Coordinating conjunction must be in lower case, eg than, as, if

### 4.2 English US or UK1

We use British-English as a standard. (e.g. "Licence" and not "License")

### 4.3 Headers

- A page always starts with \"#\" header level that is similar to the navigation label.
- Only one \"#\" can be per document.

### 4.4 Code & Command line

Always use \"\`\`\`\"

Please alo specify the language. (json, python, bash, basic...)

### 4.5 Usage of tables

Tables are useful to condense and display detailed information avoiding wordiness. Whenever possible, use a table to convey structured information in a concise way. When designing a table, use these guidelines:

- Design the table with a clear objective in mind.
- Ensure that your tables function as a matrix and show relationships between categories.
- Keep entries brief.
- Try whenever possible to confine the table to a single page or screen.
- Use notes and bullets sparingly inside tables.

### 4.6 Usage of lists

Lists help to clarify, emphasize, and organize information. A well-formatted list can improve the visual impact of a document and can enhance the user’s comprehension. The content of a list can be procedural, as in a list of sequential tasks, or categorical, as in a list of parts or items.

#### When Creating a List

- Write a short introduction to put the list in context.
- Capitalise the first letter of each list element.
- Use the same punctuation for each list item.
- Use numbered lists for procedures.
- Begin each sentence with the same part of speech.

#### Using Punctuation in a List

When using punctuation in a list:

- The list must be preceded by colon.
- Use a period if the list contains one or more full sentences.
- Do not use any punctuation, if each list element is a word.

## 5. Questions

Please contact the DevX Team

> GLONAVIFY-Platform-DevX_Team <glonavify-platform-devx_team@msxdl.roche.com>
